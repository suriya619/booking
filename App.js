import React from 'react';
import { NativeBaseProvider, extendTheme } from 'native-base';
import { Provider } from "react-redux"; //** redux */
import { createStore, applyMiddleware } from "redux";   //** redux */
import thunk from "redux-thunk";
import reducers from './src/redux/reducers';
import { LogBox } from 'react-native';
import { persistReducer, persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import AsyncStorage from '@react-native-async-storage/async-storage';

// Components
import AuthenticateRoute from './src/components/AuthenticateRoute';

const persistConfig = {
	key: 'root',
	storage: AsyncStorage,
	whitelist: ['presistLocal']
};
LogBox.ignoreAllLogs();//Ignore all log notifications
const persistedReducer = persistReducer(persistConfig, reducers);
const store = createStore(persistedReducer, applyMiddleware(thunk));   // *redux */
const persistor = persistStore(store);
export { store, persistor };

const App = (props) => {

	const theme = extendTheme({
		colors: {
			primary: {
				50: '#E3F2F9',
				100: '#C5E4F3',
				200: '#A2D4EC',
				300: '#7AC1E4',
				400: '#47A9DA',
				500: '#0088CC',
				600: '#007AB8',
				700: '#006BA1',
				800: '#005885',
				900: '#003F5E',
			},
			amber: {
				400: '#d97706',
			},
		},
		config: {
			initialColorMode: 'dark',
		},
	});


	return (
		<NativeBaseProvider theme={theme}>
			<Provider store={store}>
				<PersistGate loading={null} persistor={persistor}>
						<AuthenticateRoute {...props}></AuthenticateRoute>
				</PersistGate>
			</Provider>
		</NativeBaseProvider>
	);
}


export default App;
