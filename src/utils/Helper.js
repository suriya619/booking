import DocumentPicker, { types } from 'react-native-document-picker';
import ImagePicker from 'react-native-image-crop-picker';

export const chooseFiles = async (fileType, type, state, setState, uploadFiles) => {
    if (fileType === 'Files') {
        try {
            const response = await DocumentPicker.pick({
                presentationStyle: 'fullScreen',
                type: [types.images],
            });
            setState({ ...state, [type]: response[0]?.uri })
        } catch (err) {
            console.log(err, 'Files err');
        }
    } else if (fileType === 'Images') {
        try {
            ImagePicker.openCamera({
                cropping: true,
                useFrontCamera: false,
                mediaType: 'photo',
                compressImageQuality: 0.2
            }).then(image => {
                setState({ ...state, [type]: image?.path });
                uploadFiles && uploadFiles(image?.path)
            });
        } catch (err) {
            console.log(err, 'Images err');
        }
    }
};


export const getUserTypeName = (routeName) => {
    switch (routeName) {
        case "Incoming Manifest Document":
            return "IncomingManifest";
        case "Outgoing Manifest Document":
            return "OutgoingManifest";
        case "RateCalculator":
            return "RateCalculator";
        default:
            return "";
    }
}
