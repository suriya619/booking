import { StyleSheet, Dimensions, Platform } from "react-native";

const css = StyleSheet.create({
	routeheader: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		backgroundColor: "transparent",
		width: Dimensions.get('window').width - 20,
	},
	dateText: {
		fontSize: 14,
		fontWeight: "bold",
		paddingRight: 10
	},
	text_andrd: {
		fontSize: 14,
		fontWeight: "bold",
	}
})

export default css;