import React from "react";
import { View, Text } from "react-native";
import { connect } from "react-redux";
import moment from "moment";
import css from "./styles";

const RouteHeader = (props) => {

    const { route } = props;

    const date = moment(new Date).format("DD/MM/YYYY");
    return (
        <View style={css.routeheader} >
            <Text style={css.text_andrd}>{(route?.params?.name) ? (route?.params?.name) : route?.name}</Text>
            {((route?.name === "Incoming Manifest Document" || route?.name === "Outgoing Manifest Document")) ? <Text style={css.dateText}>Date: {date}</Text> : null}
        </View>
    )
}

export default connect(null, null)(RouteHeader);