const InManifestData = [
    { id: 0, Consignment: "MAA20560010", Origin: "MAA", Dist: "MAA", Weight: "0.250 kg", Status: "Normal" },
    { id: 1, Consignment: "MAA20560010", Origin: "MAA", Dist: "MAA", Weight: "0.250 kg", Status: "Normal" },
    { id: 2, Consignment: "MAA20560010", Origin: "MAA", Dist: "MAA", Weight: "0.250 kg", Status: "Normal" },
    { id: 3, Consignment: "MAA20560010", Origin: "MAA", Dist: "MAA", Weight: "0.250 kg", Status: "Normal" },
    { id: 4, Consignment: "MAA20560010", Origin: "MAA", Dist: "MAA", Weight: "0.250 kg", Status: "Normal" },
    { id: 5, Consignment: "MAA20560010", Origin: "MAA", Dist: "MAA", Weight: "0.250 kg", Status: "Normal" },
    { id: 6, Consignment: "MAA20560010", Origin: "MAA", Dist: "MAA", Weight: "0.250 kg", Status: "Normal" },
    { id: 7, Consignment: "MAA20560010", Origin: "MAA", Dist: "MAA", Weight: "0.250 kg", Status: "Normal" }
]

const BookingReportData = [
    { id: 0, Consignment: "PON20560010", Origin: "MAA", Dist: "PON", Amount: 60 },
    { id: 1, Consignment: "PON20560010", Origin: "MAA", Dist: "PON", Amount: 60 },
    { id: 2, Consignment: "PON20560010", Origin: "MAA", Dist: "PON", Amount: 60 },
    { id: 3, Consignment: "PON20560010", Origin: "MAA", Dist: "PON", Amount: 60 },
    { id: 4, Consignment: "PON20560010", Origin: "MAA", Dist: "PON", Amount: 60 },
    { id: 5, Consignment: "PON20560010", Origin: "MAA", Dist: "PON", Amount: 60 },
    { id: 6, Consignment: "PON20560010", Origin: "MAA", Dist: "PON", Amount: 60 },
    { id: 7, Consignment: "PON20560010", Origin: "MAA", Dist: "PON", Amount: 60 }
];

const RunsheetReportData = [
    { id: 0, DRS_NO: "RPON2010", PC_Code: "HO", Date: "07/11/2012", Total_Consignment: 250 },
    { id: 1, DRS_NO: "RPON2011", PC_Code: "HO", Date: "07/11/2012", Total_Consignment: 250 },
    { id: 2, DRS_NO: "RPON2012", PC_Code: "HO", Date: "07/11/2012", Total_Consignment: 250 },
    { id: 3, DRS_NO: "RPON2013", PC_Code: "HO", Date: "07/11/2012", Total_Consignment: 250 },
    { id: 4, DRS_NO: "RPON2014", PC_Code: "HO", Date: "07/11/2012", Total_Consignment: 250 },
    { id: 5, DRS_NO: "RPON2015", PC_Code: "HO", Date: "07/11/2012", Total_Consignment: 250 },
    { id: 6, DRS_NO: "RPON2016", PC_Code: "HO", Date: "07/11/2012", Total_Consignment: 250 },
    { id: 7, DRS_NO: "RPON2017", PC_Code: "HO", Date: "07/11/2012", Total_Consignment: 250 }
];

const DummyDataSearch = [{ "value": "purple", }, { "value": "red", }, { "value": "blue", }, { "value": "blue", }, { "value": "blue", },
{ "value": "blue", }, { "value": "blue", }, { "value": "blue", }, { "value": "blue", }, { "value": "blue", }, { "value": "blue", },
{ "value": "blue", }, { "value": "blue", }, { "value": "blue", }, { "value": "blue", }, { "value": "blue", }, { "value": "blue", },
{ "value": "purple", }, { "value": "purple", }, { "value": "purple", }, { "value": "purple", }, { "value": "purple", }, { "value": "purple", },]

const DummyDataDropDown = [{ "value": "Apple", }, { "value": "Apple", }, { "value": "Apple", }, { "value": "Apple", }, { "value": "Apple", }
    , { "value": "Apple", }, { "value": "Apple", }, { "value": "mango", }, { "value": "watermelon", }, { "value": "Apple", },
    , { "value": "Apple", }, { "value": "Apple", }, { "value": "Apple", }, { "value": "Apple", }, { "value": "Apple", }]

const commonHeader = Platform.OS === 'ios' ? 65 : 55
const routeHeader = Platform.OS === 'ios' ? 50 : 48
const tabHeader = Platform.OS === 'ios' ? 50 : 40


export {
    InManifestData,
    BookingReportData,
    RunsheetReportData,
    commonHeader,
    routeHeader,
    tabHeader,
    DummyDataSearch,
    DummyDataDropDown,
}
