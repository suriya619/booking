import {
  CONNECTED,
  LOADER_SCREEN,
  AFTER_SUCCESS,
  DROPDOWN_ITEM,
  MANIFEST_DETAILS,
  MANIFEST_DETAILS_RESET,
  MANIFEST_RESEND_DETAILS,
  RUNSHEET_PUSH,
  RUNSHEET_PULL_EMPTY,
  RUNSHEET_PUSH_EMPTY,
  SIDEMENU_TOGGLE,
  CUSTOM_SEARCH_OUTSIDE_CLICK,
} from '../types';

export const Internet = (result) => {
  return {
    type: CONNECTED,
    payload: result,
  };
}
export const Loading = (result) => {
  return {
    type: LOADER_SCREEN,
    payload: result,
  };
}
export const getAfterSuccess = (result, key) => {
  return {
    type: AFTER_SUCCESS,
    payload: result,
    page: key,
  }
};
export const setMenuItem = (result, key) => {
  return {
    type: DROPDOWN_ITEM,
    payload: result,
    selectedType: key,
  }
};
export const setMainifestDetails = (result, key) => {
  return {
    type: MANIFEST_DETAILS,
    payload: result,
    key: key
  }
};
export const setInMainDetailsReset = (key) => {
  return {
    type: MANIFEST_DETAILS_RESET,
    key: key
  }
};
export const setManifestResendDatails = (result) => {
  return {
    type: MANIFEST_RESEND_DETAILS,
    payload: result,
  }
};
export const setScannedRunsheetPush = (result) => {
  return {
    type: RUNSHEET_PUSH,
    payload: result,
  }
};
export const setRunsheetPullEmpty = () => {
  return {
    type: RUNSHEET_PULL_EMPTY,
  }
};
export const setRunsheetPushEmpty = () => {
  return {
    type: RUNSHEET_PUSH_EMPTY,
  }
};
export const setToggleSideMenu = (result) => {
  return {
    type: SIDEMENU_TOGGLE,
    payload: result,
  }
};
export const setCustomSearchOutsideClick = (result) => {
  return {
    type: CUSTOM_SEARCH_OUTSIDE_CLICK,
    payload: result,
  }
};