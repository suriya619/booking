import axios from 'axios';
import {
    REGISTER_USERDETAILS,
    OTP,
    BOOKIN_DOC_IMAGE,
} from '../types';



const getHeaders = () => {
    let headers = { 'Content-Type': 'application/json' }
    return headers;
};
const getAPIURL = () => {
    let url = 'http://206.189.134.200:8080/'
    return url;
};
const APIConfig = {
    headers: getHeaders(), baseURL: getAPIURL()
};


// --------------------------API------------------------------------------------------------------------------------------------------------------------------------
export const getRegisterUserDetails = (reqObj, response) => {
    return {
        type: REGISTER_USERDETAILS,
        payload: reqObj,
        registrationResponse: response
    };
}
export const POST_Registration = (reqObj) => {
    return async (dispatch) => {
        let apiResponse = axios({
            method: 'POST',
            baseURL: 'http://206.189.134.200:8080',
            url: '/mapi/req/common',
            headers: await APIConfig.headers,
            data: JSON.stringify(reqObj)
        }).then((response) => {
            dispatch(getRegisterUserDetails(reqObj, response));
            return response;
        }).catch((err) => {
            console.log(err, 'err')
            if (err?.message) {
                return err?.message;
            } else {
                return err;
            }
        });
        return apiResponse;
    };
};
// --------------------------------------------------------------------------
export const getOTP = (result) => {
    return {
        type: OTP,
        payload: result,
    };
}
export const POST_OTP = (reqObj) => {
    return async (dispatch) => {
        let apiResponse = axios({
            method: 'POST',
            baseURL: 'http://206.189.134.200:8080',
            url: '/mapi/req/common',
            headers: await APIConfig.headers,
            data: JSON.stringify(reqObj)
        }).then((response) => {
            dispatch(getOTP(response));
            return response;
        }).catch((err) => {
            if (err?.message) {
                return err?.message;
            } else {
                return err;
            }
        });
        return apiResponse;
    };
}
// --------------------------------------------------------------------------
export const getglobalActions = (result, reducerType, reducerKey) => {
    return {
        type: reducerType,
        payload: result,
        Key: reducerKey
    };
}
export const Common_GlobalActions = (reqObj, reducerType, reducerKey) => {
    return async (dispatch, getState) => {
        const { ResponseOTP } = getState().presistLocal;
        const URL = ResponseOTP?.url;
        let apiResponse = axios({
            method: 'POST',
            url: URL,
            headers: await APIConfig.headers,
            data: JSON.stringify(reqObj)
        }).then((response) => {
            dispatch(getglobalActions(response, reducerType, reducerKey));
            return response;
        }).catch((err) => {
            if (err?.message) {
                return err?.message;
            } else {
                return err;
            }
        });
        return apiResponse;
    };
}

// --------------------------------------------------------------------------
export const POST_BookingDocument = (reqObj) => {
    return async (dispatch, getState) => {
        const { ResponseOTP } = getState().presistLocal;
        const URL = ResponseOTP?.url;
        let apiResponse = axios({
            method: 'POST',
            url: URL,
            headers: await APIConfig.headers,
            data: JSON.stringify(reqObj)
        }).then((response) => {
            return response;
        }).catch((err) => {
            if (err?.message) {
                return err?.message;
            } else {
                return err;
            }
        });
        return apiResponse;
    };
}
// --------------------------------------------------------------------------
export const POST_IN_OUT_GET_Manifest = (reqObj) => {
    return async (dispatch, getState) => {
        const { ResponseOTP } = getState().presistLocal;
        const URL = ResponseOTP?.url;
        let apiResponse = axios({
            method: 'POST',
            url: URL,
            headers: await APIConfig.headers,
            data: JSON.stringify(reqObj)
        }).then((response) => {
            return response;
        }).catch((err) => {
            console.log(err, 'err')
            if (err?.message) {
                return err?.message;
            } else {
                return err;
            }
        });
        return apiResponse;
    };
}
// --------------------------------------------------------------------------
export const getbookingdocimage = (result, key) => {
    return {
        type: key != 'PushRunsheet' ? BOOKIN_DOC_IMAGE : null,
        payload: result,
        Key: key,
    };
}
export const POST_ImageUpload = (formData, key) => {
    return async (dispatch) => {
        let apiResponse = axios({
            method: 'POST',
            url: "http://206.189.134.200:8080/pon/mapi/req/uploadimg",
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            },
        }).then((response) => {
            dispatch(getbookingdocimage(response?.data, key));
            return response;
        }).catch((err) => {
            console.log(err, 'err')
            if (err?.message) {
                return err?.message;
            } else {
                return err;
            }
        });
        return apiResponse;
    };
}
// --------------------------------------------------------------------------
export const POST_PushRunsheet = (reqObj) => {
    return async (dispatch, getState) => {
        const { ResponseOTP } = getState().presistLocal;
        const URL = ResponseOTP?.url;
        let apiResponse = axios({
            method: 'POST',
            url: URL,
            headers: await APIConfig.headers,
            data: JSON.stringify(reqObj)
        }).then((response) => {
            return response;
        }).catch((err) => {
            console.log(err, 'err')
            if (err?.message) {
                return err?.message;
            } else {
                return err;
            }
        });
        return apiResponse;
    };
}
// --------------------------------------------------------------------------
export const POST_EmployeePunchTime = (reqObj) => {
    return async (dispatch, getState) => {
        const { ResponseOTP } = getState().presistLocal;
        const URL = ResponseOTP?.url;
        let apiResponse = axios({
            method: 'POST',
            url: URL,
            headers: await APIConfig.headers,
            data: JSON.stringify(reqObj)
        }).then((response) => {
            return response;
        }).catch((err) => {
            console.log(err, 'err')
            if (err?.message) {
                return err?.message;
            } else {
                return err;
            }
        });
        return apiResponse;
    };
}