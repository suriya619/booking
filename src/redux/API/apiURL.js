const URL_Registration = `/mapi/req/common`;
const URL_OTP = `/mapi/req/common`;
const URL_BOOKING_DOC = `/pon/mapi/req/common`;
const URL_IMCOMING_MANIFEST = `/pon/mapi/req/common`;


export {
    URL_Registration,
    URL_OTP,
    URL_BOOKING_DOC,
    URL_IMCOMING_MANIFEST,
}