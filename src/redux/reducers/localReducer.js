import {
  CONNECTED,
  DROPDOWN_ITEM,
  AFTER_SUCCESS,
  LOADER_SCREEN,
  MANIFEST_DETAILS,
  MANIFEST_DETAILS_UPDATE,
  MANIFEST_DETAILS_RESET,
  MANIFEST_RESEND_DETAILS,
  SIDEMENU_TOGGLE,
  CUSTOM_SEARCH_OUTSIDE_CLICK,
} from "../types";


const initialstate = {
  loadingScreen: false,
  afterLoginsuccess: {
    successLogin: false,
    successOtp: false,
  },
  mainfestObj: {},
  InMainfestArr: [],
  OutMainfestArr: [],
  Internet: false,
  MainResendDatails: [],
  ToggleSideMenu: false,
  CustomSearchHides: false,
}



const localReducer = (state = initialstate, action) => {
  switch (action.type) {
    case CONNECTED:
      return {
        ...state, Internet: action?.payload,
      }
    case LOADER_SCREEN:
      return {
        ...state,
        loadingScreen: action?.payload,
      }
    case AFTER_SUCCESS:
      return {
        ...state,
        ...(action?.page === "login" && { afterLoginsuccess: { successLogin: action?.payload } }),
        ...(action?.page === "otp" && { afterLoginsuccess: { successOtp: action?.payload } }),
      }
    case DROPDOWN_ITEM:
      return {
        ...state,
        mainfestObj: { ...state.mainfestObj, [action.selectedType]: action.payload }
      }
    case MANIFEST_DETAILS:
      return {
        ...state,
        ...(action?.key === "Incoming Manifest Document" && { InMainfestArr: [action.payload] }),
        ...(action?.key === "Outgoing Manifest Document" && { OutMainfestArr: [action.payload] }),
      }
    case MANIFEST_DETAILS_UPDATE:
      return {
        ...state,
        ...(action?.key === "Incoming Manifest Document" && { InMainfestArr: [...state?.InMainfestArr, action.payload] }),
        ...(action?.key === "Outgoing Manifest Document" && { OutMainfestArr: [...state?.OutMainfestArr, action.payload] }),
      }
    case MANIFEST_DETAILS_RESET:
      return {
        ...state,
        ...(action?.key === "Incoming Manifest Document" && { InMainfestArr: [] }),
        ...(action?.key === "Outgoing Manifest Document" && { OutMainfestArr: [] }),
      }
    case MANIFEST_RESEND_DETAILS:
      return {
        ...state,
        MainResendDatails: action?.payload,
      }
    case SIDEMENU_TOGGLE:
      return {
        ...state,
        ToggleSideMenu: action?.payload,
      }
    case CUSTOM_SEARCH_OUTSIDE_CLICK:
      return {
        ...state,
        CustomSearchHides: action?.payload,
      }
    default:
      return state;
  }
}

export default localReducer;