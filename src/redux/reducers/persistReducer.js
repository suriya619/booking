import {
  OTP,
  SIDEMENU,
  RUNSHEET_PULL,
  RUNSHEET_PUSH,
  RUNSHEET_PULL_EMPTY,
  RUNSHEET_PUSH_EMPTY,
  PRESIST_AUTOCOMPLETE_RELATION,
  PRESIST_AUTOCOMPLETE_REASON,
} from "../types";

const initialstate = {
  ResponseOTP: {},
  ResponseSideMenu: [],
  PullRunsheet: [],
  PushRunsheet: [],
  PersistRelation: [],
  PersistReason: []
}


const persistStorageReducer = (state = initialstate, action) => {
  switch (action.type) {
    case OTP:
      return {
        ...state,
        ResponseOTP: action?.payload?.data?.data[0],
      }
    case SIDEMENU:
      return {
        ...state,
        ResponseSideMenu: action?.payload?.data?.data
      }
    case RUNSHEET_PULL:
      return {
        ...state,
        PullRunsheet: action?.payload?.data?.data
      }
    case RUNSHEET_PUSH:
      const temp = [...state?.PushRunsheet, action.payload]
      const ids = (temp || []).map(fil => fil?.ConsignmentNo)
      const filtered = (temp || []).filter(({ ConsignmentNo }, index) => !ids.includes(ConsignmentNo, index + 1))
      return {
        ...state, PushRunsheet: filtered
      }
    case RUNSHEET_PULL_EMPTY:
      return {
        ...state,
        PullRunsheet: []
      }
    case RUNSHEET_PUSH_EMPTY:
      return {
        ...state,
        PushRunsheet: []
      }
    case PRESIST_AUTOCOMPLETE_RELATION:
      return {
        ...state, PersistRelation: action?.payload?.data?.data,
      }
    case PRESIST_AUTOCOMPLETE_REASON:
      return {
        ...state, PersistReason: action?.payload?.data?.data,
      }
    default:
      return state;
  }
}

export default persistStorageReducer;