import { combineReducers } from "redux";
import globalReducers from "../reducers/globalReducers";
import localReducer from "../reducers/localReducer";
import presistStorageReducer from "../reducers/persistReducer";

export default combineReducers({
    global: globalReducers,
    local: localReducer,
    presistLocal: presistStorageReducer,
});