import {
	GET_DATA_LIST,
	REGISTER_USERDETAILS,
	RECEIVED_STATUS,
	IN_OUT_MANIFEST,
	AUTOCOMPLETE_DATA,
	BOOKIN_DOC_IMAGE,
	BOOKING_REPORT_DATE,
	RUNSHEET_IMG_UPLOAD_GET,
	RUNSHEET_IMG_UPLOAD_POST,
	BOOKING_IMG_CAPTURE_GET,
	RUNSHEET_ENTRY_GET_DRS,
} from "../types";
import * as Constants from "../../utils/constants";

const tempArr = [
	{ id: 0, Name: "ANANDHAN", Street: "#245 First Main Road,", Address: "Pondicherry 6005001" },
	{ id: 1, Name: "SANJAY", Street: "No 2/45 Second Street", Address: "Chennai 6000012" }
]

const initialstate = {
	getBookingDataList: Constants.BookingReportData,
	getRunsheetDataList: Constants.RunsheetReportData,
	getInManifestDataList: Constants.InManifestData,
	datalist: tempArr,
	RegisterUserDetails: {},
	receivedStatus: [],
	incommingDetailsList: [],
	outgoingDetailsList: [],
	outgoingDestnationData: [],
	AutoCompleteData: {
		Branch: [],
		Pincode: [],
		Destination: [],
		BookingRepBranch: [],
		OutManifestDestination: [],
		RouteName: [],
	},
	BookingDocImage: {
		fullImage: [],
		toImage: [],
		runSheetImg: [],
		fullImagesAdd: [],
		bookingImageCapTo: [],
		officeDelivery: [],
		bookingImage: [],
	},
	BothReport: {
		RunReport: [],
		BookReport: [],
	},
	RunsheetImgUploadGet: [],
	RunsheetImgShow: [],
	BookingImgCapture: {},
	RunsheetEntryDetails: [],
}

const globalReducer = (state = initialstate, action) => {
	switch (action.type) {
		case GET_DATA_LIST:
			return {
				...state,
				datalist: [...state.datalist, action?.payload],
			}
		case REGISTER_USERDETAILS:
			return {
				...state,
				RegisterUserDetails: { ...action?.payload, data: action?.registrationResponse?.data }
			}
		case RECEIVED_STATUS:
			return {
				...state,
				receivedStatus: action?.payload?.data,
			}
		case IN_OUT_MANIFEST:
			return {
				...state,
				...(action?.tab === "Incoming" && { incommingDetailsList: action?.payload?.data }),
				...(action?.tab === "Outgoing" && { outgoingDetailsList: action?.payload?.data }),
			}
		case AUTOCOMPLETE_DATA:
			return {
				...state,
				outgoingDestnationData: action?.payload?.data,
				...(action?.Key === "SEARCBRANCH" && { AutoCompleteData: { Branch: action?.payload?.data } }),
				...(action?.Key === "SEARCHPIN" && { AutoCompleteData: { Pincode: action?.payload?.data } }),
				...(action?.Key === "SEARCHDEST" && { AutoCompleteData: { Destination: action?.payload?.data } }),
				...(action?.Key === "BOOKREPBRANCH" && { AutoCompleteData: { BookingRepBranch: action?.payload?.data } }),
				...(action?.Key === "GETDISTIN" && { AutoCompleteData: { OutManifestDestination: action?.payload?.data } }),
				...(action?.Key === "SEARCROUT" && { AutoCompleteData: { RouteName: action?.payload?.data } }),
			}
		case BOOKIN_DOC_IMAGE:
			return {
				...state,
				...(action?.Key === "fullImages" && { BookingDocImage: { fullImage: action?.payload } }),
				...(action?.Key === "toImages" && { BookingDocImage: { toImage: action?.payload } }),
				...(action?.Key === "runsheetImages" && { BookingDocImage: { runSheetImg: [...state?.BookingDocImage?.runSheetImg, action.payload.data], } }),
				...(action?.Key === "fullImagesAdd" && { BookingDocImage: { fullImagesAdd: [...state?.BookingDocImage?.fullImagesAdd, action.payload.data], } }),
				...(action?.Key === "bookingImageCapTo" && { BookingDocImage: { bookingImageCapTo: [...state?.BookingDocImage?.bookingImageCapTo, action.payload.data], } }),
				...(action?.Key === "officeDelivery" && { BookingDocImage: { officeDelivery: [...state?.BookingDocImage?.officeDelivery, action.payload.data], } }),
				...(action?.Key === "booking" && { BookingDocImage: { bookingImage: [...state?.BookingDocImage?.bookingImage, action.payload.data], } }),
			}
		case BOOKING_REPORT_DATE:
			return {
				...state,
				...(action?.Key === "BOOKREPORT" && { BothReport: { BookReport: action?.payload } }),
				...(action?.Key === "RUNREPORT" && { BothReport: { RunReport: action?.payload } }),
			}
		case RUNSHEET_IMG_UPLOAD_GET:
			return {
				...state,
				RunsheetImgUploadGet: action?.payload
			}
		case RUNSHEET_IMG_UPLOAD_POST:
			return {
				...state,
				RunsheetImgShow: action?.payload
			}
		case BOOKING_IMG_CAPTURE_GET:
			return {
				...state,
				BookingImgCapture: action?.payload
			}
		case RUNSHEET_ENTRY_GET_DRS:
			return {
				...state,
				RunsheetEntryDetails: action?.payload?.data?.data
			}
		default:
			return state;
	}
}

export default globalReducer;