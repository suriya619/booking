import React, { useState } from "react";
import { connect } from "react-redux";
import { Platform, View } from "react-native";
import { CustomCancel } from "../../components/CustomButton";
import { CustomSubmit } from "../../components/CustomButton";
import styles from "../Registration/styles";
import * as callActions from "../../redux/actions/globalActions";
import * as calllocalActions from "../../redux/actions/localActions";
import CustomTextField from "../../components/CustomTextField";
import { isEmpty } from "lodash";


const ManageOTP = (props) => {

    const { navigation, RegisterUserDetails, otp, aftersuccess, afterLoginsuccess, loading } = props;

    const [registerOTP, setRegisterOTP] = useState("");
    const handleChangeValue = (value) => {
        setRegisterOTP(value);
    };


    const submitHandler = async (event) => {
        if (!isEmpty(RegisterUserDetails)) {
            const reqObj = {
                objName: 'mobileregistration',
                cmod: 'CHECKOTP',
                p001: RegisterUserDetails?.p001,
                p002: RegisterUserDetails?.p002,
                p003: RegisterUserDetails?.p003,
                p004: RegisterUserDetails?.p004,
                p005: RegisterUserDetails?.data?.data[0]?.temptoken,
                p006: registerOTP,
            }
            loading(true);
            const fetch_API = await (otp(reqObj));
            if (fetch_API["data"]?.data?.length > 0) {
                loading(false);
                setRegisterOTP("");
                aftersuccess(true, "otp");
                if (fetch_API?.data?.data[0]?.userid) {
                    navigation.push('Home');
                }
            } else {
                loading(false);
                alert("It is wrong OTP");
            }
        }
    }

    return (
        <>
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <CustomTextField onChangeText={(e) => handleChangeValue(e)} value={registerOTP}
                        placeholder="OTP" keyboardType="numeric" placeholderTextColor={Platform.OS === 'ios' ? "#a3a3a3" : "#d4d4d4"} style={styles.field} />
                    <View style={{ paddingTop: 40 }}>
                        <CustomSubmit type={"Submit"} onPress={submitHandler} />
                        <CustomCancel type={"Cancel"} {...props} successLogin={afterLoginsuccess?.successLogin} />
                    </View>
                </View>
            </View>
        </>
    )
}

const mapStateToProps = (state) => {
    const { RegisterUserDetails, } = state.global;
    const { afterLoginsuccess } = state.local;

    return {
        RegisterUserDetails,
        afterLoginsuccess,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        otp: (loginDetails) => { return dispatch(callActions.POST_OTP(loginDetails)) },
        aftersuccess: (reqObj, key) => { return dispatch(calllocalActions.getAfterSuccess(reqObj, key)) },
        loading: (reqObj) => { return dispatch(calllocalActions.Loading(reqObj)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageOTP);