import React, { useState, useEffect } from 'react';
import { useWindowDimensions } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import { connect } from 'react-redux';

// Components
import CustomManifest from '../../components/CustomManifest';
import CustomFlatlist from '../../components/CustomFlatlist';

// Redux :: Actions
import * as callActions from "../../redux/actions/globalActions";
import { RECEIVED_STATUS } from '../../redux/types';


const ManageManifest = (props) => {

	const { globalaction } = props;

	const initialObj = {
		staffName: "",
		manifestNo: "",
		consigmentNo: "",
		packet: "1",
		weight: "0.250",
		pices: "1",
		destination: "",
		total: 40,
		count: 0,
	}
	const layout = useWindowDimensions();
	const [MainfestData, setMainfestData] = useState(initialObj);
	const [index, setIndex] = useState(0);
	const [previousConsigment, setPreviousConsigment] = useState(null);
	const [disableContainer, setDisableContainer] = useState(true);
	const [routes] = useState([
		{ key: 'first', title: 'Manifest' },
		{ key: 'second', title: 'Details' },
	]);
	const reqObjReceivedStatus = {
		objName: 'manifestin',
		cmod: 'GETRECIVEDSTATUS',
	};

	useEffect(() => {
		globalaction(reqObjReceivedStatus, RECEIVED_STATUS, null);
	}, []);

	useEffect(() => {
		if (MainfestData?.staffName != "" && MainfestData?.manifestNo != "") {
			setDisableContainer(false);
		} else {
			setDisableContainer(true);
		}
	})

	const renderTabBar = props => (
		<TabBar {...props} style={{ backgroundColor: '#06b6d4' }} />
	);

	const renderScene = ({ route }) => {
		switch (route.key) {
			case 'first':
				return <CustomManifest {...props} MainfestData={MainfestData} setMainfestData={setMainfestData} initialObj={initialObj} setPreviousConsigment={setPreviousConsigment}
					previousConsigment={previousConsigment} setDisableContainer={setDisableContainer} disableContainer={disableContainer} />;
			case 'second':
				return <CustomFlatlist {...props} setPreviousConsigment={setPreviousConsigment} />;
			default:
				return null;
		}
	};


	return <>
		<TabView
			renderTabBar={renderTabBar}
			navigationState={{ index, routes }}
			renderScene={renderScene}
			onIndexChange={setIndex}
			initialLayout={{ width: layout.width }}
		/>
	</>
}

const mapStateToProps = (state) => {
	const { receivedStatus, incommingDetailsList, outgoingDetailsList } = state.global;
	return { receivedStatus, incommingDetailsList, outgoingDetailsList }
};

const mapDispatchToProps = (dispatch) => {
	return {
		globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageManifest);