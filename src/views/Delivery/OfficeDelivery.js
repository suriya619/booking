import React, { useState, useRef } from "react";
import { View, Image, Platform, ScrollView, Text, TouchableOpacity, Linking, Alert } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { connect } from "react-redux";
import moment from 'moment';
import Geolocation from 'react-native-geolocation-service';

// Components
import { CustomSubmit, CustomReset } from "../../components/CustomButton";
import CustomTextField from "../../components/CustomTextField";
import CustomSearchColumn from "../../components/CustomSearchColumn";
import CustomQRScanner from "../../components/CustomQRScanner";
import CustomFieldWithAction from "../../components/CustomFieldWithAction";

// Stylesheet
import styles from "../Tabcomponents/styles";
import imageStyles from "../../components/styles"

// Utils
import { chooseFiles } from "../../utils/Helper";

// Redux :: Actions
import * as callActions from "../../redux/actions/globalActions";
import * as calllocalActions from "../../redux/actions/localActions";
import { AUTOCOMPLETE_DATA, OFFICE_DELIVERY } from "../../redux/types";



const OfficeDelivery = (props) => {

    const { AutoCompleteData, officeimageupload, PullRunsheet, sacnnedrunsheetpush, route,
        loading, PersistRelation, PersistReason, globalaction } = props;

    const consigmentNoRef = useRef(null);
    const mobileNoRef = useRef(null);
    const tempObj = {
        officeIdProof: '',
        officeIdProofResponse: '',
        homeIdProof: '',
        homeIdProofResponse: '',
    }
    const tempOffice = {
        officesearchBranch: '',
        officeexpandbranch: false,
        officesearchRelation: 'SELF',
        officeexpandRelation: false,
        officesearchStatus: '',
        officeexpandStatus: false,
        officeConsigmentNo: '',
        officeIconSwap: false,
        officemobile: '',
        officeIdproof: ''

    }
    const tempHome = {
        homesearchRelation: 'SELF',
        homeexpandRelation: false,
        homesearchStatus: '',
        homeexpandStatus: false,
        homeConsigmentNo: '',
        homemobile: '',
    }

    const todayDate = (moment(new Date).toISOString().slice(0, 10) + ' ' + moment(new Date).format('LT'));
    const [idproof, setIdproof] = useState(tempObj);
    const [swappingOffice, setSwappingOffice] = useState(false);
    const [swappingHome, setSwappingHome] = useState(false);
    const [scan, setScan] = useState(false);
    const [officeDelivery, setOfficeDelivery] = useState(tempOffice);
    const [homeDelivery, setHomeDelivery] = useState(tempHome);


    const getLocation = async () => {
        return new Promise((res) => {
            Geolocation.getCurrentPosition(
                (position) => {
                    res(position);
                },
                (error) => {
                    Alert.alert(
                        //This is title
                        "Error",
                        //This is body text
                        `${error?.message + " " + "Open settings to enable it."}`,
                        [
                            { text: 'ok', onPress: () => Linking.openSettings() },
                        ],
                    );
                },
                { enableHighAccuracy: true, showLocationDialog: true, distanceFilter: 100 })
        });
    };

    const handleScan = () => {
        setScan(true);
    };

    const onSuccess = (e) => {
        (route?.params?.name === 'Office Delivery') ? setOfficeDelivery({ ...officeDelivery, officeConsigmentNo: e?.data })
            : setHomeDelivery({ ...homeDelivery, homeConsigmentNo: e?.data });
        setScan(false);
    };

    const handleChangeQR = (data) => {
        (route?.params?.name === 'Office Delivery') ? setOfficeDelivery({ ...officeDelivery, officeConsigmentNo: data })
            : setHomeDelivery({ ...homeDelivery, homeConsigmentNo: data });
    };

    const handleSearchColumn = (txt, key) => {
        if (key === ("branch")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCBRANCH",
                p001: txt,
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "SEARCBRANCH");
            setOfficeDelivery({ ...officeDelivery, officesearchBranch: txt, officeexpandbranch: true });
        } else if (key === ("relation")) {
            (route?.params?.name === 'Office Delivery') ? setOfficeDelivery({ ...officeDelivery, officesearchRelation: txt, officeexpandRelation: true })
                : setHomeDelivery({ ...homeDelivery, homesearchRelation: txt, homeexpandRelation: true });
        } else if (key === ("deliverystatus")) {
            (route?.params?.name === 'Office Delivery') ? setOfficeDelivery({ ...officeDelivery, officesearchStatus: txt, officeexpandStatus: true })
                : setHomeDelivery({ ...homeDelivery, homesearchStatus: txt, homeexpandStatus: true });
        }
    };

    const handleSelectColumn = (sel, key) => {
        if (key === ("branch")) {
            setOfficeDelivery({ ...officeDelivery, officesearchBranch: sel?.value, officeexpandbranch: false });
        } else if (key === ("relation")) {
            (route?.params?.name === 'Office Delivery') ? setOfficeDelivery({ ...officeDelivery, officesearchRelation: sel?.value, officeexpandRelation: false })
                : setHomeDelivery({ ...homeDelivery, homesearchRelation: sel?.value, homeexpandRelation: false });
        } else if (key === ("deliverystatus")) {
            (route?.params?.name === 'Office Delivery') ? setOfficeDelivery({ ...officeDelivery, officesearchStatus: sel?.value, officeexpandStatus: false })
                : setHomeDelivery({ ...homeDelivery, homesearchStatus: sel?.value, homeexpandStatus: false });
        }
    };

    const handleChangeValue = (value) => {
        (route?.params?.name === 'Office Delivery') ? setOfficeDelivery({ ...officeDelivery, officemobile: value })
            : setHomeDelivery({ ...homeDelivery, homemobile: value });
    };

    const handleCancel = (key) => {
        if (key === 'officeImg') {
            setSwappingOffice(false);
            setIdproof(tempObj);
        }
        else {
            setSwappingHome(false);
            setIdproof(tempObj);
        }
    };


    const imgUpload = async () => {
        loading(true);
        const formData = new FormData();
        formData.append("interBranchCode", "HEAD");
        formData.append("consinmentdate", todayDate);
        formData.append("formtype", "Office");
        formData.append("file", {
            uri: idproof?.officeIdProof,
            type: "image/jpeg",
            name: idproof?.officeIdProof
        });
        const fetch_API = await officeimageupload(formData, 'officeDelivery');
        return fetch_API?.data?.data?.folder + '/' + fetch_API?.data?.data?.filename
    };

    const submitHandler = async () => {
        if (route?.params?.name === 'Office Delivery') {
            const img = await imgUpload();
            if (img && img != 'undefined/undefined' && officeDelivery?.officesearchBranch && officeDelivery?.officeConsigmentNo && officeDelivery?.officemobile && officeDelivery?.officesearchRelation) {
                const reqObj = {
                    objName: "delivery",
                    cmod: "DELIVERYINOFFICE",
                    p001: officeDelivery?.officesearchBranch,
                    p002: officeDelivery?.officeConsigmentNo,
                    p003: officeDelivery?.officemobile,
                    p004: officeDelivery?.officesearchRelation,
                    p005: img,
                }
                const fetch_API = await globalaction(reqObj, OFFICE_DELIVERY, null);
                if (fetch_API?.data?.data.length > 0) {
                    setOfficeDelivery(tempOffice);
                    setSwappingOffice(false);
                    setIdproof(tempObj);
                    loading(false);
                } else {
                    loading(false);
                    alert('Something went wrong');
                }
            } else {
                loading(false);
                alert('Not able to upload');
            }
        } else {
            const LOCATION = await getLocation();
            console.log(LOCATION, 'LOCATION')
            const checkAvailable = PullRunsheet.find(element => element?.ConsignmentNo?.toLowerCase() === homeDelivery?.homeConsigmentNo?.toLowerCase())
            if (checkAvailable) {
                if (LOCATION?.coords?.latitude && LOCATION?.coords?.longitude && todayDate && idproof?.homeIdProof && homeDelivery?.homesearchRelation
                    && homeDelivery?.homesearchStatus) {
                    const temp = {
                        ...checkAvailable,
                        latitude: LOCATION?.coords?.latitude,
                        longitude: LOCATION?.coords?.longitude,
                        DeliveryStatus: homeDelivery?.homesearchStatus,
                        DeliveryDate: todayDate,
                        ImgPath: idproof?.homeIdProof,
                        ReceiverRelationship: homeDelivery?.homesearchRelation
                    }
                    setIdproof(tempObj);
                    setHomeDelivery(tempHome);
                    setSwappingHome(false);
                    sacnnedrunsheetpush(temp);
                } else {
                    alert('Required all fields');
                }
            } else {
                alert('Consigment number not found');
            }
        }
    }

    const resetHandler = () => {
        if ((route?.params?.name === 'Office Delivery')) {
            setOfficeDelivery(tempOffice)
            setSwappingOffice(false);
            setIdproof(tempObj);
        } else {
            setHomeDelivery(tempHome);
            setSwappingHome(false);
            setIdproof(tempObj);
        }
    }


    return (
        <>
            <ScrollView showsHorizontalScrollIndicator={false} >
                <View style={styles.flex}>
                    {route?.params?.name === 'Office Delivery' && <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"branch"}
                        placeHolder="HO" handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={officeDelivery?.officesearchBranch}
                        show={officeDelivery?.officeexpandbranch} data={AutoCompleteData?.Branch?.data} onSubmitEditing={() => consigmentNoRef?.current?.focus()} />}

                    <CustomFieldWithAction placeholder="Consignment Number" {...props}
                        value={route?.params?.name === 'Office Delivery' ? officeDelivery.officeConsigmentNo : homeDelivery.homeConsigmentNo} onPress={() => handleScan()}
                        onChange={(e) => handleChangeQR(e?.nativeEvent?.text)} ref={consigmentNoRef} returnKeyType="next" onSubmitEditing={() => mobileNoRef?.current?.focus()}
                        blurOnSubmit={false} />

                    <CustomTextField placeholder='Mobile Number' onChangeText={(e) => handleChangeValue(e)}
                        value={(route?.params?.name === 'Office Delivery') ? officeDelivery.officemobile : homeDelivery.homemobile}
                        maxLength={10} keyboardType='numeric' ref={mobileNoRef} />

                    <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"relation"} placeHolder="Relation"
                        handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)}
                        searchTxt={(route?.params?.name === 'Office Delivery') ? officeDelivery?.officesearchRelation : homeDelivery?.homesearchRelation}
                        show={(route?.params?.name === 'Office Delivery') ? officeDelivery?.officeexpandRelation : homeDelivery?.homeexpandRelation}
                        data={PersistRelation} />

                    {route?.params?.name === 'Home Delivery' && <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"deliverystatus"} placeHolder="DeliveryStatus"
                        handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)}
                        searchTxt={(route?.params?.name === 'Office Delivery') ? officeDelivery?.officesearchStatus : homeDelivery?.homesearchStatus}
                        show={(route?.params?.name === 'Office Delivery') ? officeDelivery?.officeexpandStatus : homeDelivery?.homeexpandStatus}
                        data={PersistReason} />}

                    {(route?.params?.name === 'Office Delivery') ?
                        <View style={imageStyles.dragdrop}>
                            <Text style={Platform.OS === 'ios' ? imageStyles.imageText_ios : imageStyles.imageText}>ID Proof</Text>
                            {swappingOffice ?
                                <View>
                                    <TouchableOpacity>
                                        <Image style={imageStyles.imagesUpload}
                                            source={{ uri: idproof?.officeIdProof }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <MaterialIcons name="cancel" color="red" size={24} style={imageStyles.multiImageCancelIcon} onPress={() => handleCancel('officeImg')} />
                                </View> : <View style={{ flexDirection: 'row' }}>
                                    <View style={{ paddingRight: 20 }}>
                                        <MaterialIcons name="photo-camera" color="gray" size={24} onPress={() =>
                                            chooseFiles('Images', "officeIdProof", idproof, setIdproof) && setSwappingOffice(true)} />
                                    </View>
                                    <View style={{ paddingLeft: 20 }}>
                                        <MaterialIcons name="insert-photo" color="gray" size={24} onPress={() =>
                                            chooseFiles('Files', "officeIdProof", idproof, setIdproof) && setSwappingOffice(true)} />
                                    </View>
                                </View>}
                        </View>
                        :
                        <View style={imageStyles.dragdrop}>
                            <Text style={Platform.OS === 'ios' ? imageStyles.imageText_ios : imageStyles.imageText}>ID Proof</Text>
                            {swappingHome ?
                                <View>
                                    <TouchableOpacity>
                                        <Image style={imageStyles.imagesUpload}
                                            source={{ uri: idproof?.homeIdProof }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <MaterialIcons name="cancel" color="red" size={24} style={imageStyles.multiImageCancelIcon} onPress={() => handleCancel('homeImg')} />
                                </View> : <View style={{ flexDirection: 'row' }}>
                                    <View style={{ paddingRight: 20 }}>
                                        <MaterialIcons name="photo-camera" color="gray" size={24} onPress={() =>
                                            chooseFiles('Images', "homeIdProof", idproof, setIdproof) && setSwappingHome(true)} />
                                    </View>
                                    <View style={{ paddingLeft: 20 }}>
                                        <MaterialIcons name="insert-photo" color="gray" size={24} onPress={() =>
                                            chooseFiles('Files', "homeIdProof", idproof, setIdproof) && setSwappingHome(true)} />
                                    </View>
                                </View>}
                        </View>
                    }
                </View>
            </ScrollView>

            <View style={Platform.OS === 'ios' ? styles.buttons : styles.btn_container}>
                <CustomSubmit type={"Submit"} onPress={submitHandler} />
                <CustomReset type={"Reset"} {...props} onPress={resetHandler} />
            </View>
            {scan && <CustomQRScanner onSuccess={onSuccess} />}
        </>
    )
}



const mapStatetoProps = (state) => {
    const { AutoCompleteData, BookingDocImage } = state.global;
    const { Internet } = state.local;
    const { PullRunsheet, PersistRelation, PersistReason } = state.presistLocal;
    return {
        AutoCompleteData,
        BookingDocImage,
        PullRunsheet,
        Internet,
        PersistRelation,
        PersistReason
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
        officeimageupload: (reqObj, key) => { return dispatch(callActions.POST_ImageUpload(reqObj, key)) },
        sacnnedrunsheetpush: (reqObj) => { return dispatch(calllocalActions.setScannedRunsheetPush(reqObj)) },
        loading: (reqObj) => { return dispatch(calllocalActions.Loading(reqObj)) },
    }
}

export default connect(mapStatetoProps, mapDispatchToProps)(OfficeDelivery);