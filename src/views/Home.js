import React, { useEffect } from "react";
import { BackHandler, Alert, Text } from "react-native";
import { useNavigation } from '@react-navigation/native';
import { connect } from "react-redux";

const Home = (props) => {
  const { ToggleSideMenu } = props;
  const navigation = useNavigation();

  useEffect(() => {
    navigation.addListener('beforeRemove', (e) => {
      e.preventDefault();
      Alert.alert('Hold on', "Are you sure you want exit to the App?",
        [
          { text: "NO", onPress: () => null },
          { text: 'YES', onPress: () => BackHandler.exitApp() },
        ]
      );
    })
  }, [navigation]
  );

  return (
    <>
      <Text>HomePage</Text>
    </>
  )
}

const mapStateToProps = (state) => {
  const { ToggleSideMenu } = state.local;
  return { ToggleSideMenu }
}

export default connect(mapStateToProps, null)(Home);