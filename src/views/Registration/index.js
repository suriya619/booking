import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import { View, Platform, PermissionsAndroid } from "react-native";
import { CustomCancel } from "../../components/CustomButton";
import { CustomSubmit } from "../../components/CustomButton";
import styles from './styles';
import * as callActions from '../../redux/actions/globalActions';
import * as calllocalActions from '../../redux/actions/localActions';
import { getUniqueId } from 'react-native-device-info';
import Geolocation from 'react-native-geolocation-service';
import CustomTextField from "../../components/CustomTextField";

const reqObject = {
    branch: "",
    name: "",
    mobileNumber: "",
    uniqueID: ""
};

const Registration = (props) => {

    const { navigation, registration, aftersuccess, loading } = props;
    const nameRef = useRef();
    const mobilenumberRef = useRef();
    const [registerObj, setRegisterObj] = useState(reqObject);
    const [loader, setLoader] = useState(false);

    useEffect(() => {
        getUniqueId().then((uniqueId) => {
            setRegisterObj({ ...registerObj, uniqueID: uniqueId });
        });

    }, [registerObj.uniqueID])

    useEffect(() => {
        setInterval(() => {
            handleLocation();
        }, 900000)
    }, [])

    const handleLocation = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                Geolocation.getCurrentPosition(
                    (position) => {
                        console.log(position);
                    },
                    (error) => {
                        console.log(error);
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                );
                console.log("You can use the location");
            } else {
                console.log("location permission denied");
            }
        } catch (err) {
            console.log(err);
        }
    }

    const handleChangeValue = (value, type) => {
        setRegisterObj({ ...registerObj, [type]: value });
    };


    const submitHandler = async () => {
        const reqObj = {
            objName: "mobileregistration",
            cmod: "MOBILEREGISTER",
            p001: registerObj?.branch,
            p002: registerObj?.name,
            p003: registerObj?.mobileNumber,
            p004: registerObj?.uniqueID,
        }
        setLoader(true);
        loading(true);
        const fetch_API = await (registration(reqObj));
        if (fetch_API["data"]?.data?.length > 0) {
            setRegisterObj(reqObject);
            alert("Registration Success");
            aftersuccess(true, "login");
            loading(false);
            navigation.push('OTP');
        } else {
            loading(false);
            alert("Registration Failed");
        }
    }


    return (
        <>
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <View>
                        <CustomTextField
                            onChangeText={(e) => handleChangeValue(e, "branch")}
                            value={registerObj.branch}
                            placeholder='Branch'
                            placeholderTextColor={Platform.OS === 'ios' ? "#a3a3a3" : "#d4d4d4"}
                            returnKeyType="next"
                            onSubmitEditing={() => nameRef.current.focus()}
                            blurOnSubmit={false} />

                        <CustomTextField
                            onChangeText={(e) => handleChangeValue(e, "name")}
                            value={registerObj.name}
                            placeholder='Name'
                            placeholderTextColor={Platform.OS === 'ios' ? "#a3a3a3" : "#d4d4d4"}
                            blurOnSubmit={false}
                            returnKeyType="next"
                            onSubmitEditing={() => mobilenumberRef.current.focus()}
                            ref={nameRef} />

                        <CustomTextField
                            onChangeText={(e) => handleChangeValue(e.replace(/[^0-9]/g, ""), "mobileNumber")}
                            value={registerObj.mobileNumber}
                            placeholder='Mobile Number'
                            keyboardType={Platform.OS === 'android' ? "numeric" : "number-pad"}
                            maxLength={10}
                            placeholderTextColor={Platform.OS === 'ios' ? "#a3a3a3" : "#d4d4d4"}
                            ref={mobilenumberRef} />
                    </View>
                    <View style={{ paddingTop: 40 }}>
                        <CustomSubmit type={"Submit"} onPress={submitHandler} />
                        <CustomCancel type={"Cancel"} {...props} />
                    </View>
                </View>
            </View>
        </>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        registration: (reqObj) => { return dispatch(callActions.POST_Registration(reqObj)); },
        aftersuccess: (reqObj, key) => { return dispatch(calllocalActions.getAfterSuccess(reqObj, key)) },
        loading: (reqObj) => { return dispatch(calllocalActions.Loading(reqObj)) },
    }
}
export default connect(null, mapDispatchToProps)(Registration);