import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { View } from "react-native";
import NetInfo from "@react-native-community/netinfo";

// Components
import { CustomSubmit } from "../components/CustomButton";
import CustomTextField from "../components/CustomTextField";

// Redux :: Actions
import * as callActions from "../redux/actions/globalActions";
import * as calllocalActions from '../redux/actions/localActions';
import { PRESIST_AUTOCOMPLETE_REASON, RUNSHEET_PULL } from "../redux/types";


const PullRunsheet = (props) => {

  const { loading, ResponseOTP, globalaction } = props;

  const [connected, setConnected] = useState(false);
  const [runsheetNo, setRunsheetNo] = useState("");

  useEffect(() => {
    NetInfo.addEventListener(state => {
      setConnected(state?.isConnected);
    });
  }, [connected])

  handleChangeValue = (e) => {
    setRunsheetNo(e);
  }

  const submitHandler = async () => {
    if (runsheetNo) {
      loading(true);
      const rew = {
        objName: "runsheet",
        cmod: "SYNCRUN",
        p001: ResponseOTP?.branch_code,
        p002: runsheetNo
      }
      const reqObj = {
        objName: "autocomplete",
        cmod: "SEARCDELRE",
        p001: ""
      }
      if (connected) {
        const fetch_API = await globalaction(rew, RUNSHEET_PULL, null);
        const fetch_API_Reason = await globalaction(reqObj, PRESIST_AUTOCOMPLETE_REASON, "REASON");
        if (fetch_API?.data?.data.length > 0 && fetch_API_Reason?.data?.data.length > 0) {
          loading(false);
          setRunsheetNo("");
        } else {
          loading(false);
          alert('Not able to pull runsheet');
        }
      } else {
        loading(false);
        alert('Required internet to pull');
      }
    } else {
      alert('Required Runsheet Number');
    }
  }
  return (
    <View style={{ flex: 1, justifyContent: 'center' }}>
      <View style={{ paddingVertical: 10 }}>
        <CustomTextField placeholder="RunSheet Number" onChangeText={(e) => handleChangeValue(e)} value={runsheetNo}{...props}></CustomTextField>
      </View>
      <CustomSubmit type={"Submit"} onPress={submitHandler}></CustomSubmit>
    </View>
  )
}


const mapStatetoProps = (state) => {
  const { ResponseOTP, ResponseSideMenu, PullRunsheet, PushRunsheet } = state.presistLocal;
  return {
    ResponseOTP,
    ResponseSideMenu,
    PullRunsheet,
    PushRunsheet,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
    loading: (reqObj) => { return dispatch(calllocalActions.Loading(reqObj)) },
  }
}

export default connect(mapStatetoProps, mapDispatchToProps)(PullRunsheet);