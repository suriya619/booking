import React, { useEffect } from 'react';
import { View, BackHandler, Alert } from "react-native";
import { CustomSubmit, CustomCancel } from '../../components/CustomButton';
import styles from './styles';
import { connect } from "react-redux";
import { CustomReset } from '../../components/CustomButton';


const InitialPage = (props) => {

    const { navigation, afterLoginsuccess, ResponseOTP } = props;

    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", backAction)
        return () =>
            BackHandler.removeEventListener("hardwareBackPress", backAction)
    }, []);

    useEffect(() => {
        if (ResponseOTP?.userid) {
            navigation.push('Home');
        }
    }, [])

    const backAction = () => {
        Alert.alert("Hold on!", "Are you sure you want exit to the App?", [
            { text: "No", onPress: () => null },
            { text: "YES", onPress: () => BackHandler.exitApp() }
        ]);
        return true;
    };

    const handleExit = () => {
        Alert.alert("Hold on!", "Are you sure you want exit to the App?", [
            { text: "No", onPress: () => null },
            { text: "YES", onPress: () => BackHandler.exitApp() }
        ]);
    }


    return (
        <View style={styles.container}>
            {(afterLoginsuccess?.successLogin === false) ? < CustomSubmit type='Registration' onPress={() => navigation.push('Registration')} /> :
                <CustomSubmit type='OTP' onPress={() => navigation.push('OTP')} />}
            <CustomReset type='Exit' {...props} onPress={() => handleExit()} />
        </View>
    )
}

const mapStatetoProps = (state) => {
    const { afterLoginsuccess } = state.local;
    const { ResponseOTP } = state.presistLocal;

    return {
        afterLoginsuccess,
        ResponseOTP
    }
}


export default connect(mapStatetoProps, null)(InitialPage);