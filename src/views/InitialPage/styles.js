import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn_container: {
    display: "flex",
    alignSelf: "center",
    flexDirection: "column",
    justifyContent: "center"
  },
  btn: {
    width: 200,
    height: 30,
    backgroundColor: "#06b6d4",
    borderColor: "#a3a3a3",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 3,
    margin: 10,
  },
  exit: {
    width: 200,
    height: 30,
    backgroundColor: "red",
    borderColor: "#a3a3a3",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 3,
    margin: 10,
  },
  text: {
    fontSize: 15,
    justifyContent: "center",
    // paddingTop:4,
    alignSelf: "center",
    color: "#fff"
  },
})

export default styles;