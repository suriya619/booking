import React, { useState, useRef } from "react";
import { TextInput, View, Platform, ScrollView, Text, TouchableOpacity, Image, Modal, StyleSheet, Keyboard, TouchableWithoutFeedback } from 'react-native';
import { connect } from "react-redux";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import moment from 'moment';
import ImageViewer from 'react-native-image-zoom-viewer';

// Components
import { CustomSubmit, CustomReset } from "../../components/CustomButton";
import CustomFieldWithAction from "../../components/CustomFieldWithAction";
import CustomTextField from "../../components/CustomTextField";
import CustomSearchColumn from "../../components/CustomSearchColumn";
import CustomQRScanner from "../../components/CustomQRScanner";

// Stylesheet
import styles from "./styles";
import imageViewStyles from "../../components/styles"

// Utils
import { chooseFiles } from "../../utils/Helper";

// Redux :: Actions
import * as callActions from "../../redux/actions/globalActions";
import * as calllocalActions from "../../redux/actions/localActions";
import { AUTOCOMPLETE_DATA, BOOKING } from "../../redux/types";

const Booking = (props) => {

    const { AutoCompleteData, bookingimgupload, loading, globalaction, ToggleSideMenu } = props;

    const pincodeRef = useRef(null);
    const destinationRef = useRef(null);
    const noOfPicRef = useRef(null);
    const qrCodeRef = useRef(null);
    const tempImageObj = {
        customer: "",
        noOfPics: "",
        remarks: "",
        fullImages: "",
        toImages: "",
    }
    const todayDate = moment(new Date).format("DD/MM/YYYY");
    const [imageObject, setImageObj] = useState(tempImageObj);
    const [expandPincode, setExpandPincode] = useState(false);
    const [searchPincode, setSearchPincode] = useState(null);
    const [expandDestination, setExpandDestination] = useState(false);
    const [searchDestination, setSearchDestination] = useState(null);
    const [QRCode, setQRCode] = useState(null);
    const [scan, setScan] = useState(false);
    const [swappingFull, setSwappingFull] = useState(false);
    const [swappingTo, setSwappingTo] = useState(false);
    const [qrArr, setQrArr] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const [popupImage, setPopupImage] = useState(null);
    const [scannedPic, setScannedPic] = useState(null);


    const handleScan = () => {
        setScan(true);
    };

    const onSuccess = (e) => {
        if (e?.data) {
            if (Number(imageObject?.noOfPics) >= (Number(qrArr.length + 1))) {
                const findDuplicate = (qrArr || []).find(element => element === e?.data);
                if (findDuplicate) {
                    alert('Duplicate Scan Number');
                    setScan(false);
                }
                else {
                    setQRCode(null);
                    setQrArr([...qrArr, e?.data])
                    setScan(false);
                    setScannedPic(String(qrArr.length + 1));
                }
            } else {
                alert('Reached scanning limit');
                setScan(false);
            }
        }
    };

    const handleChangeQR = (data) => {
        setQRCode(data);
    };

    const handleChangeValue = (value, type) => {
        setImageObj({ ...imageObject, [type]: value });
        setExpandPincode(false);
        setExpandDestination(false);
    };

    const handleSearchColumn = (txt, key) => {
        if (key === ("pincode")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCHPIN",
                p001: txt,
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "SEARCHPIN");
            setSearchPincode(txt);
            setExpandPincode(true);
            setExpandDestination(false);
        } else if (key === ("destination")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCHDEST",
                p001: txt,
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "SEARCHDEST");
            setSearchDestination(txt);
            setExpandDestination(true);
            setExpandPincode(false);
        }
    };

    const handleSelectColumn = (sel, key) => {
        if (key === ("pincode")) {
            setSearchPincode(sel?.name);
            setSearchDestination(sel?.value);
            setExpandPincode(false);
        }
        else if (key === ("destination")) {
            setSearchDestination(sel?.value);
            setSearchPincode(sel?.name);
            setExpandDestination(false);
        }
    };

    const handleCancel = (data) => {
        (data === 'fullImages') && [setSwappingFull(false), setImageObj({ ...imageObject, fullImages: "" })];
        (data === 'toImages') && [setSwappingTo(false), , setImageObj({ ...imageObject, toImages: "" })];
    };

    const submitHandler = async () => {
        loading(true);
        const imgArr = [];
        const resArr = [];
        imgArr.push(imageObject?.fullImages, imageObject?.toImages);
        for (let i = 0; i < imgArr.length; i++) {
            const formData = new FormData();
            formData.append("interBranchCode", "HEAD");
            formData.append("consinmentdate", todayDate);
            formData.append("formtype", "Booking");
            formData.append("file", {
                uri: imgArr[i],
                type: "image/jpeg",
                name: imgArr[i]
            });
            const fetch_API = await bookingimgupload(formData, 'booking');
            if (fetch_API?.status === 201) {
                resArr.push(fetch_API?.data?.data);
            }
        }

        if (resArr && resArr.length && searchDestination && searchPincode && imageObject?.customer && imageObject?.noOfPics && scannedPic && qrArr && imageObject?.remarks) {
            const reqObj = {
                objName: "consignment",
                cmod: "QRBOOKING",
                p001: "PON",
                p002: "PON",
                p003: searchDestination,
                p004: searchPincode,
                p005: "",
                p006: imageObject?.customer,
                p007: resArr[0]?.folder + '/' + resArr[0]?.filename,
                p008: ((resArr[1] != undefined) ? (resArr[1]?.folder + '/' + resArr[1]?.filename) : ("")),
                p009: imageObject?.noOfPics,
                p010: scannedPic,
                p011: qrArr.join(),
                p012: imageObject?.remarks,
            }
            const fetch_API = await globalaction(reqObj, BOOKING, null);
            if (fetch_API?.data?.data.length >= 0) {
                setImageObj(tempImageObj);
                setSwappingFull(false);
                setSwappingTo(false);
                setSearchDestination(null);
                setSearchPincode(null);
                setQrArr([]);
                setQRCode(null);
                loading(false);
                setScannedPic(null);
            } else {
                alert(fetch_API);
                loading(false);
            }
        } else {
            alert('Invalid Inputs');
            loading(false);
        }
    };

    const resetHandler = () => {
        setImageObj(tempImageObj);
        setSwappingFull(false);
        setSwappingTo(false);
        setSearchDestination(null);
        setSearchPincode(null);
        setQrArr([]);
        setQRCode(null);
        loading(false);
        setScannedPic(null);
        setExpandPincode(false);
        setExpandDestination(false);
    }


    return (
        <>
            <ScrollView showsHorizontalScrollIndicator={false} >
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <View style={styles.flex}>
                        <CustomTextField placeholder="Customer" {...props} onChangeText={(e) => handleChangeValue(e, "customer")} value={imageObject?.customer}
                            returnKeyType="next"
                            onSubmitEditing={() => pincodeRef?.current?.focus()}
                            blurOnSubmit={false} />

                        <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"pincode"} placeHolder="Pincode" keyboardType="numeric"
                            handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={searchPincode} show={expandPincode} data={AutoCompleteData?.Pincode?.data}
                            ref={pincodeRef}
                            returnKeyType="next"
                            onSubmitEditing={() => destinationRef?.current?.focus()}
                            blurOnSubmit={false} />
                        <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"destination"} placeHolder="Destination"
                            handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={searchDestination} show={expandDestination} data={AutoCompleteData?.Destination?.data}
                            ref={destinationRef}
                            blurOnSubmit={false}
                            returnKeyType="next"
                            onSubmitEditing={() => noOfPicRef?.current?.focus()} />
                        <View style={styles.gridview}>
                            <TextInput
                                placeholder='No of Pcs'
                                placeholderTextColor="#a3a3a3"
                                style={styles.grid}
                                keyboardType={Platform.OS === 'android' ? "numeric" : "number-pad"}
                                onChangeText={(e) => handleChangeValue(e, "noOfPics")}
                                value={imageObject?.noOfPics}
                                ref={noOfPicRef}
                                blurOnSubmit={false}
                                returnKeyType="next"
                                onSubmitEditing={() => qrCodeRef?.current?.focus()} />
                            <TextInput
                                placeholder='Scanned Pcs'
                                editable={false}
                                placeholderTextColor="#a3a3a3"
                                style={styles.grid}
                                value={scannedPic} />
                        </View>

                        <CustomTextField
                            placeholder='QR Code'
                            editable={false}
                            placeholderTextColor="#a3a3a3"
                            style={styles.input}
                            value={qrArr.join()} />

                        <CustomFieldWithAction placeholder="QR Code" {...props} value={QRCode} onPress={() => handleScan()} onChange={(e) => handleChangeQR(e)} editable={false} />

                        <CustomTextField placeholder='Remarks' {...props} onChangeText={(e) => handleChangeValue(e, "remarks")} value={imageObject?.remarks} blurOnSubmit={false}
                            returnKeyType="next"
                            ref={qrCodeRef} />

                        <View>
                            <View style={imageViewStyles.dragdrop} >
                                <Text style={Platform.OS === 'ios' ? imageViewStyles.imageText_ios : imageViewStyles.imageText}>Full Images</Text>
                                {swappingFull ? <View >
                                    <TouchableOpacity onPress={() => [setModalVisible(true), setPopupImage(imageObject?.fullImages)]}>
                                        <Image style={imageViewStyles.imagesUpload}
                                            source={{ uri: imageObject?.fullImages }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <MaterialIcons name="cancel" color="red" size={24} style={imageViewStyles.multiImageCancelIcon} onPress={() => handleCancel('fullImages')} />
                                </View> : null}
                                {!swappingFull ? <View style={{ flexDirection: 'row' }}>
                                    <View style={{ paddingRight: 20 }}>
                                        <MaterialIcons name="photo-camera" color="gray" size={24} onPress={() =>
                                            [chooseFiles('Images', "fullImages", imageObject, setImageObj), setSwappingFull(true)]} />
                                    </View>
                                    <View style={{ paddingLeft: 20 }}>
                                        <MaterialIcons name="insert-photo" color="gray" size={24} onPress={() =>
                                            [chooseFiles('Files', "fullImages", imageObject, setImageObj), setSwappingFull(true)]} />
                                    </View>
                                </View> : null}
                            </View>
                            <View style={imageViewStyles.dragdrop} >
                                <Text style={Platform.OS === 'ios' ? imageViewStyles.imageText_ios : imageViewStyles.imageText}>To Images</Text>
                                {swappingTo ? <View>
                                    <TouchableOpacity onPress={() => [setModalVisible(true), setPopupImage(imageObject?.toImages)]}>
                                        <Image style={imageViewStyles.imagesUpload}
                                            source={{ uri: imageObject?.toImages }}>
                                        </Image>
                                    </TouchableOpacity>
                                    <MaterialIcons name="cancel" color="red" size={24} style={imageViewStyles.multiImageCancelIcon} onPress={() => handleCancel('toImages')} />
                                </View> : null}
                                {!swappingTo ? <View style={{ flexDirection: 'row' }}>
                                    <View style={{ paddingRight: 20 }}>
                                        <MaterialIcons name="photo-camera" color="gray" size={24} onPress={() =>
                                            [chooseFiles('Images', "toImages", imageObject, setImageObj), setSwappingTo(true)]} />
                                    </View>
                                    <View style={{ paddingLeft: 20 }}>
                                        <MaterialIcons name="insert-photo" color="gray" size={24} onPress={() =>
                                            [chooseFiles('Files', "toImages", imageObject, setImageObj), setSwappingTo(true)]} />
                                    </View>
                                </View> : null}
                            </View>
                        </View>
                        <View style={styles.btn_container}>
                            <CustomSubmit type={"Submit"} onPress={submitHandler} />
                            <CustomReset type={"Reset"} onPress={resetHandler} {...props} />
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </ScrollView>
            {scan && <CustomQRScanner onSuccess={onSuccess} />}

            {modalVisible &&
                <View style={modelStyle.centeredView}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => { setModalVisible(!modalVisible) }} >
                        <ImageViewer imageUrls={[{ url: popupImage }]} enableSwipeDown={true} saveToLocalByLongPress={false}
                            loadingRender={() => <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>Loading ...</Text>} />
                    </Modal>
                </View>
            }
        </>
    )
}

const mapStatetoProps = (state) => {
    const { AutoCompleteData } = state.global;
    const { ResponseOTP } = state.presistLocal;
    const { ToggleSideMenu, CustomSearchHides } = state.local;
    return {
        AutoCompleteData, ResponseOTP, ToggleSideMenu, CustomSearchHides
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
        bookingimgupload: (reqObj, key) => { return dispatch(callActions.POST_ImageUpload(reqObj, key)) },
        loading: (reqObj) => { return dispatch(calllocalActions.Loading(reqObj)) },
    }
}


const modelStyle = StyleSheet.create({
    centeredView: {
        flex: 1,
        flexDirection: 'column'
    },
});

export default connect(mapStatetoProps, mapDispatchToProps)(Booking);