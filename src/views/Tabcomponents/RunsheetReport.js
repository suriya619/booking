import React, { useState } from 'react';
import { View, TouchableOpacity, Text, FlatList, Dimensions } from 'react-native';
import { Menu, Pressable } from "native-base";
import { connect } from 'react-redux';
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Device from 'react-native-device-detection';

// Components
import { CustomCancel } from '../../components/CustomButton';
import CustomAccordion from '../../components/CustomAccordion';

// Stylesheet
import styles from '../../components/styles';


const RunsheetReport = (props) => {
    const { BothReport } = props;

    const [showPopover, setShowPopover] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [id, setId] = useState(null);
    const [poupselect, setPopupSelect] = useState(null);
    const arr = [
        { key: 0, name: "Open", icon: <AntDesign name="folderopen" size={16} color="#000" /> },
        { key: 1, name: "Close", icon: <AntDesign name="folder1" size={16} color="#000" /> },
        { key: 2, name: "Print", icon: <MaterialIcons name="print" size={18} color="#000" /> },
        { key: 3, name: "Image", icon: <MaterialIcons name="cloud-upload" size={18} color="#000" /> },
        { key: 4, name: "Image", icon: <MaterialIcons name="cloud-download" size={18} color="#000" /> },
        { key: 5, name: "Details", icon: <FontAwesome name="angle-double-down" size={19} color="#000" /> }
    ];

    const handleSelect = (id) => {
        setId(id)
        setShowModal(!showModal)
    };

    const handleMenu = () => {
        setShowPopover(!showPopover)
    };

    const renderItem = ({ item, index }) => {
        return <>
            <View style={styles.runsheetAccordian} key={index}>
                <TouchableOpacity onPress={() => handleSelect(index)} style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <View style={{ justifyContent: "flex-start", flexDirection: "row", }}>
                        <View style={{
                            width: 8,
                            backgroundColor: showModal === true && index === id ? "yellow" : "#6ee7b7",
                        }} />
                        <View style={styles.list_view}>
                            <View style={styles.list_vw}>
                                <AntDesign
                                    name={(showModal === true && index === id) ? "export" : "checksquare"}
                                    size={18}
                                    color={(showModal === true && index === id) ? "#fdba74" : "#6ee7b7"}
                                    style={{ paddingRight: 5 }}
                                />
                                <Text style={styles.name}>PC Code: {item.pccode}</Text>
                            </View>
                            <Text style={styles.content}>DRS No: {item.drsno}</Text>
                        </View>
                        <View style={styles.list_view}>
                            <Text style={styles.content}>Date: {item.drsdate}</Text>
                            <Text style={styles.content}>Total_Consignment: {item.total}</Text>
                        </View>
                    </View>

                    <TouchableOpacity style={{ marginTop: 10 }}>
                        <Menu style={styles.primaryColor} closeOnSelect={false} trigger={triggerProps => {
                            return (
                                <View onPress={() => handleMenu(index)}>
                                    <Pressable accessibilityLabel="More options menu" {...triggerProps} >
                                        <MaterialIcons name={"more-vert"} size={20} color="#000" />
                                    </Pressable>
                                </View>
                            )
                        }}>
                            {(arr || []).map((menuArr, index) => {
                                return (
                                    <View key={menuArr.key}>
                                        <Menu.Item onPress={() => setPopupSelect(index)} style={((poupselect === index) ? (styles.popupselect) : (styles.popupnotselect))}>{menuArr.icon}
                                            <Text style={((poupselect === index) ? (styles.popuptextselect) : (styles.popuptextnotselect))} >{menuArr.name}</Text> </Menu.Item>
                                    </View>)
                            })}
                        </Menu>
                    </TouchableOpacity>
                </TouchableOpacity >
                {((index === id) && showModal) && <CustomAccordion open={showModal} eleId={id} name="Runsheet" data={item} />}
            </View >
        </>
    };

    return (
        <>
            <View style={styles.flex}>
                <FlatList
                    data={BothReport?.RunReport?.data?.data || []}
                    renderItem={renderItem}
                    style={{
                        height: Dimensions.get('window').height,
                        top: Device.isTablet ? 10 : 2,
                        width: Dimensions.get('window').width,
                        backgroundColor: "transparent",
                        marginTop: 20,
                        marginBottom: 60
                    }}
                />
            </View>
            <View style={{ alignSelf: "center", flexDirection: "column", width: Device.isTablet ? "70%" : '95%' }}>
                <CustomCancel type={"Back"} {...props} />
            </View>
        </>
    );
}

const mapStateToProps = (state) => {
    const { BothReport } = state.global;
    return { BothReport }
}

export default connect(mapStateToProps)(RunsheetReport);
