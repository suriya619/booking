import React, { useState } from 'react';
import { View, useWindowDimensions } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

// Components
import CustomAddImage from '../../components/CustomAddImage';

// Utils
import * as renderHieght from '../../utils/constants';


const ImageCapture = (props) => {

    const [index, setIndex] = React.useState(0);
    const layout = useWindowDimensions();
    const [routes] = useState([
        { key: 'first', title: 'Barcode' },
        { key: 'second', title: 'Manual' },
    ]);


    //--------------main Tab
    const renderTabBar = props => (
        <TabBar {...props} style={{ backgroundColor: '#06b6d4' }} />
    );
    const FirstRoute = () => (
        <CustomAddImage {...props} />
    );
    const SecondRoute = () => (
        <View></View>
    );
    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute,
    });


    return (
        <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            renderTabBar={renderTabBar}
            onIndexChange={setIndex}
            initialLayout={{ width: layout.width }}
        />
    );
}

export default ImageCapture;