import React, { useEffect, useState, useRef } from "react";
import { TextInput, View, Text, Platform, TouchableOpacity, ScrollView, Pressable, Keyboard, TouchableWithoutFeedback } from 'react-native';
import Device from "react-native-device-detection";
import AntDesign from "react-native-vector-icons/AntDesign";
import { connect } from "react-redux";

// Components
import { CustomSubmit } from "../../components/CustomButton";
import CustomSearchColumn from "../../components/CustomSearchColumn";

// Stylesheet
import styles from "./styles";
import dropdownStyle from "../../views/Registration/styles";

// Redux :: Actions
import * as callActions from "../../redux/actions/globalActions";
import { AUTOCOMPLETE_DATA, PRODUCTTYPE_CALC } from "../../redux/types";




const RateCalculator = (props) => {

    const { producttypecalc, autoCompleteData, AutoCompleteData, globalaction } = props;

    const destinationRef = useRef(null);
    const weightRef = useRef(null);
    const initObj = {
        pincode: "",
        destination: "",
        weight: "",
        productType: "",
        productKey: "",
    }
    const [calcDropDown, setCalcDropDown] = useState([])
    const [open, setOpen] = useState(false);
    const [amount, setAmount] = useState(null);
    const [rateCalc, setRateCalc] = useState(initObj);
    const [expandPincode, setExpandPincode] = useState(false);
    const [expandDestination, setExpandDestination] = useState(false);

    useEffect(() => {
        dropDownData()
    }, [])


    const dropDownData = async () => {
        const reqObj = {
            objName: "consignment",
            cmod: "GETCONSINMENTTYPE",
            p001: "PON"
        }
        const fetch_API = await globalaction(reqObj, PRODUCTTYPE_CALC, null);
        if (fetch_API?.data.data.length > 1) {
            setCalcDropDown(fetch_API?.data?.data);
        }
    };
    const handelDropDown = () => {
        setOpen(!open);
    };
    const selectHandler = (data) => {
        setRateCalc({ ...rateCalc, productType: data?.name, productKey: data?.value })
        setOpen(!open);
    };
    const handleSearchColumn = (txt, key) => {
        if (key === ("pincode")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCHPIN",
                p001: txt,
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "SEARCHPIN");
            setRateCalc({ ...rateCalc, pincode: txt })
            setExpandPincode(true);
        } else if (key === ("destination")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCHDEST",
                p001: txt,
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "SEARCHDEST");
            setRateCalc({ ...rateCalc, destination: txt })
            setExpandDestination(true);
        }
    };
    const handleSelectColumn = (sel, key) => {
        if (key === ("pincode")) {
            setRateCalc({ ...rateCalc, pincode: sel?.name, destination: sel?.value })
            setExpandPincode(false);
        }
        else if (key === ("destination")) {
            setRateCalc({ ...rateCalc, destination: sel?.value, pincode: sel?.name })
            setExpandDestination(false);
        }
    };
    const handleSubmit = async () => {
        if (rateCalc.pincode !== "" && rateCalc.destination !== "" && rateCalc.weight !== "" && rateCalc.productType !== "") {
            const reqObj = {
                objName: "consignment",
                cmod: "RATECALC",
                p001: "PON",
                p002: rateCalc?.pincode,
                p003: rateCalc?.destination,
                p004: rateCalc?.weight,
                p005: rateCalc?.productKey
            }
            const fetch_API = await globalaction(reqObj, PRODUCTTYPE_CALC, null);
            if (fetch_API?.data?.data.length > 0) {
                setAmount(fetch_API?.data?.data[0].Amount);
                setRateCalc(initObj);
            }
        }
    }

    return (
        <>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <View style={styles.flex}>
                    <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"pincode"} placeHolder="Pincode" keyboardType="numeric"
                        handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={rateCalc?.pincode} show={expandPincode} data={AutoCompleteData?.Pincode?.data}
                        onSubmitEditing={() => destinationRef?.current?.focus()} />
                    <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"destination"} placeHolder="Destination"
                        handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={rateCalc?.destination} show={expandDestination} data={AutoCompleteData?.Destination?.data}
                        ref={destinationRef} returnKeyType="next" onSubmitEditing={() => weightRef?.current?.focus()} blurOnSubmit={false} />
                    <View >
                        <Text style={styles.rate_txt}>Weight</Text>
                        <TextInput placeholder="Enter Weight Kg" keyboardType="Numeric" style={styles.inputField}
                            onChange={(e) => setRateCalc({ ...rateCalc, weight: e?.nativeEvent?.text })} value={rateCalc?.weight} ref={weightRef}
                            blurOnSubmit={false} />
                    </View>
                    <Pressable style={dropdownStyle.field_with_icon} onPress={(e) => handelDropDown(e)}>
                        <TextInput placeholderTextColor="#a3a3a3" editable={false} placeholder="Product Type" value={rateCalc?.productType} style={dropdownStyle.input} />
                        <TouchableOpacity style={styles.icon}>
                            <AntDesign name="caretdown" size={Platform.OS === 'ios' ? 12 : 10} color="#a3a3a3" />
                        </TouchableOpacity>
                    </Pressable>
                    {open && <View style={dropdownStyle.dropDownScrollView}>
                        {(calcDropDown || []).map((drop, index) => {
                            return (
                                <TouchableOpacity key={index} style={{ padding: 5, }} onPress={() => selectHandler(drop)}>
                                    <Text>{drop?.name}</Text>
                                </TouchableOpacity>
                            )
                        })}
                    </View>}
                    <View>
                        <Text style={styles.rate_txt}>Amount (Inclusive of Tax)</Text>
                        <TextInput editable={false} placeholder="0.00" style={styles.inputField} value={amount} />
                    </View>
                </View>
            </TouchableWithoutFeedback>
            <View style={styles.btn_container}>
                <CustomSubmit type="Get Amount" onPress={() => handleSubmit()} />
            </View>
        </>
    )
}

const mapStatetoProps = (state) => {
    const { AutoCompleteData } = state.global;
    return {
        AutoCompleteData,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
    }
}

export default connect(mapStatetoProps, mapDispatchToProps)(RateCalculator); 