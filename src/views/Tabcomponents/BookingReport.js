import React, { useState } from 'react';
import { View, TouchableOpacity, Text, FlatList, Pressable, Dimensions, StyleSheet, Modal } from 'react-native';
import Device from "react-native-device-detection";
import { connect } from 'react-redux';
import AntDesign from "react-native-vector-icons/AntDesign";
import ImageViewer from 'react-native-image-zoom-viewer';

// Components
import CustomAccordian from "../../components/CustomAccordion";
import { CustomSubmit } from '../../components/CustomButton';

// Stylesheet
import styles from '../../components/styles';

// Redux :: Actions
import * as calllocalActions from "../../redux/actions/localActions";


const BookingReport = (props) => {

    const { navigation, BothReport } = props;

    const [showModal, setShowModal] = useState(false);
    const [eleId, setEleId] = useState(null);
    const [popupImg, setPopupImg] = useState(null);
    const [modalVisible, setModalVisible] = useState(false);


    const handleSelectItem = (id) => {
        setEleId(id);
        setShowModal(!showModal);
    };

    const renderItem = ({ item, index }) => {
        return <>
            <View style={styles.accordionContainer} key={index.toString()}>
                <TouchableOpacity onPress={() => handleSelectItem(index)} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ justifyContent: "flex-start", flexDirection: "row", }}>
                        <View style={{
                            width: 8,
                            backgroundColor: showModal === true && index === eleId ? "yellow" : "#6ee7b7",
                        }} />
                        <View style={{ paddingLeft: 5 }}>
                            <Text style={styles.accordionHeader}>Consignment: <Text>{item?.consinmentno}</Text></Text>
                            <Text style={styles.accordionSubHeader}>{item?.origion} to {item?.destination} Amount: <Text>{item?.amount}</Text></Text>
                        </View>
                    </View>

                    <Pressable style={styles.field_with_icon}>
                        <Text style={styles.more}>More</Text>
                        <AntDesign name={((item?.consinmentno === eleId) && showModal) ? 'caretup' : 'caretdown'} size={10} color="#06b6d4" />
                    </Pressable>
                </TouchableOpacity>
                {((index === eleId) && showModal) &&
                    <CustomAccordian open={showModal} eleId={eleId} data={item} setPopupImg={setPopupImg} setModalVisible={setModalVisible} name="Book" />}
            </View>
        </>
    };

    return (
        <>
            <View style={styles.flex}>
                <FlatList
                    data={BothReport?.BookReport?.data?.data || []}
                    renderItem={renderItem}
                    style={{
                        height: Dimensions.get('window').height,
                        width: Dimensions.get('window').width,
                        marginBottom: 60
                    }}
                />
            </View>
            {modalVisible &&
                <View style={modelStyle.centeredView}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => { setModalVisible(!modalVisible) }}>
                        <ImageViewer imageUrls={[{ url: popupImg }]} enableSwipeDown={false} saveToLocalByLongPress={false} failImageSource={() => alert('Error Image')}
                            loadingRender={() => <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>Loading ...</Text>} />
                    </Modal>
                </View>
            }

            <View style={{ alignSelf: "center", flexDirection: "column", width: Device.isTablet ? "70%" : '95%' }}>
                <CustomSubmit type="Back" {...props} onPress={() => navigation.pop()} />
            </View>

        </>
    );
}

const mapStateToProps = (state) => {
    const { BothReport } = state.global;

    return {
        BothReport
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loading: (reqObj) => { return dispatch(calllocalActions.Loading(reqObj)) },
    }
}

const modelStyle = StyleSheet.create({
    centeredView: {
        flex: 1,
        flexDirection: 'column',
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(BookingReport);
