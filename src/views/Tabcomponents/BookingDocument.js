import React, { useState, useRef } from "react";
import { chooseFiles } from "../../utils/Helper";
import { connect } from "react-redux";
import { View, Text, Platform, ScrollView, Modal, StyleSheet, Image, TouchableOpacity, Keyboard, TouchableWithoutFeedback, SafeAreaView } from 'react-native';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import moment from 'moment';
import ImageViewer from 'react-native-image-zoom-viewer';

// Components
import { CustomSubmit, CustomReset } from "../../components/CustomButton";
import CustomTextField from "../../components/CustomTextField";
import CustomSearchColumn from "../../components/CustomSearchColumn";

// Stylesheet
import styles from "./styles";
import imageViewStyles from "../../components/styles"

// Redux :: Actions
import * as callActions from "../../redux/actions/globalActions";
import * as calllocalActions from "../../redux/actions/localActions";
import { AUTOCOMPLETE_DATA } from "../../redux/types";


const BookingDocument = (props) => {

    const { bookingDocument, AutoCompleteData, bookdocimageupload, loading, globalaction, CustomSearchHides, route } = props;

    const customerRef = useRef(null);
    const mobileRef = useRef(null);
    const pincodeRef = useRef(null);
    const destinationRef = useRef(null);

    const tempObj = {
        branch: "",
        customer: "",
        mobile: "",
        pincode: "",
        destination: "",
        fullImage: "",
        toImage: "",
    };
    const reqObjectData = {
        charger: "00.00",
        tax: "00.00",
        amount: "00.00",
    };
    const tempImageObj = {
        fullImages: "",
        toImages: "",
    }

    const todayDate = moment(new Date).format("YYYY/MM/DD");
    const [bookingDoc, setBookingDoc] = useState(tempObj);
    const [bookingDocData, setBookingDocData] = useState(reqObjectData);
    const [imageObject, setImageObj] = useState(tempImageObj);
    const [modalVisible, setModalVisible] = useState(false);
    const [expandbranch, setExpandBranch] = useState(false);
    const [searchBranch, setSearchBranch] = useState(null);
    const [expandPincode, setExpandPincode] = useState(false);
    const [searchPincode, setSearchPincode] = useState(null);
    const [expandDestination, setExpandDestination] = useState(false);
    const [searchDestination, setSearchDestination] = useState(null);
    const [swappingFull, setSwappingFull] = useState(false);
    const [swappingTo, setSwappingTo] = useState(false);
    const [popupImage, setPopupImage] = useState(null);


    const handleSearchColumn = (txt, key) => {
        if (key === ("branch")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCBRANCH",
                p001: txt,
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "SEARCBRANCH");
            setSearchBranch(txt);
            setExpandBranch(true);
            setExpandPincode(false);
            setExpandDestination(false);
        } else if (key === ("pincode")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCHPIN",
                p001: txt,
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "SEARCHPIN");
            setSearchPincode(txt);
            setExpandPincode(true);
            setExpandBranch(false);
            setExpandDestination(false);
        } else if (key === ("destination")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCHDEST",
                p001: txt,
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "SEARCHDEST");
            setSearchDestination(txt);
            setExpandDestination(true);
            setExpandBranch(false);
            setExpandPincode(false);
        }
    };

    const handleSelectColumn = (sel, key) => {
        if (key === ("branch")) {
            setSearchBranch(sel?.value);
            setExpandBranch(false);
        }
        else if (key === ("pincode")) {
            setSearchPincode(sel?.name);
            setSearchDestination(sel?.value);
            setExpandPincode(false);
        }
        else if (key === ("destination")) {
            setSearchDestination(sel?.value);
            setSearchPincode(sel?.name);
            setExpandDestination(false);
        }
        setBookingDoc({ ...bookingDoc, [key]: sel?.value });
    };


    const handleChangeValue = (value, type) => {
        setBookingDoc({ ...bookingDoc, [type]: value });
        setExpandBranch(false);
        setExpandPincode(false);
        setExpandDestination(false);
    };

    const handleCancel = (data) => {
        (data === 'fullImages') && [setSwappingFull(false), setImageObj({ ...imageObject, fullImages: "" })];
        (data === 'toImages') && [setSwappingTo(false), , setImageObj({ ...imageObject, toImages: "" })];
    };

    const submitHandler = async () => {
        loading(true);
        const imgArr = [];
        const resArr = [];
        imgArr.push(imageObject?.fullImages, imageObject?.toImages);
        for (let i = 0; i < imgArr?.length; i++) {
            const formData = new FormData();
            formData.append("interBranchCode", "HEAD");
            formData.append("consinmentdate", todayDate);
            formData.append("formtype", "Booking");
            formData.append("file", {
                uri: imgArr[i],
                type: "image/jpeg",
                name: imgArr[i]
            });
            const fetch_API = await bookdocimageupload(formData, "Booking");
            if (fetch_API?.status === 201) {
                resArr.push(fetch_API?.data?.data);
            } else {
                alert(fetch_API);
                loading(false);
            }
        }
        if (resArr[0] != undefined && searchBranch && searchDestination && searchPincode && bookingDoc?.mobile && bookingDoc?.customer) {
            const reqObj = {
                objName: "consignment",
                cmod: "BOOKING",
                p001: searchBranch,
                p002: "PON",
                p003: searchDestination,
                p004: searchPincode,
                p005: bookingDoc?.mobile,
                p006: bookingDoc?.customer,
                p007: resArr[0]?.folder + '/' + resArr[0]?.filename,
                p008: ((resArr[1] != undefined) ? (resArr[1]?.folder + '/' + resArr[1]?.filename) : ("")),
            }
            const fetch_API = await (bookingDocument(reqObj));
            if (fetch_API["data"]?.data?.length > 0) {
                const temp = {
                    charger: fetch_API?.data?.data[0]?.charges,
                    tax: fetch_API?.data?.data[0]?.servicetax,
                    amount: fetch_API?.data?.data[0]?.amount,
                };
                setBookingDocData(temp);
                setBookingDoc(tempObj);
                setSearchDestination('');
                setSearchPincode('');
                setSearchBranch('');
                setImageObj(tempImageObj);
                setSwappingTo(false);
                setSwappingFull(false);
                loading(false);
            } else {
                alert(fetch_API);
                loading(false);
            }
        } else {
            loading(false);
            alert('Upload Image Again');
        }
    };

    const resetHandler = () => {
        loading(true);
        setBookingDoc(tempObj);
        setSearchDestination('');
        setSearchPincode('');
        setSearchBranch('');
        setImageObj(tempImageObj);
        setSwappingTo(false);
        setSwappingFull(false);
        setExpandBranch(false);
        setExpandPincode(false);
        setExpandDestination(false);
        loading(false);
        setBookingDocData(reqObjectData);
    }


    return (
        <>
            <SafeAreaView>
                <ScrollView showsHorizontalScrollIndicator={false}>
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <View>
                            <View style={styles.flex}>
                                <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"branch"} placeHolder="HO"
                                    handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={searchBranch} show={expandbranch} data={AutoCompleteData?.Branch?.data}
                                    onSubmitEditing={() => customerRef?.current?.focus()} />

                                <CustomTextField  {...props}
                                    onChangeText={(e) => handleChangeValue(e, "customer")}
                                    value={bookingDoc?.customer}
                                    placeholder='Customer'
                                    ref={customerRef}
                                    returnKeyType="next"
                                    onSubmitEditing={() => mobileRef?.current?.focus()}
                                    blurOnSubmit={false}
                                />

                                <CustomTextField   {...props}
                                    onChangeText={(e) => handleChangeValue(e, "mobile")}
                                    value={bookingDoc?.mobile}
                                    placeholder='Mobile'
                                    keyboardType={Platform.OS === 'android' ? "numeric" : "number-pad"}
                                    maxLength={10}
                                    ref={mobileRef}
                                    returnKeyType="next"
                                    onSubmitEditing={() => pincodeRef?.current?.focus()}
                                    blurOnSubmit={false}
                                />

                                <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"pincode"} placeHolder="Pincode" keyboardType="numeric"
                                    handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={searchPincode} show={expandPincode} data={AutoCompleteData?.Pincode?.data}
                                    ref={pincodeRef} returnKeyType="next" onSubmitEditing={() => destinationRef?.current?.focus()} blurOnSubmit={false} />

                                <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"destination"} placeHolder="Destination"
                                    handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={searchDestination} show={expandDestination} data={AutoCompleteData?.Destination?.data}
                                    ref={destinationRef} />

                                <View>
                                    <View style={imageViewStyles.dragdrop} >
                                        <Text style={Platform.OS === 'ios' ? imageViewStyles.imageText_ios : imageViewStyles.imageText}>Full Images</Text>
                                        {swappingFull ? <View >
                                            <TouchableOpacity onPress={() => [setModalVisible(true), setPopupImage(imageObject?.fullImages)]}>
                                                <Image style={imageViewStyles.imagesUpload}
                                                    source={{ uri: imageObject?.fullImages }}>
                                                </Image>
                                            </TouchableOpacity>
                                            <MaterialIcons name="cancel" color="red" size={24} style={imageViewStyles.multiImageCancelIcon} onPress={() => handleCancel('fullImages')} />
                                        </View> : null}
                                        {!swappingFull ? <View style={{ flexDirection: 'row' }}>
                                            <View style={{ paddingRight: 20 }}>
                                                <MaterialIcons name="photo-camera" color="gray" size={24} onPress={() =>
                                                    [chooseFiles('Images', "fullImages", imageObject, setImageObj), setSwappingFull(true)]} />
                                            </View>
                                            <View style={{ paddingLeft: 20 }}>
                                                <MaterialIcons name="insert-photo" color="gray" size={24} onPress={() =>
                                                    [chooseFiles('Files', "fullImages", imageObject, setImageObj), setSwappingFull(true)]} />
                                            </View>
                                        </View> : null}
                                    </View>
                                    <View style={imageViewStyles.dragdrop} >
                                        <Text style={Platform.OS === 'ios' ? imageViewStyles.imageText_ios : imageViewStyles.imageText}>To Images</Text>
                                        {swappingTo ? <View>
                                            <TouchableOpacity onPress={() => [setModalVisible(true), setPopupImage(imageObject?.toImages)]}>
                                                <Image style={imageViewStyles.imagesUpload}
                                                    source={{ uri: imageObject?.toImages }}>
                                                </Image>
                                            </TouchableOpacity>
                                            <MaterialIcons name="cancel" color="red" size={24} style={imageViewStyles.multiImageCancelIcon} onPress={() => handleCancel('toImages')} />
                                        </View> : null}
                                        {!swappingTo ? <View style={{ flexDirection: 'row' }}>
                                            <View style={{ paddingRight: 20 }}>
                                                <MaterialIcons name="photo-camera" color="gray" size={24} onPress={() =>
                                                    [chooseFiles('Images', "toImages", imageObject, setImageObj), setSwappingTo(true)]} />
                                            </View>
                                            <View style={{ paddingLeft: 20 }}>
                                                <MaterialIcons name="insert-photo" color="gray" size={24} onPress={() =>
                                                    [chooseFiles('Files', "toImages", imageObject, setImageObj), setSwappingTo(true)]} />
                                            </View>
                                        </View> : null}
                                    </View>
                                </View>

                                <View style={styles.view1}>
                                    <Text style={styles.drstext} >Charges: {bookingDocData?.charger}</Text>
                                    <Text style={styles.weight}>Service Tax: {bookingDocData?.tax}</Text>
                                </View>
                                <Text style={styles.text}>Total Amount: {bookingDocData?.amount}</Text>
                            </View>
                            <View style={Platform.OS === 'ios' ? styles.buttons : styles.btn_container}>
                                <CustomSubmit type={"Submit"} onPress={submitHandler} />
                                <CustomReset type={"Reset"} onPress={resetHandler} {...props} />
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>

                {modalVisible &&
                    <View style={modelStyle.centeredView}>
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={modalVisible}
                            onRequestClose={() => { setModalVisible(!modalVisible) }}>
                            <ImageViewer imageUrls={[{ url: popupImage }]} enableSwipeDown={true} saveToLocalByLongPress={false}
                                loadingRender={() => <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>Loading ...</Text>} />
                        </Modal>
                    </View>
                }
            </SafeAreaView>
        </>
    )
}
const mapStatetoProps = (state) => {

    const { AutoCompleteData, BookingDocImage } = state.global;
    const { CustomSearchHides } = state.local;

    return {
        AutoCompleteData, BookingDocImage, CustomSearchHides,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
        bookingDocument: (reqObj) => { return dispatch(callActions.POST_BookingDocument(reqObj)) },
        bookdocimageupload: (reqObj, key) => { return dispatch(callActions.POST_ImageUpload(reqObj, key)) },
        loading: (reqObj) => { return dispatch(calllocalActions.Loading(reqObj)) },
    }
}

const modelStyle = StyleSheet.create({
    centeredView: {
        flex: 1,
        flexDirection: 'column',
    },
});
export default connect(mapStatetoProps, mapDispatchToProps)(BookingDocument);