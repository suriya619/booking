import { Platform, StyleSheet } from 'react-native';
import Device from 'react-native-device-detection';


const styles = StyleSheet.create({
    primaryColor: {
        backgroundColor: '#06b6d4',
    },
    flex: {
        flex: 1,
        flexDirection: 'column',
        alignItems: "center",
        justifyContent: "center",
        marginTop: 20
    },
    drstext_ios: {
        fontSize: 14,
        color: "gray",
        fontWeight: "500",
        paddingRight: 120
    },
    drstext: {
        fontSize: 12,
        fontWeight: "500",
        color: "gray"
    },
    datetext: {
        fontSize: 12,
        color: "gray",
        fontWeight: "500"
    },
    datetext_ios: {
        fontSize: 14,
        color: "gray",
        fontWeight: "500",
        paddingLeft: 50
    },
    runsheetContainer: {
        display: "flex",
        flexDirection: 'column',
        alignItems: "center",
        justifyContent: "flex-start",
        top: 0,
        margin: 10,
        backgroundColor: "red"
    },
    gridview: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        width: Device.isTablet ? "70%" : "90%",
        alignSelf: "center",
    },
    grid: {
        width: Platform.OS === 'ios' ? 164 : "50%",
        height: 35,
        fontSize: 14,
        margin: 2,
        padding: 5,
        borderWidth: 1,
        borderColor: "#a3a3a3",
        backgroundColor: "#fff",
        borderRadius: 5
    },
    count: {
        fontSize: 11,
        fontWeight: "700",
        color: "gray",
        display: "flex",
        justifyContent: "flex-start",
        alignSelf: 'flex-start',
        left: 10
    },
    total: {
        fontSize: 14,
        color: "gray",
        fontWeight: "700",
        display: "flex",
        justifyContent: "flex-start",
        alignSelf: "center",
        marginTop: 10,
    },
    inputTextField: {
        width: Device.isTablet ? "70%" : "90%",
        height: 35,
        margin: 2,
        padding: 5,
        borderWidth: 1,
        borderColor: "#a3a3a3",
        backgroundColor: "#fff",
        borderRadius: 5
    },
    field_with_icon: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
    },
    icon: {
        position: "absolute",
        right: 20,
    },
    inputField: {
        borderBottomWidth: 1,
        marginBottom: 5,
        borderBottomColor: "#a3a3a3",
        paddingTop: 10,
        padding: 5,
        fontSize: 12
    },
    container_in: {
        position: "absolute",
        bottom: "35%",
        height: 100,
        padding: 5,
        width: 320,
        borderColor: "#06b6d4",
        borderWidth: 4,
        backgroundColor: "#bae6fd",
        margin: 5
    },
    tabcontainer_ios: {
        backgroundColor: "transparent",
        display: "flex",
        height: Platform.OS === 'ios' ? 320 : 220,
        alignSelf: "center",
        justifyContent: "flex-start",
        marginBottom: 15
    },
    contentAlign: {
        alignItems: "center",
        justifyContent: "flex-start",
        padding: 10,
        marginBottom: 45,
        width: (Device.isTablet ? "70%" : "80%"),
        borderColor: "#06b6d4",
        borderWidth: 4,
        backgroundColor: "#bae6fd",
        margin: 5,
    },
    contentAlign11: {
        justifyContent: "space-between",
        height: 130,
        padding: 10,
        width: 320,
        borderColor: "#06b6d4",
        borderWidth: 4,
        backgroundColor: "#bae6fd",
        marginBottom: 45,
        margin: 5
    },
    container_input: {
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
        width: (Device.isTablet ? "70%" : 320),
        borderColor: "#06b6d4",
        borderWidth: 4,
        backgroundColor: "#bae6fd",
        margin: 5
    },
    manifest_txt: {
        margin: 4,
        color: "gray",
        fontSize: 12
    },
    weight: {
        marginRight: 8,
        marginTop: 4,
        color: "gray",
        fontWeight: "500",
        fontSize: 12
    },
    text: {
        color: "gray",
        margin: 10,
        fontWeight: "800",
        fontSize: 15
    },
    prev_cnsgn: {
        color: "gray",
        fontSize: 12,
        position: "absolute",
        left: 10,
        bottom: 4
    },
    view1: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        width: Device.isTablet ? "70%" : "80%",
        alignSelf: "center",
        paddingTop: 10,
    },
    custombuttons: {
        display: "flex",
        alignSelf: "center",
        flexDirection: "column",
        justifyContent: "flex-end"
    },
    rate_txt: {
        display: "flex",
        justifyContent: "center",
        alignSelf: "flex-start",
        fontSize: 12,
        fontWeight: "800",
        paddingLeft: 30,
        color: "#a3a3a3"
    },
    buttons: {
        position: "absolute",
        bottom: 2,
        alignSelf: "center",
        flexDirection: "column",
        width: Device.isTablet ? "70%" : Platform.OS === 'ios' ? '100%' : '95%',
    },
    close_btn: {
        alignSelf: 'stretch',
        flexDirection: "column",
        justifyContent: 'center',
        marginTop: 20
    },
    imageUploadThumbnail_ios: {
        width: 100,
        margin: 10
    },
    imageUploadThumbnail_android: {
        width: Device.isTablet ? 170 : 90,
        height: Device.isTablet ? 200 : 100,
    },
    rate_txt_ios: {
        display: "flex",
        justifyContent: "center",
        alignSelf: "flex-start",
        fontSize: 14,
        fontWeight: "800",
        color: "#a3a3a3"
    },
    inputField_ios: {
        borderBottomWidth: 1,
        marginBottom: 5,
        borderBottomColor: "#a3a3a3",
        paddingTop: 10,
        width: 330,
        height: 35,
        padding: 5,
        fontSize: 15
    },
    count_ios: {
        fontSize: 12,
        fontWeight: "700",
        display: "flex",
        justifyContent: "flex-start",
        alignSelf: 'flex-start',
        paddingBottom: 10,
    },
    container_input_ios: {
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
        width: 330,
        borderColor: "#06b6d4",
        borderWidth: 4,
        backgroundColor: "#bae6fd",
        margin: 5,
        padding: 10,
    },
    btn_container: {
        flex: 1,
        alignSelf: "center",
        width: "100%",
        marginVertical: 20,
    },
    searchScrollView: {
        backgroundColor: '#ffffff',
        width: '90%',
        margin: 2,
        padding: 5,
        borderWidth: 1,
        borderColor: "#a3a3a3",
        borderRadius: 5,
        maxHeight: 200,
    },
    runsheetRepOutercontainer: {
        display: "flex",
        flexDirection: 'column',
        alignItems: "stretch",
        backgroundColor: '#bae6fd',
        borderColor: "#06b6d4",
        borderWidth: 4,
        marginTop: 20,
        marginBottom: 20,
        margin: 20,
    },
    runsheetRepcontainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: "center",
        paddingBottom: 20,
        paddingTop: 20,
    },
    runsheetRepcontainerTwo: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: "center",
        paddingBottom: 20,
        paddingTop: 20,
        alignContent: 'center',
        alignItems: 'center',
    },
    runsheetRepDate: {
        display: "flex",
        flexDirection: 'column',
        alignSelf: 'flex-end',
        marginTop: 10,
        paddingRight: 10
    },
    runsheetRepDateTxt: {
        fontWeight: '900',
    },
    runsheetCount: {
        fontSize: 16,
        fontWeight: "900",
    },
    runEntryAccordionContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: "flex-start",
        justifyContent: "center",
        top: 20,
    }
})

export default styles;