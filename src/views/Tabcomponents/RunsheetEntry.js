import React, { useState, useEffect, useRef } from 'react';
import { View, useWindowDimensions, Text, ScrollView, FlatList, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import moment from 'moment';
import Ionicons from "react-native-vector-icons/Ionicons";
import Device from 'react-native-device-detection';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from "react-redux";

// Components
import { CustomCancel, CustomSubmit } from '../../components/CustomButton';
import CustomSearchColumn from '../../components/CustomSearchColumn';
import CustomTextField from '../../components/CustomTextField';
import CustomFieldWithAction from '../../components/CustomFieldWithAction';
import CustomQRScanner from '../../components/CustomQRScanner';
import CustomAccordion from '../../components/CustomAccordion';

// Stylesheet
import styles from './styles';
import styleAccordion from '../../components/styles';

// Utils
import * as renderHieght from '../../utils/constants';

// Redux :: Actions
import * as callActions from "../..//redux/actions";
import { AUTOCOMPLETE_DATA, RUNSHEET_ENTRY_GET_DRS } from '../../redux/types';


const RunsheetEntry = (props) => {

    const { AutoCompleteData, RunsheetEntryDetails, globalaction } = props;

    const pcnameRef = useRef(null);
    const routenameRef = useRef(null);
    const deliverystuffRef = useRef(null);
    const consigmentRef = useRef(null);
    const layout = useWindowDimensions();
    const date = moment(new Date).format("DD/MM/YYYY");
    const [drsNo, setDrsNo] = useState(null);
    const [consigmentNo, setconsigmentNo] = useState(null);
    const [scanKey, setScanKey] = useState(null);
    const [scan, setScan] = useState(false);
    const [index, setIndex] = useState(0);
    const [innerTab, setInnerTab] = useState(1);
    const [id, setId] = useState(null);
    const [showModal, setShowModal] = useState(false);
    const [checkBox, setCheckBox] = useState(false);
    const [expandbranch, setExpandBranch] = useState(false);
    const [searchBranch, setSearchBranch] = useState(null);
    const [expandroutename, setExpandRouteName] = useState(false);
    const [searchRoutename, setSearchRoutename] = useState(null);
    const [textfiedDisabled, setTextfiedDisabled] = useState(true);
    const [routes] = useState([
        { key: 'first', title: 'Runsheet' },
        { key: 'second', title: 'Details' },
    ]);

    useEffect(() => {
        if (drsNo != "") {
            handelDRSscan();
        }
    }, [drsNo]);


    const handelDRSscan = async () => {
        const reqObj = {
            objName: "runsheet",
            cmod: "GETRUNSHEETDETAILS",
            p001: drsNo
        }
        if (drsNo) {
            await globalaction(reqObj, RUNSHEET_ENTRY_GET_DRS, null);
        }
    };
    const handleScan = () => {
        setScan(true);
    };
    const onSuccess = (e) => {
        if (scanKey === 'drs') {
            setDrsNo(e?.data);
        }
        else {
            setconsigmentNo(e?.data)
        }
        setScan(false);
    };
    const handleChangeValue = (data, key) => {
        if (key === 'drs') {
            setDrsNo(data)
            setScanKey('drs');
        } else {
            setconsigmentNo(data)
            setScanKey('consigment');
        }
        const reqObj = {
            objName: "drs",
            cmod: "GETRUNSHEETIMG",
            p001: "PON",
            p002: data
        }
        // const fetch_API = await getrunsheetimg(reqObj);
    };
    const handleSelect = (id) => {
        setShowModal(!showModal)
        setId(id)
    };
    const handleSearchColumn = (txt, key) => {
        if (key === ("branch")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCBRANCH",
                p001: txt,
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "SEARCBRANCH");
            setSearchBranch(txt);
            setExpandBranch(true);
            if (txt === "") {
                setTextfiedDisabled(true);
            }
            else {
                setTextfiedDisabled(false);
            }
        } else if (key === ("routename")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCROUT",
                p001: searchBranch,
                p002: txt
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "SEARCROUT");
            setSearchRoutename(txt);
            setExpandRouteName(true);
        }
    };
    const handleSelectColumn = (sel, key) => {
        if (key === ("branch")) {
            setSearchBranch(sel?.value);
            setExpandBranch(false);
            setTextfiedDisabled(false);
        }
        else if (key === ("routename")) {
            setSearchRoutename(sel?.name);
            setExpandRouteName(false);
        }
    }

    //--------------main Tab
    const renderTabBar = props => (
        <TabBar {...props} style={{ backgroundColor: '#06b6d4' }} />
    );


    const renderItem = ({ item, index }) => {
        return <>
            <View style={styleAccordion.runsheetAccordian} key={index}>
                <TouchableOpacity style={{ flexDirection: "row", justifyContent: "space-between" }} onPress={() => handleSelect(index)}>
                    <View style={{ justifyContent: "flex-start", flexDirection: "row", }}>
                        <View style={{
                            width: 8,
                            backgroundColor: showModal === true && index === id ? "yellow" : "#6ee7b7",
                        }} />
                        <View style={styleAccordion.list_view}>
                            <View style={styleAccordion.list_vw}>
                                <Text style={styleAccordion.name}>ConsignmentNo: {item?.ConsignmentNo}</Text>
                            </View>
                            <Text style={styleAccordion.content}>DRS No: {item?.drsno}</Text>
                        </View>
                        <View style={styleAccordion.list_view}>
                            <Text style={styleAccordion.content}>Date: {item?.drsdate}</Text>
                            <Text style={styleAccordion.content}>Total Consignment: {item?.total}</Text>
                        </View>
                    </View>
                </TouchableOpacity >
                {((index === id) && showModal) && <CustomAccordion open={showModal} eleId={id} name="RunsheetEntry" data={item} />}
            </View >
        </>
    };



    const renderScene = ({ route }) => {
        switch (route.key) {
            case 'first':
                return <View>
                    <ScrollView>
                        <View style={styles.runsheetRepOutercontainer}>
                            <View style={styles.runsheetRepDate}><Text style={styles.runsheetRepDateTxt}>Date: {date}</Text></View>
                            <View style={styles.runsheetRepcontainer}>
                                <CustomFieldWithAction placeholder="DRS Number" {...props} value={drsNo} onPress={() => [handleScan(), setScanKey('drs')]}
                                    onChange={(e) => handleChangeValue(e?.nativeEvent?.text, 'drs')} onSubmitEditing={() => pcnameRef?.current?.focus()} />

                                <CustomSearchColumn placeHolder='PC Name' {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"branch"}
                                    handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={searchBranch} show={expandbranch} data={AutoCompleteData?.Branch?.data} ref={pcnameRef}
                                    returnKeyType="next" onSubmitEditing={() => routenameRef?.current?.focus()} blurOnSubmit={false} />

                                <CustomSearchColumn placeHolder='Route Name' {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"routename"}
                                    handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={searchRoutename} show={expandroutename} data={AutoCompleteData?.RouteName?.data}
                                    inputFieldDisable={inputFieldDisable} textfiedDisabled={textfiedDisabled} ref={routenameRef} returnKeyType="next" onSubmitEditing={() => deliverystuffRef?.current?.focus()}
                                    blurOnSubmit={false} />

                                <CustomTextField placeholder='Delivery Stuff' {...props} ref={deliverystuffRef} returnKeyType="next" onSubmitEditing={() => consigmentRef?.current?.focus()}
                                    blurOnSubmit={false} />
                            </View>
                        </View>

                        <View style={styles.runsheetRepOutercontainer}>
                            <View style={{ flexDirection: 'row', height: 48 }}>
                                <TouchableOpacity style={[nestedTabBorderStyle.nestedBorderStyle, innerTab && nestedTabBorderStyle.selected]} onPress={() => setInnerTab(1)} >
                                    <Text style={{ color: 'white', }}>Consigment</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[nestedTabBorderStyle.nestedBorderStyle, !innerTab && nestedTabBorderStyle.selected]} onPress={() => setInnerTab(0)} >
                                    <Text style={{ color: 'white', }}>Other Details</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.runsheetRepcontainerTwo}>
                                {innerTab ?
                                    <>
                                        <CustomFieldWithAction placeholder="Consigment Number" {...props} value={consigmentNo} onPress={() => [handleScan(), setScanKey('consigment')]}
                                            onChange={(e) => handleChangeValue(e?.nativeEvent?.text, 'consigment')} ref={consigmentRef} />
                                        <View style={nestedTabBorderStyle.nestedTabCheckTxt}>
                                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                                <Icon onPress={() => setCheckBox(!checkBox)} name={checkBox ? "checkbox-marked" : "checkbox-blank-outline"} size={24} />
                                                <Text style={{ paddingLeft: 10 }}>Is Same Customer</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', flex: 1 }}>
                                                <Text style={{ paddingLeft: 10 }}>Weight:   Psc: </Text>
                                            </View>
                                        </View>
                                        <View style={nestedTabBorderStyle.nestedTabTxt}>
                                            <Text style={{ flex: 1 }}>Privious Consigment: 3123123123123</Text>
                                            <Ionicons name="add-circle" size={40} color="#4ade80" />
                                        </View>
                                    </>
                                    :
                                    <CustomTextField placeholder='Delivery Stuff' {...props}></CustomTextField>
                                }
                            </View>
                        </View>
                        <View style={{ justifyContent: 'space-between', display: 'flex', flexDirection: 'row', marginHorizontal: 20 }}>
                            <Text style={styles.runsheetCount}>Count: {RunsheetEntryDetails[0]?.total || 0}</Text>
                            <TouchableOpacity><Icon name="printer" size={24} /></TouchableOpacity>
                        </View>
                        <View style={styles.close_btn}>
                            <CustomSubmit type={"New DRS"} {...props} />
                            <CustomCancel type={"Clear"} {...props} />
                        </View>
                    </ScrollView>
                </View>
            case 'second':
                return <View style={styles.runEntryAccordionContainer}>
                    <FlatList
                        data={RunsheetEntryDetails || []}
                        renderItem={renderItem}
                        style={{
                            height: Dimensions.get('window').height,
                            top: Device.isTablet ? 10 : 2,
                            width: Dimensions.get('window').width,
                            backgroundColor: "transparent"
                        }}
                    />
                </View>;
            default:
                return null;
        }
    };


    //-------------------------------------------
    return (
        <>
            <TabView
                renderTabBar={renderTabBar}
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
            />
            {scan && <CustomQRScanner onSuccess={onSuccess} />}
        </>
    );
}

const nestedTabBorderStyle = StyleSheet.create({
    selected: { borderBottomWidth: 3, borderColor: '#f1f7a0' },
    nestedBorderStyle: { backgroundColor: '#06b6d4', flex: 1, alignItems: 'center', justifyContent: 'center' },
    nestedTabCheckTxt: { flexDirection: 'row', justifyContent: 'space-between', flex: 1, paddingHorizontal: 20 },
    nestedTabTxt: { flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, alignItems: 'center' },
});
const inputFieldDisable = StyleSheet.create({
    disableView: {
        width: Device.isTablet ? "70%" : "80%",
        height: Device.isTablet ? 35 : 30,
        margin: 2,
        padding: 5,
        borderWidth: 1,
        borderColor: "#a3a3a3",
        backgroundColor: "lightgrey",
        borderRadius: 5,
    },
})

const mapStatetoProps = (state) => {
    const { AutoCompleteData, RunsheetEntryDetails } = state.global;
    return {
        AutoCompleteData,
        RunsheetEntryDetails,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
        drsScan: (reqObj) => { return dispatch(callActions.POST_RunsheetEntry_DRS(reqObj)) },
    }
}

export default connect(mapStatetoProps, mapDispatchToProps)(RunsheetEntry);