import React, { useState, useEffect } from "react";
import { TouchableOpacity, View, Text, Image, ScrollView, StyleSheet, Modal } from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';

// Components
import { CustomSubmit } from "../../components/CustomButton";
import CustomFieldWithAction from "../../components/CustomFieldWithAction";
import CustomQRScanner from "../../components/CustomQRScanner";
import ImageViewer from 'react-native-image-zoom-viewer';

// Stylesheet
import styles from "./styles";
import imagestyles from "../../components/styles";

// Redux :: Actions
import * as callActions from "../../redux/actions/globalActions";
import { RUNSHEET_IMG_UPLOAD_GET, RUNSHEET_IMG_UPLOAD_POST, RUNSHEET_IMG_UPLOAD_DELETE } from "../../redux/types";

// Utils
import { connect } from "react-redux";
import { isEmpty } from "lodash";
import { chooseFiles } from "../../utils/Helper";


const RunsheetImageUpload = (props) => {
    const { bookdocimageupload, RunsheetImgUploadGet, globalaction } = props;


    const tempImageObj = {
        runsheetImg: "",
    }
    const [imageObject, setImageObj] = useState(tempImageObj);
    const [runsheetNo, setrunsheetNo] = useState(null);
    const [scan, setScan] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [popupImg, setPopupImg] = useState(null);
    const [tempArrListPost, setTempArrPost] = useState([]);
    const [tempArrShow, setTempArrShow] = useState([]);
    const todayDate = moment(new Date).format("DD/MM/YYYY");


    useEffect(() => {
        if (!isEmpty(RunsheetImgUploadGet?.data?.data)) {
            setTempArrShow([RunsheetImgUploadGet?.data?.data])
        }
    }, [RunsheetImgUploadGet?.data?.data]);


    const handleSubmit = () => {
        handleChangeValue(runsheetNo)
    };

    const deleteHandler = async (data, index) => {
        const ID = (RunsheetImgUploadGet?.data?.data || []).find((x) => { return x.imagepath === data })
        const reqObj = {
            objName: "runsheet",
            cmod: "DELRUNSHEETIMG",
            p001: "PON",
            p002: data,
            p003: runsheetNo,
            p004: (ID?.id).toString()
        }
        const fetch_API = await globalaction(reqObj, RUNSHEET_IMG_UPLOAD_DELETE, null);
        setTempArrShow(tempArrShow.filter((el, idx) => idx !== index));
    };

    const handleChangeValue = async (data) => {
        setrunsheetNo(data);
        const reqObj = {
            objName: "runsheet",
            cmod: "GETRUNSHEETIMG",
            p001: "PON",
            p002: data
        }
        const fetch_API = await globalaction(reqObj, RUNSHEET_IMG_UPLOAD_GET, null);
        const test = ((fetch_API?.data?.data) || []).map((el) => { return el?.imagepath })
        setTempArrShow(test);
    };

    const handleScan = () => {
        setScan(true);
    };

    const onSuccess = (e) => {
        setrunsheetNo(e?.data);
        setScan(false);
    };

    const uploadFiles = async (data) => {
        const arr = [];
        arr.push(data);
        if (runsheetNo) {
            for (let i = 0; i < arr.length; i++) {
                const spliting = (arr[i].split('/'))
                const name = spliting[9]
                const formData = new FormData();
                formData.append("interBranchCode", "HEAD");
                formData.append("consinmentdate", todayDate);
                formData.append("formtype", 'Runsheet');
                formData.append("file", {
                    uri: arr[i],
                    type: "image/jpg",
                    name: name
                });
                const fetch_API = await bookdocimageupload(formData, 'runsheetImages');
                if (fetch_API?.status === 201) {
                    const tempArr = fetch_API?.data?.data?.folder + '/' + fetch_API?.data?.data?.filename
                    setTempArrShow([...tempArrShow, tempArr])
                    setTempArrPost([...tempArrListPost, tempArr])
                    if (tempArr) {
                        const reqObj = {
                            objName: "runsheet",
                            cmod: "INSRUNSHEETIMG",
                            p001: "PON",
                            p002: tempArr,
                            p003: runsheetNo
                        }
                        const fetch_API = await globalaction(reqObj, RUNSHEET_IMG_UPLOAD_POST, null);
                        if (fetch_API?.status === 200) {
                            setTempArrPost([]);
                        } else {
                            alert('Image upload Failed');
                        }
                    }
                } else {
                    alert('Image upload Failed');
                }
            }
        }
    };


    return (
        <>
            <View style={styles.flex}>
                <CustomFieldWithAction placeholder="Runsheet Number" {...props} value={runsheetNo} onPress={() => handleScan()}
                    onChange={(e) => handleChangeValue(e?.nativeEvent?.text)} />

                <View style={styles.view1}>
                    <ScrollView contentContainerStyle={{ paddingBottom: 50 }} showsHorizontalScrollIndicator={false}>
                        <View style={imagestyles.multiImageContainer} >
                            {(tempArrShow || []).map((imgshow, index) => {
                                return (
                                    <View style={imagestyles.multiImageLoopContainer} key={index}>
                                        <View>
                                            <TouchableOpacity onPress={(e) => setPopupImg('https://g3proweb.sgp1.digitaloceanspaces.com' + imgshow, setModalVisible(true))}>
                                                <Image style={imagestyles.imagesUpload}
                                                    source={{ uri: 'https://g3proweb.sgp1.digitaloceanspaces.com' + imgshow }}>
                                                </Image>
                                            </TouchableOpacity>
                                            <Icon name='cancel' size={24} color='red' style={imagestyles.multiImageCancelIcon} onPress={() => deleteHandler(imgshow, index)} />
                                        </View>
                                    </View>
                                )
                            })}
                            {runsheetNo && <View style={{
                                width: 100, height: 120, justifyContent: 'center', alignItems: 'center',
                                borderStyle: 'dashed', borderColor: '#d4d4d4', borderWidth: 2, margin: 10,
                            }}>
                                <Icon name="add-a-photo" color="gray" size={48} onPress={() => { chooseFiles('Images', "runsheetImg", imageObject, setImageObj, uploadFiles) }} />
                            </View>}
                        </View>
                    </ScrollView>
                </View>
            </View>
            {scan && <CustomQRScanner onSuccess={onSuccess} />}

            <View style={styles.buttons}>
                <CustomSubmit {...props} type={"Search"} onPress={() => handleSubmit()} />
                {/* <CustomCancel  {...props} type={"Cancel"} /> */}
            </View>

            {modalVisible &&
                <View style={modelStyle.centeredView}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => { setModalVisible(!modalVisible) }} >
                        <ImageViewer imageUrls={[{ url: popupImg }]} enableSwipeDown={false} saveToLocalByLongPress={false} failImageSource={() => alert('Error Image')}
                            loadingRender={() => <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>Loading ...</Text>} />
                    </Modal>
                </View>
            }
        </>
    )

}

const mapStatetoProps = (state) => {
    const { RunsheetImgUploadGet } = state.global;
    return {
        RunsheetImgUploadGet,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
        bookdocimageupload: (reqObj, key) => { return dispatch(callActions.POST_ImageUpload(reqObj, key)) },
    }
}

const modelStyle = StyleSheet.create({
    centeredView: {
        flex: 1,
        flexDirection: 'column'
    },
});

export default connect(mapStatetoProps, mapDispatchToProps)(RunsheetImageUpload);