import { Dimensions, StyleSheet } from 'react-native';
import Device from 'react-native-device-detection';


const styles = StyleSheet.create({
    flex: {
        flex: 1,
        flexDirection: 'column',
        alignItems: "center",
        justifyContent: "center",
        top: 40
    },
    container: {
        position: "absolute",
        flexDirection: 'column',
        alignSelf: "center",
        justifyContent: "space-around",
        top: 40
    },
    count: {
        fontSize: 11,
        color: "gray",
        fontWeight: "700",
        display: "flex",
        justifyContent: "flex-start",
        alignSelf: 'flex-start',
        left: 10
    },
    input: {
        width: Device.isTablet ? "70%" : "80%",
        height: 25,
        margin: 3,
        padding: 5,
        borderWidth: 1,
        borderColor: "#a3a3a3",
        backgroundColor: "#fff",
        borderRadius: 5
    },
    field_with_icon: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row"
    },
    icon: {
        position: "absolute",
        right: 20
    },
    container_in: {
        position: "absolute",
        bottom: "35%",
        height: 100,
        padding: 5,
        width: 320,
        borderColor: "#06b6d4",
        borderWidth: 4,
        backgroundColor: "#bae6fd",
        margin: 5
    },
    container_input: {
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
        height: 120,
        width: 320,
        borderColor: "#06b6d4",
        borderWidth: 4,
        backgroundColor: "#bae6fd",
        margin: 5
    },
    manifest_txt: {
        margin: 4,
        color: "gray",
        fontSize: 12
    },
    weight: {
        marginRight: 8,
        color: "gray",
        fontSize: 12
    },
    prev_cnsgn: {
        fontSize: 12,
        color: "gray",
        position: "absolute",
        left: 10,
        bottom: 4
    },
    view1: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    custombuttons: {
        display: "flex",
        alignSelf: "center",
        flexDirection: "column",
        justifyContent: "flex-end"
    },
    buttons: {
        position: "absolute",
        bottom: 0,
        alignSelf: "center",
        flexDirection: "column",
        justifyContent: "flex-end"
    },
    input: {
        width: 300,
        height: 35,
        margin: 2,
        padding: 5,
        borderWidth: 1,
        borderColor: "#a3a3a3",
        backgroundColor: "#fff",
        borderRadius: 5
    },
})

export default styles;