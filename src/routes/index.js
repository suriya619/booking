import Registration from '../views/Registration';
import ManageOTP from "../views/OTP";
import ManageManifest from "../views/Manifest/InManifest";
import Booking from '../views/Tabcomponents/Booking';
import BookingDocument from '../views/Tabcomponents/BookingDocument';
import ImageCapture from '../views/Tabcomponents/ImageCapture';
import RateCalculator from '../views/Tabcomponents/RateCalculator';
import Report from '../components/Report';
import BookingReport from '../views/Tabcomponents/BookingReport';
import RunsheetEntry from '../views/Tabcomponents/RunsheetEntry';
import RunsheetImageUpload from '../views/Tabcomponents/RunsheetImageUpload';
import OfficeDelivery from '../views/Delivery/OfficeDelivery';
import RunsheetReport from '../views/Tabcomponents/RunsheetReport';
import PullRunsheet from '../views/PullRunsheet';
import Home from '../views/Home';
import InitialPage from '../views/InitialPage/InitialPage';



export const authenticateRoutes = [
    {
        exact: true,
        component: InitialPage,
        name: "InitialPage",
    },
    {
        exact: true,
        component: Registration,
        name: "Register",
    },
    {
        exact: true,
        component: ManageOTP,
        name: "Register-OTP",
    }
];

export const unAuthenticateRoutes = [
    {
        exact: true,
        component: Home,
        name: "Home",
    },
    {
        exact: true,
        component: Booking,
        name: "Booking",
    },
    {
        exact: true,
        component: BookingDocument,
        name: "BookingDocument",
    },
    {
        exact: true,
        component: BookingDocument,
        name: "Booking Document",
    },
    {
        label: "Normal Item",
        exact: true,
        component: ImageCapture,
        name: "Booking Image Capture",
    },
    {
        exact: true,
        component: ManageManifest,
        name: "Incoming Manifest Document",
    },
    {
        exact: true,
        component: ManageManifest,
        name: "Outgoing Manifest Document",
    },
    {
        label: "Normal Item",
        exact: true,
        component: RunsheetImageUpload,
        name: "Runsheet Image Upload",
    },
    {
        exact: true,
        component: RunsheetEntry,
        name: "Runsheet Entry",
    },
    {
        exact: true,
        component: RateCalculator,
        name: "RateCalculator",
    },
    {
        exact: true,
        component: BookingReport,
        name: "Booking-Report",
    },
    {
        exact: true,
        component: Report,
        name: "BookingReport",
    },
    {
        exact: true,
        component: RunsheetReport,
        name: "Runsheet-Report",
    },
    {
        exact: true,
        component: Report,
        name: "Runsheet Report",
    },
    {
        exact: true,
        component: OfficeDelivery,
        name: "Office Delivery",
    },
    {
        exact: true,
        component: OfficeDelivery,
        name: "Home Delivery",
    },
    {
        exact: true,
        component: PullRunsheet,
        name: "PullRunSheet",
    },
];

export const tabRoutes = [

];

