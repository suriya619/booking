import React, { useState, useEffect } from "react";
import { TouchableOpacity, View, Text, FlatList, BackHandler, Alert, SafeAreaView } from "react-native";
import { connect } from "react-redux";
import { SvgXml } from 'react-native-svg';
import { Divider } from 'react-native-paper';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import NetInfo from "@react-native-community/netinfo";
import moment from 'moment';

//Stylesheet
import styles from "./styles";

//Components
import CustomQRScanner from "./CustomQRScanner";

// Redux :: Actions
import * as callActions from "../redux/actions/globalActions";
import * as calllocalActions from '../redux/actions/localActions';
import { PRESIST_AUTOCOMPLETE_RELATION } from "../redux/types";


const CustomDrawer = (props) => {
    const { onClose, navigationRef, ResponseOTP, ResponseSideMenu, PushRunsheet, loading, runsheetpush, runsheetpullempty, runsheetpushempty,
        emppunchtime, Internet, sacnnedrunsheetpush, pushrunsheetimgupload, globalaction, togglesidemenu
    } = props;

    const [open, setOpen] = useState('');
    const [connected, setConnected] = useState(false);
    const [scan, setScan] = useState(false);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setConnected(state?.isConnected);
        });
    }, [connected])

    useEffect(() => {
        fetchRelation();
    }, [])

    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", backAction)
        return () =>
            BackHandler.removeEventListener("hardwareBackPress", backAction)
    }, []);


    const backAction = () => {

    }


    const fetchRelation = async () => {
        if (Internet) {
            const reqObj = {
                objName: "autocomplete",
                cmod: "SEARCRELA",
                p001: "",
            }
            const fetch_API = await globalaction(reqObj, PRESIST_AUTOCOMPLETE_RELATION, "RELATION");
        }
    }

    const onSuccess = async (e) => {
        const reqObj = {
            objName: "employee",
            cmod: "PUNCH",
            p001: ResponseOTP?.userid,
            p002: e.data
        }
        if (e.data) {
            const fetch_API = await emppunchtime(reqObj);
            setScan(false);
            if (fetch_API?.data?.data[0]) {
                Alert.alert("Punch time", `${moment(new Date(fetch_API?.data?.data[0].punchtime)).format('LLLL')}`, [
                    { text: "ok" }
                ]);
            }
        } else {
            setScan(false);
        }
    }

    const addNewKey = () => {
        if (ResponseSideMenu != []) {
            return (ResponseSideMenu || []).map((a) => { return { ...a, children: null } });
        }
    }

    const list_to_tree = (list) => {
        var map = {}, node, roots = [], i;
        for (i = 0; i < list.length; i += 1) {
            map[list[i].id] = i;
            list[i].children = [];
        }
        for (i = 0; i < list.length; i += 1) {
            node = list[i];
            if (node.parentid !== 0) {
                list[map[node.parentid]].children.push(node);
            } else {
                roots.push(node);
            }
        }
        return roots;
    }
    const treeView = list_to_tree(addNewKey());

    const onClickMenuParent = (item, name) => {
        if (item.children.length === 0 && item.routeName === "Home") {
            navigationRef?.current?.navigate("Home", { name: "Home" });
            onClose();
        }

        if (item.routeName === 'timepuch') { setScan(true) }
        else { setScan(false) };
        setOpen(name);
        syncRunsheet(item?.disName);

        if (item.routeName === 'Exit') {
            Alert.alert("Hold on!", "Are you sure you want exit to the App?", [
                { text: "No", onPress: () => null },
                { text: "YES", onPress: () => BackHandler.exitApp() }
            ]);
        }
    }

    const onClickMenuChild = (child) => {
        navigationRef?.current?.navigate(child?.routeName, { name: child?.disName });
        onClose();
    }

    const syncRunsheet = async (run) => {
        if (run === 'Pull RunSheet') {
            navigationRef?.current?.navigate("PullRunSheet", { name: "PullRunSheet" });
            onClose();
        } else if (run === 'Push RunSheet') {
            loading(true);
            if (connected && PushRunsheet?.length > 0) {
                for (let i = 0; i < PushRunsheet?.length; i++) {
                    const formData = new FormData();
                    formData.append("interBranchCode", "HEAD");
                    formData.append("formtype", "PushRunsheet");
                    formData.append("file", {
                        uri: PushRunsheet[i]?.ImgPath,
                        type: "image/jpeg",
                        name: PushRunsheet[i]?.ImgPath
                    });
                    const fetch_API = await pushrunsheetimgupload(formData, "PushRunsheet");
                    const img = fetch_API?.data?.data?.folder + '/' + fetch_API?.data?.data?.filename
                    if (fetch_API?.status === 201) {
                        const temp = {
                            ...PushRunsheet[i],
                            ImgPath: (img !== 'undefined/undefined') ? img : ''
                        }
                        sacnnedrunsheetpush(temp);
                        const reqObj = {
                            objName: 'runsheet',
                            cmod: 'PUSHRUN',
                            p001: temp.RefId,
                            p002: temp.RunSheetNo,
                            p003: temp.ConsignmentNo,
                            p004: temp.DeliveryStatus,
                            p005: temp.DeliveryDate,
                            p006: temp.ReceiverRelationship,
                            p007: temp.latitude,
                            p008: temp.longitude
                        }
                        const fetch_API = await runsheetpush(reqObj);
                        loading(false);
                    }
                    else {
                        alert('Image not uploaded');
                        loading(false);
                    }
                }
                await runsheetpullempty();
                await runsheetpushempty();
            } else if (PushRunsheet?.length === 0) {
                alert('No runsheet to push');
            }
            else {
                loading(false);
                alert('Need internet to push');
            }
            loading(false);
        }
    }

    return (
        <>
            <SafeAreaView style={styles.drawercontainer}>
                <View style={{ flex: 1, width: '100%' }}>
                    <FlatList
                        contentContainerStyle={{ flexGrow: 1, paddingBottom: 80 }}
                        nestedScrollEnabled
                        data={(treeView || [])}
                        keyExtractor={item => item?.disName}
                        renderItem={({ item, index }) => {
                            return (
                                <SafeAreaView>
                                    <View style={styles.drawerFlaListContainer} key={item?.disName}>
                                        <TouchableOpacity onPress={() => onClickMenuParent(item, item?.disName)} style={styles.drawerFlaListTitle}>
                                            <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 10 }}>
                                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingLeft: 20 }}>
                                                    {item?.svg ? <SvgXml xml={item?.svg} width={24} height={24} /> : <MaterialIcons name='article' size={24} color='grey' />}
                                                    <Text style={styles.drawerText}>{item?.disName}</Text>
                                                </View>
                                                {item?.children?.length > 0 && <View>
                                                    <MaterialIcons name={(open === item?.disName) ? 'expand-more' : 'chevron-right'} size={24} color='grey' style={{ paddingRight: 10 }} />
                                                </View>}
                                            </View>
                                        </TouchableOpacity>
                                        {open === item?.disName && (item?.children || []).map((child, idx) => {
                                            return (
                                                <TouchableOpacity style={styles.drawerFlaListChild} onPress={() => onClickMenuChild(child)} key={child?.disName}>
                                                    <MaterialIcons name='shop' size={24} color='grey' />
                                                    <Text style={styles.drawerTextChild}>{child?.disName}</Text>
                                                </TouchableOpacity>
                                            )
                                        })}
                                    </View>
                                    <Divider mx={1} />
                                </SafeAreaView>
                            )
                        }}
                    />
                </View>
            </SafeAreaView>
            {scan && <CustomQRScanner onSuccess={onSuccess} />}
        </>
    )
}

const mapStatetoProps = (state) => {
    const { ResponseOTP, ResponseSideMenu, PullRunsheet, PushRunsheet } = state.presistLocal;
    const { Internet } = state.local;

    return {
        ResponseOTP,
        ResponseSideMenu,
        PullRunsheet,
        PushRunsheet,
        Internet,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
        loading: (reqObj) => { return dispatch(calllocalActions.Loading(reqObj)) },
        runsheetpush: (reqObj) => { return dispatch(callActions.POST_PushRunsheet(reqObj)) },
        runsheetpullempty: () => { return dispatch(calllocalActions.setRunsheetPullEmpty()) },
        runsheetpushempty: () => { return dispatch(calllocalActions.setRunsheetPushEmpty()) },
        emppunchtime: (reqObj) => { return dispatch(callActions.POST_EmployeePunchTime(reqObj)) },
        sacnnedrunsheetpush: (reqObj) => { return dispatch(calllocalActions.setScannedRunsheetPush(reqObj)) },
        pushrunsheetimgupload: (reqObj, key) => { return dispatch(callActions.POST_ImageUpload(reqObj, key)) },
        togglesidemenu: (reqObj) => { return dispatch(calllocalActions.setToggleSideMenu(reqObj)) },
    }
}

export default connect(mapStatetoProps, mapDispatchToProps)(CustomDrawer);