import React, { useState } from 'react';
import { Text, TouchableOpacity, View } from "react-native";
import Popover from 'react-native-popover-view';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import cn from 'react-native-classnames';
import styles from './styles';

const arr = [
    { key: "Open", name: "Open", icon: <AntDesign name="folderopen" size={16} color="#000" /> },
    { key: "Close", name: "Close", icon: <AntDesign name="folder1" size={16} color="#000" /> },
    { key: "Print", name: "Print", icon: <MaterialIcons name="print" size={18} color="#000" /> },
    { key: "Image", name: "Image", icon: <MaterialIcons name="cloud-upload" size={18} color="#000" /> },
    { key: "Image11", name: "Image", icon: <MaterialIcons name="cloud-download" size={18} color="#000" /> },
    { key: "Details", name: "Details", icon: <FontAwesome name="angle-double-down" size={19} color="#000" /> }
]

const CustomPopover = (props) => {
    const [value, setValue] = useState("");
    const [select, setSelect] = useState(false);

    const { showPopover } = props;

    const handleSelect = (ele) => {
        setValue(ele)
        setSelect(true)
    }

    return (
        <>
            <Popover isVisible={showPopover} popoverShift={{ x: 1 }} >
                <View style={{ width: 150, }}>
                    {(arr || []).map((ele, index) => <TouchableOpacity key={index} style={cn(styles, 'dropdown_content', select && ele.key === value && "hover")} onPress={() => handleSelect(ele.key)}  >
                        {ele.icon}
                        <Text style={styles.txt}>{ele.name}</Text>
                    </TouchableOpacity>)}
                </View>
            </Popover>
        </>
    );
}

export default CustomPopover;
