import React from 'react';
import { View, Text } from 'react-native';
import styles from "./styles";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

const CustomAccordion = (props) => {
    const { data, open, name, setPopupImg, setModalVisible } = props;

    return (
        <>
            <View>
                {open && <View>
                    <View style={{ flexDirection: "row" }}>
                        {(name === "Runsheet" || name === "RunsheetEntry" || name === "Book") && <View style={{
                            width: 8,
                            backgroundColor: "yellow",
                        }} />}
                        <View style={{ flexDirection: 'column' }} >
                            {(data && (name === "RunsheetEntry" || name === "Book")) ? <View style={{ display: "flex", flexDirection: "column", marginTop: 2, paddingLeft: 5, paddingRight: 5 }}>
                                <Text style={styles.accordionHeader}>Customer Name: {data?.customername}</Text>
                                <Text style={styles.accordionSubHeader}>Mobile Number: {data?.mobile}</Text>
                                {data?.fromimage && <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <MaterialIcons name="image" size={24} color="#a3a3a3" />
                                    <Text style={styles.accordionSubHeader}
                                        onPress={() => (setPopupImg('https://g3proweb.sgp1.digitaloceanspaces.com' + data?.fromimage), setModalVisible(true))}>From Image</Text>
                                </View>}
                                {data?.toimage && <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <MaterialIcons name="image" size={24} color="#a3a3a3" />
                                    <Text style={styles.accordionSubHeader}
                                        onPress={() => (setPopupImg('https://g3proweb.sgp1.digitaloceanspaces.com' + data?.toimage), setModalVisible(true))}>To Image</Text>
                                </View>}

                                {data?.fromaddress && <Text style={styles.accordionSubHeader}>From Address: {data?.fromaddress}</Text>}
                                {data?.toaddress && <Text style={styles.accordionSubHeader}>To Address: {data?.toaddress}</Text>}
                            </View> : <View style={{ display: "flex", flexDirection: "column", paddingLeft: 5 }}>
                                <Text style={styles.accordionSubHeader}>Delivery Route: {data?.routename}</Text>
                                <Text style={styles.accordionSubHeader}>Staff Name: {data?.staffnmae}</Text>
                                <Text style={styles.accordionSubHeader}>Vehicle No: {data?.vechileno}</Text>
                            </View>}
                        </View>
                    </View>
                </View>}
            </View>
        </>
    )
}

export default (CustomAccordion)