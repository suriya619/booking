import React from "react";
import { TouchableOpacity, Text } from "react-native";
import styles from "../views/Registration/styles";


export const CustomSubmit = (props) => {
    const { type } = props;

    return (
        <TouchableOpacity  {...props} style={styles.submit} >
            <Text style={styles.text}>{type}</Text>
        </TouchableOpacity>
    )
}

export const CustomReset = (props) => {
    const { type } = props;

    return (
        <TouchableOpacity  {...props} style={styles.resetBtn}>
            <Text style={styles.text}>{type}</Text>
        </TouchableOpacity>
    )
}

export const CustomCancel = (props) => {
    const { navigation, type, route, successLogin } = props;

    const MovtoRenderedPage = () => {
        if (route?.name !== 'InitialPage') {
            if (successLogin) {
                navigation.push('InitialPage');
            }
            else {
                navigation.pop();
            }
        }
        else {
            return null
        }
    }

    return (
        <TouchableOpacity onPress={() => MovtoRenderedPage()} style={styles.cancel}>
            <Text style={styles.text}>{type}</Text>
        </TouchableOpacity>
    )
}