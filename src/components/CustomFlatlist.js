import React, { useState, useEffect } from 'react';
import { View, FlatList, Text, ScrollView, TouchableOpacity } from 'react-native';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { connect } from 'react-redux';
import Device from 'react-native-device-detection';
import NetInfo from "@react-native-community/netinfo";

// Components
import CustomPopover from '../components/CustomPopover';

// Stylesheet
import styles from "./styles";

// Redux :: Actions
import * as callActions from '../redux/actions/globalActions';
import * as calllocalActions from '../redux/actions/localActions';
import { MANIFEST_DETAILS_UPDATE } from '../redux/types';


const CustomFlatlist = (props) => {
    const { route, InMainfestArr, OutMainfestArr, BothManifestDetailsUpdate, Internet, setPreviousConsigment, ManifestDetailsReset, loading, globalaction } = props;

    const [showPopover, setShowPopover] = useState(false);
    const [connected, setConnected] = useState(false);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setConnected(state?.isConnected);
        });
    }, [InMainfestArr, connected])

    useEffect(() => {
        handleFunction();
        setPreviousConsigment((((route?.name === "Incoming Manifest Document" ? InMainfestArr : OutMainfestArr) || []).flat(Infinity).filter((dup, index, arr) =>
            arr.findIndex(t => t.Consignmentno.toLowerCase() === dup.Consignmentno.toLowerCase()) === index)).slice(-1)[0]?.Consignmentno);
    }, [InMainfestArr, OutMainfestArr, Internet])


    const handleFunction = async () => {
        if ((InMainfestArr || OutMainfestArr)) {
            let reSendDetails = (route?.name === "Incoming Manifest Document" ? (InMainfestArr || []) : (OutMainfestArr || [])).flat(Infinity).filter((re) => { return re.IsSendApi === 0 });
            if (reSendDetails?.length > 0) {
                for (let i = 0; i < reSendDetails?.length; i++) {
                    const reqObj = {
                        p001: reSendDetails[i].ManifestID,
                        p002: reSendDetails[i].Staff,
                        p003: reSendDetails[i].Recivedtype,
                        p004: reSendDetails[i].Consignmentno,
                        p005: reSendDetails[i].Packet,
                        p006: reSendDetails[i].Weight,
                        p007: reSendDetails[i]?.DestinationCode,
                        objName: (route?.name === "Incoming Manifest Document" ? 'manifestin' : 'manifestout'),
                        cmod: "INSERTDOC"
                    }
                    if (Internet === true) {
                        loading(true);
                        const fetch_API = await globalaction(reqObj, MANIFEST_DETAILS_UPDATE, (route?.name === "Incoming Manifest Document" ? ("manifestin") : ("manifestout")));
                        Promise.all(fetch_API?.data?.data).then((results) => {
                            ManifestDetailsReset(route?.name);
                            BothManifestDetailsUpdate(results, route?.name);
                        })
                        loading(false);
                    } else {
                        loading(false);
                    }
                }
            }
        }
    }

    const renderItem = ({ item, index }) => {
        return <>
            <View style={styles.ListItem} key={index.toString()}>
                <View style={styles.item}>
                    <View style={{
                        width: 8,
                        minHeight: Device.isTablet ? 55 : 47,
                        backgroundColor: item?.IsSendApi === 0 ? "yellow" : "#6ee7b7",
                    }} />

                    <TouchableOpacity style={styles.list_view}>
                        <Text style={styles.name}>Consignment Number: {item?.Consignmentno}</Text>
                        <View style={styles.list_vw}>
                            {(route?.name === "Incoming Manifest Document") && <Text style={styles.content}>ReceivedStatus: {item?.Recivedtype}</Text>}
                            <Text style={styles.content}>  Weight: {item?.Weight}</Text>
                            <Text style={styles.content}>  Packet: {item?.Packet}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity>
                    <MaterialIcons name={"more-vert"} size={20} color="#000" />
                </TouchableOpacity>
            </View>
        </>
    };

    return (
        <ScrollView >
            <FlatList
                data={(((route?.name === "Incoming Manifest Document" ? InMainfestArr : OutMainfestArr) || []).flat(Infinity).filter((dup, index, arr) =>
                    (arr || []).findIndex(t => t.Consignmentno.toLowerCase() == dup.Consignmentno.toLowerCase()) == index))}
                renderItem={renderItem}
                style={{
                    top: 10,
                    padding: 8,
                }}
            />
            {showPopover && <CustomPopover showPopover={showPopover} />}
        </ScrollView>
    );
}

const mapStateToProps = (state) => {
    const { InMainfestArr, OutMainfestArr, Internet } = state.local;
    return {
        InMainfestArr,
        OutMainfestArr,
        Internet,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        globalaction: (reqObj, reducerType, reducerKey) => { return dispatch(callActions.Common_GlobalActions(reqObj, reducerType, reducerKey)) },
        loading: (reqObj) => { return dispatch(calllocalActions.Loading(reqObj)) },
        BothManifestDetailsUpdate: (reqObj, key) => { return dispatch(callActions.setInMainDetailsUpdate(reqObj, key)) },
        ManifestDetailsReset: (reqObj) => { return dispatch(calllocalActions.setInMainDetailsReset(reqObj)) },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomFlatlist);

