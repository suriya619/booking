import React, { useState } from 'react';
import { Text, View, Modal, StyleSheet, Button } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';


const CustomQRScanner = (props) => {

    const { onSuccess } = props;

    const [torch, setTorch] = useState(false);

    const handleTorch = () => {
        if (torch) {
            setTorch(false);
        } else {
            setTorch(true);
        }
    }

    return (
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={true}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <QRCodeScanner
                            onRead={(e) => onSuccess(e)}
                            flashMode={(torch === true) ? (RNCamera.Constants.FlashMode.torch) : (RNCamera.Constants.FlashMode.off)}
                            cameraType={'back'}
                            showMarker={true}
                            markerStyle={{ borderColor: '#06b6d4' }}
                            bottomContent={
                                <View>
                                    <Text style={{
                                        flex: 1,
                                        textAlign: 'center',
                                        fontSize: 14,
                                        padding: 32,
                                        color: 'white',
                                    }}>
                                        Please move your camera over the QR Code
                                    </Text>
                                </View>
                            }
                        />
                        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }}>
                            <Button
                                style={[styles.button, styles.buttonClose]}
                                onPress={(e) => onSuccess(e)}
                                title="Close"
                            />
                            <Button
                                style={[styles.button, styles.buttonClose]}
                                onPress={() => handleTorch()}
                                title="Torch"
                            />
                        </View>
                    </View>
                </View>
            </Modal>
        </View>
    )
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});


export default CustomQRScanner;