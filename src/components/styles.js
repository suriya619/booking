
import { Dimensions, Platform, StatusBar, StyleSheet } from "react-native";
import * as renderHieght from "../utils/constants";
import Device from "react-native-device-detection";

const styles = StyleSheet.create({
    primaryColor: {
        backgroundColor: '#06b6d4',
    },
    header: {
        height: Device.isTablet ? 72 : renderHieght.commonHeader,
        // width: Dimensions.get('window').width,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
    },
    bottomtab: {
        flexDirection: "row",
    },
    bottomTabContent: {
        display: "flex",
        justifyContent: "center",
        flexDirection: "row",
        alignSelf: "center",
    },
    search: {
        width: Platform.OS === 'ios' ? 260 : (Device.isTablet ? '70%' : 220),
        height: Device.isTablet ? 40 : 30,
        backgroundColor: "#fff",
        fontSize: 14,
        fontWeight: "800",
        padding: 5
    },
    placeholderImage: {
        width: 150,
        height: 20,
        display: "flex",
        justifyContent: "center",
        alignSelf: "flex-start"
    },
    imageText: {
        fontWeight: "700",
        color: "gray",
        fontSize: 14,
        paddingBottom: 10,
    },
    imageText_ios: {
        fontWeight: "700",
        color: "gray",
        fontSize: 18
    },
    field_with_icon: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        width: Device.isTablet ? "70%" : "80%"
    },
    input: {
        width: Device.isTablet ? "70%" : "80%",
        height: 35,
        margin: 2,
        padding: 5,
        borderWidth: 1,
        borderColor: "#a3a3a3",
        backgroundColor: "#fff",
        borderRadius: 5
    },
    field_with_icon: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        marginRight: 10
    },
    more: {
        marginRight: 5,
        fontSize: 12,
        color: "#06b6d4"
    },
    main_list: {
        flexDirection: "column",
        position: "absolute",
        backgroundColor: "transparent",
        width: Dimensions.get('screen').width,
        padding: 5, top: 80,
        bottom: 10
    },
    container: {
        display: "flex",
        flexDirection: 'column',
        alignItems: "center",
        justifyContent: "flex-start",
        top: renderHieght.routeHeader + 30,
        height: Dimensions.get('window').height - (renderHieght.commonHeader + renderHieght.routeHeader + renderHieght.tabHeader),
        backgroundColor: "red"
    },
    reportContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "transparent",
        paddingVertical: 20
    },
    imageContainer: {
        flexDirection: 'column',
        alignItems: "center",
    },
    imageContainerSearch: {
        display: "flex",
        alignItems: "center",
        paddingVertical: 20
    },
    buttons: {
        alignSelf: "center",
        flexDirection: "column",
        width: Device.isTablet ? "70%" : "100%",
        paddingVertical: 20
    },
    dropdown: {
        position: "absolute",
        backgroundColor: "#a5f3fc",
        width: 250,
        boxShadow: "0px 8px 16px 0px ",
        zIndex: 9999,
    },
    dropdown_content: {
        color: "#5f6363",
        padding: 5,
        fontSize: 12,
        display: "flex",
        justifyContent: "flex-start",
        alignSelf: "flex-start",
        flexDirection: "row"
    },
    hover: {
        backgroundColor: "#06b6d4",
        width: 150,
    },
    ListItem: {
        flexDirection: "row",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "white",
        elevation: 3,
        shadowOpacity: 0.75,
        shadowColor: "#a3a3a3",
        width: Dimensions.get('window').width - 20,
        margin: Platform.OS === 'ios' ? 5 : 2,
        minHeight: Device.isTablet ? 55 : 46,
    },
    item: {
        flexDirection: "row",
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    name: {
        fontSize: 13,
        fontWeight: "bold",
        color: "gray",
    },
    list_view: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-start",
        flexDirection: "column",
        padding: 5
    },
    list_vw: {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "flex-start",
        flexDirection: "row"
    },
    dragdrop: {
        minHeight: Device.isTablet ? 150 : 90,
        width: Device.isTablet ? 250 : 150,
        borderStyle: "dashed",
        borderColor: "#d4d4d4",
        borderWidth: Platform.OS === 'ios' ? 3 : 2,
        margin: 10,
        justifyContent: "center",
        alignItems: "center",
        padding: 20
    },
    view1: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "flex-start"
    },
    align: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "flex-start",
        left: 5,
        marginBottom: 5
    },
    flex: {
        flex: 1,
        flexDirection: 'column',
        alignItems: "flex-start",
        justifyContent: "center",
        top: 30,
        height: Dimensions.get('window').height - 100,
        backgroundColor: "transparent",
    },
    custombuttons: {
        alignSelf: "flex-end",
        justifyContent: "flex-end",

    },
    txt: {
        fontSize: 12,
        paddingLeft: 10,
        color: 'grey',
    },
    content: {
        fontSize: 12,
        color: 'grey'
    },
    datePicker: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        width: 150,
        height: 150,
        display: 'flex',
    },

    // ** modal view ** //
    modalView: {
        display: "flex",
        justifyContent: "center",
    },
    modalText: {
        fontSize: 14,
        fontWeight: "800",
        color: "gray",
        // margin: 4
    },

    // ** Accordion ** //
    accordionContainer: {
        elevation: 5,
        backgroundColor: "white",
        padding: 4,
        shadowOpacity: 1.75,
        borderRadius: 1,
        flexDirection: 'column',
        marginHorizontal: 10,
        margin: 2,
        shadowColor: 'rgb(0, 0, 0)',
        shadowRadius: 5,
        shadowOffset: {
            width: 3,
            height: 3,
        }
    },
    runsheetAccordian: {
        elevation: 5,
        backgroundColor: "white",
        shadowOpacity: 1.75,
        borderRadius: 1,
        flexDirection: 'column',
        marginLeft: 10,
        marginRight: 10,
        marginTop: Platform.OS === 'ios' ? 8 : 0,
        shadowColor: 'rgb(0, 0, 0)',
        margin: 3,
        shadowRadius: 5,
        shadowOffset: {
            width: 3,
            height: 3,
        }
    },
    accordionDropList: {
        backgroundColor: "white",
        paddingTop: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 1,
    },
    accordionHeaderTouchable: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    accordionHeader: {
        fontWeight: '900',
        fontSize: 13,
        color: 'grey',
    },
    accordionSubHeader: {
        fontSize: 12,
        fontWeight: '600',
        color: 'grey',
        paddingTop: 5,
        paddingBottom: 5
    },
    popupselect: {
        backgroundColor: '#00bcd4',
    },
    popupnotselect: {
        backgroundColor: '#a5f3fc',
    },
    popuptextselect: {
        color: 'white',
    },
    popuptextnotselect: {
        color: 'black',
    },
    icon: {
        padding: 4,
        backgroundColor: "#d4d4d4",
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#a3a3a3",
        width: Device.isTablet ? '4%' : 30,
        height: 35,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
    },
    icon_ios: {
        paddingLeft: 3,
        paddingTop: 5,
        backgroundColor: "#d4d4d4",
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#a3a3a3",
        width: 30,
        height: 35,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
    },
    textfield: {
        width: Device.isTablet ? "70%" : "80%",
        height: 35,
        borderStartColor: "green",
        padding: 5,
        borderWidth: 1,
        borderColor: "#a3a3a3",
        backgroundColor: "#fff",
        borderRadius: 5,
        borderRightWidth: 0,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
    },
    textfield_ios: {
        width: 300,
        height: 35,
        padding: 5,
        borderWidth: 1,
        borderRightWidth: 0,
        borderColor: "#a3a3a3",
        backgroundColor: "#fff",
        borderRadius: 5,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0
    },
    // ****** drawer section ****** //
    drawercontainer: {
        backgroundColor: 'white',
        elevation: 5,
        shadowOpacity: 0.55,
        transition: 1,
        alignItems: 'flex-start',
        marginTop: Platform.OS === 'ios' ? 47 : 0,
        height: Dimensions.get('screen').height,
    },
    drawerIconContainer: {
        height: renderHieght.commonHeader,
        width: "100%",
        backgroundColor: "white",
        borderBottomColor: '#d4d4d4',
        justifyContent: "center",
        borderBottomWidth: 1
    },
    drawerLeftIcon: {
        display: "flex",
        borderRadius: 20,
        backgroundColor: "transparent",
        justifyContent: "center",
        marginRight: 10,
        alignSelf: "flex-end",
        cursor: 'pointer',
    },
    drawercontent: {
        display: "flex",
        justifyContent: "center",
        alignItems: "flex-start",
        alignSelf: 'flex-start',
        backgroundColor: "transparent",
        margin: Platform.OS === 'ios' ? 5 : 1,
        paddingLeft: 20,
    },
    hover: {
        backgroundColor: "#f5f5f4",
        height: 50,
        width: 250,
    },
    drawerText: {
        fontSize: 16,
        fontWeight: '500',
        paddingLeft: 10,
    },
    drawerTextChild: {
        fontSize: 16,
        fontWeight: '400',
        paddingLeft: 10,
    },
    drawerTextContainer: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    drawerFlaListContainer: {
        flex: 1,
        justifyContent: 'flex-start',
    },
    drawerFlaListTitle: {
        flex: 1,
        justifyContent: 'flex-start',
        alignContent: 'center',
        alignItems: "center",
        flexDirection: 'row',
    },
    drawerFlaListChild: {
        flex: 1,
        justifyContent: 'flex-start',
        alignContent: 'center',
        alignItems: "center",
        paddingVertical: 10,
        paddingLeft: 40,
        flexDirection: 'row',
        backgroundColor: '#e3faff'
    },
    imagesUpload: {
        height: Platform.OS === 'ios' ? 180 : 120,
        width: Platform.OS === 'ios' ? 230 : 100,
    },
    multiImageContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        display: 'flex',
        justifyContent: 'flex-start',
        padding: 10,
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
    },
    multiImageLoopContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding: 10,
    },
    multiImageCancelIcon: {
        position: 'absolute',
        right: 5,
        top: 5
    },
    imageUploadThumbnail_ios: {
        height: Platform.OS === 'ios' ? 120 : 110,
        width: Platform.OS === 'ios' ? 95 : 90,
        margin: Platform.OS === 'ios' ? 10 : 0,
    },
    btn_container: {
        position: 'absolute',
        alignSelf: "center",
        flexDirection: "column",
        justifyContent: "flex-end",
        width: "100%",
        bottom: 5,
    },
})

export default styles;