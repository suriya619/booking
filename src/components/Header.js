import React, { useState, useEffect } from "react";
import { View, TextInput, Platform, TouchableOpacity, Image, Pressable } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import CustomDrawer from "./CustomDrawer";
import styles from "./styles";
import Device from "react-native-device-detection";
import { connect } from "react-redux";


const Header = (props) => {
	const { navigationRef, ResponseOTP, getheadersearch, ToggleSideMenu, handleDrawer } = props;


	const [enableMenu, setEnableMenu] = useState(false);

	useEffect(() => {
		if (ResponseOTP?.userid) {
			setEnableMenu(true);
		} else {
			setEnableMenu(false);
		}
	})

	return (<>
		<View style={[styles.header, styles.primaryColor]}>
			{enableMenu && <TouchableOpacity onPress={() => handleDrawer()}>
				<MaterialIcons name={"menu"} size={Platform.OS === 'ios' ? 30 : (Device.isTablet ? 32 : 24)} color="white"></MaterialIcons>
			</TouchableOpacity>}
			<Pressable style={styles.search}  >
				<Image source={require('../assets/appname.png')} style={styles.placeholderImage} />
			</Pressable>
			<MaterialIcons name={"search"} size={Platform.OS === 'ios' ? 30 : (Device.isTablet ? 32 : 24)} color="white" />
			<MaterialIcons name={"more-vert"} size={Platform.OS === 'ios' ? 30 : (Device.isTablet ? 32 : 24)} color="white" />
		</View>
		{ToggleSideMenu &&
			<CustomDrawer navigationRef={navigationRef} onClose={handleDrawer} getheadersearch={getheadersearch} />
		}
	</>)
}

const mapStateToProps = (state) => {
	const { afterLoginsuccess, ToggleSideMenu } = state.local;
	const { ResponseOTP } = state.presistLocal;
	return {
		afterLoginsuccess, ResponseOTP, ToggleSideMenu
	}
}

export default connect(mapStateToProps, null)(Header);