import React, { useState } from "react";
import { TextInput, View, TouchableOpacity, Text, Platform, StyleSheet, Keyboard, TouchableWithoutFeedback } from 'react-native';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import moment from "moment";
import Device from 'react-native-device-detection';
import { connect } from "react-redux";

// Components
import { CustomSubmit } from "../components/CustomButton";
import CustomDatePicker from "../components/CustomDatePicker";
import CustomSearchColumn from "./CustomSearchColumn";

// Stylesheet
import styles from "./styles";

// Redux :: Actions
import * as callActions from "../redux/actions/globalActions";
import * as calllocalActions from "../redux/actions/localActions";
import { AUTOCOMPLETE_DATA, BOOKING_REPORT_DATE } from '../redux/types';


const Report = (props) => {
    const { route, navigation, AutoCompleteData, loading, globalaction } = props;

    const currentDate = moment(new Date).format('DD-MM-YYYY');
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [fromDate, setFromDate] = useState(currentDate);
    const [toDate, setToDate] = useState(currentDate);
    const [assign, setassign] = useState(null);
    const [expandbranch, setExpandBranch] = useState(false);
    const [searchBranch, setSearchBranch] = useState((route?.name === "BookingReport") ? "ALL" : null);
    const [expandRouteName, setExpandRouteName] = useState(false);
    const [searchRouteName, setSearchRouteName] = useState(null);

    const showDatePicker = (key) => {
        setDatePickerVisibility(true);
        if (key === 'from') {
            setassign('from');
        } else if (key === 'to') {
            setassign('to');
        }
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (date) => {
        if (assign === 'from') {
            setFromDate(moment(date).format('DD-MM-YYYY'));
        } else if (assign === 'to') {
            setToDate(moment(date).format('DD-MM-YYYY'));
        }
        setDatePickerVisibility(false);
    };

    const handleSearchColumn = async (txt, key) => {
        if (key === ("reportbranch")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCBRANCHRPT",
                p001: txt,
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "BOOKREPBRANCH")
            setSearchBranch(txt);
            setExpandBranch(true);
        } else if (key === ("routename")) {
            const outreqObj = {
                objName: "autocomplete",
                cmod: "SEARCROUT",
                p001: searchBranch,
                p002: txt
            }
            globalaction(outreqObj, AUTOCOMPLETE_DATA, "SEARCROUT")
            setSearchRouteName(txt);
            setExpandRouteName(true);
        }
    };

    const handleSelectColumn = (sel, key) => {
        if (key === ("reportbranch")) {
            setSearchBranch(sel?.value);
            setExpandBranch(false);
        } else if (key === ("routename")) {
            setSearchRouteName(sel?.name);
            setExpandRouteName(false);
        }
    };

    const submitHandler = async () => {
        loading(true);
        if (fromDate && toDate && searchBranch && route?.name === "BookingReport") {
            const fromDateArr = fromDate.split("-");
            const toDateArr = toDate.split("-");
            const tempObj = {
                objName: "consignment",
                cmod: "GETPERIODBOOKING",
                p001: searchBranch,
                p002: `${fromDateArr[2]}-${fromDateArr[1]}-${fromDateArr[0]}`,
                p003: `${toDateArr[2]}-${toDateArr[1]}-${toDateArr[0]}`
            }
            const fetch_API = await globalaction(tempObj, BOOKING_REPORT_DATE, 'BOOKREPORT');
            if (fetch_API?.data?.data) {
                setSearchBranch((route?.name === "BookingReport") ? "ALL" : null);
                setFromDate(currentDate);
                setToDate(currentDate);
                loading(false);
                navigation.push('Booking-Report');
            } else {
                loading(false);
                alert(fetch_API);
            }
        } else if (fromDate && toDate && searchBranch && searchRouteName && route?.name === "Runsheet Report") {
            const fromDateArr = fromDate.split("-");
            const toDateArr = toDate.split("-");
            const tempObj = {
                objName: "runsheet",
                cmod: "GETRUNSHEETS",
                p001: searchBranch,
                p002: `${fromDateArr[2]}-${fromDateArr[1]}-${fromDateArr[0]}`,
                p003: `${toDateArr[2]}-${toDateArr[1]}-${toDateArr[0]}`,
                p004: searchRouteName
            }
            const fetch_API = await globalaction(tempObj, BOOKING_REPORT_DATE, 'RUNREPORT');
            if (fetch_API?.data?.data) {
                setSearchBranch((route?.name === "BookingReport") ? "ALL" : null);
                setSearchRouteName(null);
                setFromDate(currentDate);
                setToDate(currentDate);
                loading(false);
                navigation.push('Runsheet-Report');
            } else {
                loading(false);
                alert(fetch_API);
            }
        } else {
            loading(false);
            alert('Invalid Input')
        }
    };



    return (
        <>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <View style={styles.reportContainer}>
                    {route?.name === "Runsheet Report" && <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"reportbranch"} placeHolder="PC Name"
                        handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={searchBranch} show={expandbranch} data={AutoCompleteData?.BookingRepBranch?.data} />}

                    {route?.name === "BookingReport" && <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"reportbranch"} placeHolder="HO"
                        handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={searchBranch} show={expandbranch} data={AutoCompleteData?.BookingRepBranch?.data} />}

                    <View style={styles.align}>
                        <Text style={styles.modalText}>From Date</Text>
                        <View style={styles.field_with_icon}  >
                            <TextInput placeholder={fromDate} value={fromDate} placeholderTextColor="#a3a3a3" style={((Platform.OS === 'ios') ? (styles.textfield_ios) : (styles.textfield))} selectTextOnFocus={false} />
                            <TouchableOpacity style={Platform.OS === 'ios' ? styles.icon_ios : styles.icon} onPress={() => showDatePicker('from')}>
                                <MaterialIcons name="date-range" size={Platform.OS === 'ios' ? 22 : 20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.align}>
                        <Text style={styles.modalText}>To Date</Text>
                        <View style={styles.field_with_icon}  >
                            <TextInput placeholder={toDate} value={toDate} placeholderTextColor="#a3a3a3" style={((Platform.OS === 'ios') ? (styles.textfield_ios) : (styles.textfield))} selectTextOnFocus={false} />
                            <TouchableOpacity style={Platform.OS === 'ios' ? styles.icon_ios : styles.icon} onPress={() => showDatePicker('to')}>
                                <MaterialIcons name="date-range" size={Platform.OS === 'ios' ? 22 : 20} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>

                    {route?.name === "Runsheet Report" && <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)} type={"routename"}
                        placeHolder="Route Name" handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={searchRouteName} show={expandRouteName}
                        data={AutoCompleteData?.RouteName?.data} />}

                    {isDatePickerVisible && < CustomDatePicker handleConfirm={handleConfirm} hideDatePicker={hideDatePicker} isDatePickerVisible={isDatePickerVisible} />}
                </View>
            </TouchableWithoutFeedback>
            <View style={styles.buttons}>
                <CustomSubmit onPress={submitHandler}{...props} type="Search" />
            </View>
        </>
    )
}

const mapStateToProps = (state) => {
    const { BookingReportDate, AutoCompleteData } = state.global;
    return {
        BookingReportDate, AutoCompleteData,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
        loading: (reqObj) => { return dispatch(calllocalActions.Loading(reqObj)) },
    }
}


const inputFieldDisable = StyleSheet.create({
    disableView: {
        width: Device.isTablet ? "70%" : "80%",
        height: Device.isTablet ? 35 : 30,
        margin: 2,
        padding: 5,
        borderWidth: 1,
        borderColor: "#a3a3a3",
        backgroundColor: "lightgrey",
        borderRadius: 5,
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(Report); 