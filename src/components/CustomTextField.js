import React from "react";
import { View, TextInput, Platform } from 'react-native';
import styles from "../views/Registration/styles";
import cn from 'react-native-classnames';


const CustomTextField = React.forwardRef((props, ref) => {
    const { route } = props;
    return (
        <>
            <View style={styles.field_with_icon}  >
                <TextInput {...props} style={styles.input} placeholderTextColor="#a3a3a3" ref={ref} />
            </View>
        </>
    )
})

export default CustomTextField;