import React, { useEffect } from 'react';
import { StyleSheet, SafeAreaView, View, Button, Alert } from 'react-native';

const CustomAlert = (props) => {
  const { subTitle, setExit, handleAlert } = props;


  useEffect(() => {
    twoOptionAlert();
  }, [])

  const twoOptionAlert = () => {
    Alert.alert(
      //This is title
      "",
      //This is body text
      subTitle,
      [
        { text: 'Yes', onPress: () => handleAlert(true) },
        { text: 'No', onPress: () => handleAlert(false) },
      ],
      //on clicking out side, Alert will not dismiss
    );
  }
}
export default CustomAlert;
