import React, { useState } from 'react'
import { TouchableOpacity, Text, Image, Platform, ScrollView } from "react-native";
import styles from "./styles";
import Ionicons from "react-native-vector-icons/Ionicons";
import { launchCamera } from 'react-native-image-picker';
import { PermissionsAndroid } from 'react-native';
import Device from 'react-native-device-detection';


export const CustomImagePicker = (props) => {
    const { selectableImages, setmultiImages, thumbnailText, routerName } = props;

    const [fullImagesAdd, setFullImagesAdd] = useState('');
    const renderImage = Device.isTablet ? require("../assets/11.png") : require('../assets/add.png')


    const photoUpload = () => {
        requestCameraPermission()
    }
    const requestCameraPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message: "App needs access to your camera ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                const options = {
                    mediaType: 'photo',
                    selectionLimit: selectableImages,
                };
                launchCamera(options, setResponse => {
                    if (setResponse.didCancel === true) {
                        setFullImagesAdd('')
                        setmultiImages([])
                    } else {
                        if (selectableImages === 1) {
                            setFullImagesAdd(setResponse?.assets[0]?.uri)
                        } else {
                            setmultiImages(setResponse.assets.map((x) => (x.uri)))
                        }
                    }
                });
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            alert(err);
        }
    };


    return (
        <>

            {(routerName === 'Runsheet Image Upload') ?
                <ScrollView contentContainerStyle={{ padding: 10 }} showsHorizontalScrollIndicator={false}>
                    <TouchableOpacity onPress={() => photoUpload()} >
                        <Image source={renderImage} style={styles.imageUploadThumbnail_ios} />
                    </TouchableOpacity>
                </ScrollView>
                :
                <>
                    <TouchableOpacity style={styles.dragdrop} onPress={() => photoUpload()}>
                        <Ionicons name="add" color="gray" size={24} />
                        <Text style={Platform.OS === 'ios' ? styles.imageText_ios : styles.imageText}>{thumbnailText}</Text>
                    </TouchableOpacity>
                    {
                        fullImagesAdd && <Image style={styles.imagesUpload}
                            source={{ uri: fullImagesAdd }}>
                        </Image>
                    }
                </>
            }

        </>
    )
}