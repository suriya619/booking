import React from "react";
import { View, Text, Platform, StyleSheet, FlatList, TouchableOpacity, TextInput, SafeAreaView } from 'react-native';
import Device from "react-native-device-detection";
import { HStack } from "native-base";

// Stylesheet
import styles from "../views/Tabcomponents/styles";

// Utils
import MaterialIcons from "react-native-vector-icons/MaterialIcons";



const CustomSearchColumn = React.forwardRef((props, ref) => {
  const { type, searchTxt, handleSearchColumn, handleSelectColumn, show, data, placeHolder } = props;


  return (
    <>
      <View style={styles.field_with_icon}>
        <TextInput {...props} style={styles.inputTextField} placeholderTextColor="#a3a3a3" placeholder={placeHolder}
          onChange={(e) => handleSearchColumn(e?.nativeEvent?.text, type)} value={searchTxt} ref={ref} />
        <MaterialIcons name="search" size={20} color="#a3a3a3" style={styles.icon} />
      </View>
      {show && <View style={styles.searchScrollView}>
        <View style={{ backgroundColor: '#06b6d4', height: 50, flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 1, paddingLeft: 10 }}><Text style={{ color: 'white' }}>Name</Text></View>
          <View style={{ flex: 1 }}><Text style={{ color: 'white' }}>Code</Text></View>
        </View>
        <FlatList
          data={(data || []).filter(el => (type === 'destination' ? el.value : el.name)?.toLowerCase().indexOf(searchTxt?.toLowerCase()) !== -1)}
          nestedScrollEnabled={true}
          keyExtractor={item => item.name}
          renderItem={({ item, index }) => {
            return (<SafeAreaView>
              <TouchableOpacity key={index} style={{ padding: 5 }} onPress={() => handleSelectColumn(item, type)}>
                <HStack space={0} justifyContent="center" height="10" backgroundColor="red" flex="1">
                  <View style={{ flex: 1 }}><Text>{(type === 'destination') ? (item?.value) : (item?.name)}</Text></View>
                  <View style={{ flex: 1 }}><Text>{((type === 'branch') || (type === 'relation')) ? (item?.value) : ((type === 'pincode') ? (item?.desc) : (item?.name))}</Text></View>
                </HStack>
                <View style={{ borderBottomColor: 'black', borderBottomWidth: StyleSheet.hairlineWidth }} />
              </TouchableOpacity>
            </SafeAreaView>
            )
          }}
          ListEmptyComponent={() => {
            return (
              <SafeAreaView>
                <View style={{ flex: 1, padding: 5 }}><Text>No Records Found</Text></View>
              </SafeAreaView>
            )
          }} />
      </View>}
    </>
  )
})


export default CustomSearchColumn;