import React, { useState } from "react";
import { View, TouchableOpacity, Platform, Text, ScrollView, TextInput } from 'react-native';
import AntDesign from "react-native-vector-icons/AntDesign";
import cn from 'react-native-classnames';

// Redux :: Actions
import * as calllocalActions from '../redux//actions/localActions'

// Stylesheet
import styles from "../views/Registration/styles";
import { connect } from "react-redux";
import { getUserTypeName } from '../utils/Helper'


const CustomSelect = (props) => {

    const { route, dataDropdown, setMenuItem, mainfestObj, isSelected, dissableField } = props;

    const [open, setOpen] = useState(false);

    const handelDropDown = (e) => {
        if (isSelected && route?.name === "Outgoing Manifest Document") {
            setOpen(false)
        } else {
            setOpen(!open)
        }
    }

    const selectHandler = (val) => {
        (open === true) ? setOpen(false) : setOpen(true);
        const Type = getUserTypeName(route?.name);
        setMenuItem(val, Type);
    }


    return (
        <>
            <View style={styles.field_with_icon} onTouchStart={() => handelDropDown()}>
                <TextInput {...props} placeholderTextColor="#a3a3a3" editable={false} style={cn(
                    styles,
                    Platform.OS === 'android' ? (dissableField ? "outRecivedStatus" : "input") :
                        (route?.name === "Runsheet Entry" || route?.name === "Incoming Manifest Document" || route?.name === "Outgoing Manifest Document") ? "input_ios" : "input_route_ios")} />
                <TouchableOpacity style={styles.icon}>
                    <AntDesign name="caretdown" size={Platform.OS === 'ios' ? 12 : 10} color="#a3a3a3" />
                </TouchableOpacity>
            </View>
            {open &&
                <ScrollView style={styles.dropDownScrollView}>
                    {((dataDropdown) || []).map((drop, index) => {
                        return (
                            <TouchableOpacity key={index} style={{ padding: 5, }} onPress={() => selectHandler(drop?.value)}>
                                <Text>{drop?.value}</Text>
                            </TouchableOpacity>
                        )
                    })
                    }
                </ScrollView>
            }
        </>
    )
}
const mapStateToProps = (state) => {
    const { mainfestObj } = state.local;
    return {
        mainfestObj,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setMenuItem: (val, key) => { return dispatch(calllocalActions.setMenuItem(val, key)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomSelect);