import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Popover } from "native-base";
import AntDesign from "react-native-vector-icons/AntDesign";
import { connect } from 'react-redux';
import styles from './styles';
import { isEmpty } from 'lodash';


const CustomModal = (props) => {
    const { select, datalist, id, setSelect } = props;

    const handleSelect = () => {
        if (select === false) {
            setSelect(true)
        } else {
            setSelect(true)
        }
    }

    return (
        <>
            {!isEmpty(datalist) && (datalist || []).filter((item) => item.id === id).map((ele) => <Popover trigger={triggerProps => {
                return <TouchableOpacity  {...triggerProps} style={styles.list_view} onPress={() => handleSelect()}>
                    <View style={styles.list_vw}>
                        <AntDesign
                            name={(select === true && ele.id === id) ? "export" : "checksquare"}
                            size={20}
                            color={(select === true && ele.id === id) ? "#fdba74" : "#6ee7b7"}
                            style={{ paddingRight: 5 }}
                        />
                        <Text style={styles.name}>{ele.email}</Text>
                    </View>
                    <View style={styles.list_vw}>
                        <Text style={styles.txt}>{ele.username}</Text>
                        <Text style={styles.txt}>{ele.website}</Text>
                        <Text style={styles.txt}>Status: Normal</Text>
                    </View>
                </TouchableOpacity>

            }} isOpen={select && ele.id === id} onClose={() => setSelect(false)}>
                <Popover.Content w={500}>
                    <Popover.Body>
                        <View style={styles.list_vw}>
                            <AntDesign
                                name={(select === true && ele.id === id) ? "export" : "checksquare"}
                                size={20}
                                color={(select === true && ele.id === id) ? "#fdba74" : "#6ee7b7"}
                                style={{ paddingRight: 5 }}
                            />
                            <Text style={styles.name}>{ele.email}</Text>
                        </View>
                        <View style={styles.list_vw}>
                            <Text style={styles.txt}>{ele.username}</Text>
                            <Text style={styles.txt}>{ele.website}</Text>
                            <Text style={styles.txt}>Status: Normal</Text>
                        </View>
                    </Popover.Body>
                </Popover.Content>
            </Popover>)}
        </>
    )
}

const mapStateToProps = (state) => {
    const { datalist } = state.global;
    return { datalist }
};

export default connect(mapStateToProps)(CustomModal);