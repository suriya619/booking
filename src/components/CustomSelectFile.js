import React from "react";
import { View, TextInput, TouchableOpacity } from 'react-native';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import styles from "../views/Registration/styles";


const CustomSelectFile = (props) => {

    return (
        <View style={styles.field_with_Button}  >
            <TouchableOpacity style={styles.inputButton} {...props}>
                <MaterialIcons name="photo-camera" size={15} color="#a3a3a3" />
            </TouchableOpacity>
            <TextInput {...props} style={styles.entryInput} placeholderTextColor="#a3a3a3" editable={false}></TextInput>
            {<TouchableOpacity style={styles.inputViewButton} {...props} onPress={props.onView}>
                <MaterialIcons name="remove-red-eye" size={15} color="#a3a3a3" />
            </TouchableOpacity>}
            <TouchableOpacity style={styles.inputCancelButton} {...props} onPress={props.onClose}>
                <MaterialIcons name="cancel" size={15} color="#a3a3a3" />
            </TouchableOpacity>
        </View>
    )
}

export default CustomSelectFile;