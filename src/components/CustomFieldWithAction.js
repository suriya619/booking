import React from "react";
import { View, TextInput, Text, TouchableOpacity } from 'react-native';

// Stylesheet
import styles from "../views/Registration/styles";


const CustomFieldWithAction = (props) => {
    const { dissableField, ref } = props;


    return (
        <View style={styles.field_with_Button}  >
            <TextInput {...props} style={dissableField ? styles.entryInput_left_disable : styles.entryInput_left} placeholderTextColor="#a3a3a3" ref={ref} />
            <TouchableOpacity style={styles.inputButton_right} {...props} >
                <Text >{'Scan'}</Text>
            </TouchableOpacity>
        </View>
    )
}

export default CustomFieldWithAction;