import React from 'react'
import styles from '../components/styles';
import DateTimePickerModal from "react-native-modal-datetime-picker";

const CustomDatePicker = (props) => {
    const { handleConfirm, hideDatePicker, isDatePickerVisible } = props;

    return (
        <DateTimePickerModal
            isVisible={isDatePickerVisible}
            mode="date"
            onConfirm={handleConfirm}
            onCancel={hideDatePicker}
            style={styles.datePicker}
        />
    )
}

export default CustomDatePicker;