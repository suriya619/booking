import React, { useState, useEffect, useRef } from 'react';
import { TextInput, TouchableOpacity, View, Text, Pressable, Keyboard, TouchableWithoutFeedback, ScrollView, Alert } from 'react-native';
import moment from 'moment';
import { isEmpty } from 'lodash';
import { connect } from "react-redux";
import cn from 'react-native-classnames';
import RNPrint from 'react-native-print';
import Device from 'react-native-device-detection';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

// Components
import CustomSelect from '../CustomSelect';
import CustomQRScanner from '../CustomQRScanner';
import CustomTextField from '../CustomTextField';
import CustomSearchColumn from '../CustomSearchColumn';
import CustomFieldWithAction from '../CustomFieldWithAction';
import { CustomSubmit } from '../CustomButton';


// Redux :: Actions
import * as callActions from '../../redux/actions/globalActions';
import * as calllocalActions from '../../redux/actions/localActions';
import { MANIFEST_DETAILS_UPDATE, AUTOCOMPLETE_DATA } from '../../redux/types';

// Stylesheet
import styles from '../../views/Registration/styles';

//Utils
import { getUserTypeName } from '../../utils/Helper';


const myFunction = (mainfestObj, routeName) => {
	var returnedObject = {};
	if (routeName === "Incoming Manifest Document") {
		returnedObject["receivedStatus"] = mainfestObj?.IncomingManifest;
	} else if (routeName === "Outgoing Manifest Document") {
		returnedObject["receivedStatus"] = mainfestObj?.OutgoingManifest;
	}
	return returnedObject;
}

const CustomManifest = (props) => {

	const { route, receivedStatus, inoutmanifest, mainfestObj, setmenuTtem, outgoingSearch, MainfestData, setMainfestData, initialObj,
		AutoCompleteData, loading, InMainfestArr, BothManifestDetailsUpdate, OutMainfestArr, Internet, ManifestDetailsReset,
		setPreviousConsigment, previousConsigment, getinoutmanifest, setmainifestDetails, disableContainer, setDisableContainer, globalaction
	} = props;

	const manifestNoRef = useRef(null);
	const consigmentNoRef = useRef(null);
	const destinationRef = useRef(null);
	const date = moment(new Date).format("DD/MM/YYYY");
	const [isSelected, setSelection] = useState(true);
	const renderedStatus = myFunction(mainfestObj, route?.name);
	const [scan, setScan] = useState(false);
	const [typedata, settypeData] = useState(null);
	const [expandDestination, setExpandDestination] = useState(false);
	const [searchDestination, setSearchDestination] = useState(null);
	const initialCount = { inFailed: 0, outFailed: 0 };
	const [allCount, setAllCount] = useState(initialCount);
	const [totalCount, setTotalCount] = useState(0);
	const initialAdd = {
		staffName: MainfestData?.staffName,
		manifestNo: MainfestData?.manifestNo,
		consigmentNo: "",
		packet: "1",
		weight: "0.250",
		pices: "1",
	}

	useEffect(() => {
		if (route?.name) {
			const Type = getUserTypeName(route?.name);
			setmenuTtem("Normal", Type);
		}
	}, [setmenuTtem])

	useEffect(() => {
		const fail = (route?.name === "Incoming Manifest Document" ? (InMainfestArr || []) : (OutMainfestArr || [])).flat(Infinity).filter((item) => {
			if (item?.IsSendApi === 0) {
				return true;
			} else {
				return false;
			}
		}).length;
		(route?.name === "Incoming Manifest Document" ? (setAllCount({ ...allCount, inFailed: fail })) : (setAllCount({ ...allCount, outFailed: fail })));
		setTotalCount((route?.name === "Incoming Manifest Document" ? (InMainfestArr || []) : (OutMainfestArr || [])).flat(Infinity).filter((dup, index, arr) =>
			arr.findIndex(t => t.Consignmentno?.toLowerCase() == dup.Consignmentno?.toLowerCase()) == index).length);
	}, [InMainfestArr, OutMainfestArr, totalCount, previousConsigment])

	useEffect(() => {
		if (disableContainer) {
			setSelection(true);
			setMainfestData({ ...MainfestData, packet: "1", weight: "0.250", pices: "1", consigmentNo: "" });
			const Type = getUserTypeName(route?.name);
			setmenuTtem("Normal", Type);
		}
	}, [disableContainer])

	const handleScan = (data) => {
		setScan(true);
		settypeData(data);
	};

	const toggleCheckbox = () => {
		setSelection(!isSelected)
		if (isSelected) {
			setMainfestData({ ...MainfestData, packet: "1", weight: "0.250", pices: "1" });
		} else {
			setMainfestData({ ...MainfestData, packet: "1", weight: "0.250", pices: "1" });
		}
	};

	const handleChangeValue = (value, type) => {
		setMainfestData({ ...MainfestData, [type]: value });
	};

	const onSuccess = (e) => {
		if (typedata === "manifestNo") {
			setMainfestData({ ...MainfestData, manifestNo: ((e?.data !== undefined || e?.data !== "") && (e?.data)) });
			consigmentNoRef?.current?.focus();
		}
		if (typedata === "consigmentNo") {
			setMainfestData({ ...MainfestData, consigmentNo: ((e?.data) === undefined ? "" : (e?.data)) });
		}
		setScan(false);
	};

	const handleSearchColumn = (txt, key) => {
		const outreqObj = {
			objName: "manifestout",
			cmod: "GETDISTIN",
			p001: txt,
		}
		globalaction(outreqObj, AUTOCOMPLETE_DATA, "GETDISTIN");
		setSearchDestination(txt);
		setExpandDestination(true);
	};

	const handleSelectColumn = (sel, key) => {
		setSearchDestination(sel?.value);
		setExpandDestination(false);
	};

	const handlePrinter = async () => {
		if (Internet) {
			const fetchHTML = await fetch('http://206.189.134.200:8080/dgos3').then(thisres => thisres.text()).then(res => res);
			const results = await RNHTMLtoPDF.convert({ html: fetchHTML });
			await RNPrint.print({ filePath: results.filePath });
		} else {
			alert('Internet required');
		}
	}

	const handleApi = async (el) => {
		if (!isEmpty(MainfestData)) {
			loading(true);
			const reqobj = {
				objName: ((el === "Incoming") ? ("manifestin") : ("manifestout")),
				cmod: "INSERTDOC",
				p001: MainfestData?.manifestNo,
				p002: MainfestData?.staffName,
				p003: ((renderedStatus?.receivedStatus === undefined) ? "" : renderedStatus?.receivedStatus),
				p004: MainfestData?.consigmentNo,
				p005: MainfestData?.packet,
				p006: MainfestData?.weight,
				p007: searchDestination,
			}
			try {
				if (reqobj?.p001 !== "" && reqobj?.p002 !== "" && reqobj?.p003 !== "" && reqobj?.p004 !== "" && (route?.name === "Incoming Manifest Document" ? true : searchDestination)) {
					const manifestCheck = (route?.name === "Incoming Manifest Document" ? (InMainfestArr || []) : (OutMainfestArr || [])).flat(Infinity).find((mc) =>
						mc?.ManifestID?.toLowerCase() === reqobj?.p001?.toLowerCase());
					if (manifestCheck) {
						const consigmentCheck = (route?.name === "Incoming Manifest Document" ? (InMainfestArr || []) : (OutMainfestArr || [])).flat(Infinity).find((cc) =>
							cc?.Consignmentno?.toLowerCase() === reqobj?.p004?.toLowerCase());
						if (consigmentCheck) {
							loading(false);
							alert('Found Duplicate');
						}
						else {
							if (Internet) {
								const fetch_API = await globalaction(reqobj, MANIFEST_DETAILS_UPDATE, el);
								if (fetch_API?.data?.data.length > 0 && Internet) {
									const responseData = fetch_API?.data?.data;
									BothManifestDetailsUpdate(responseData, route?.name);
								}
							} else {
								if (route?.name === "Incoming Manifest Document" ? (InMainfestArr?.length > 0) : (OutMainfestArr?.length > 0)) {
									const swap = {
										"Consignmentno": reqobj?.p004,
										"ManifestID": reqobj?.p001,
										"Packet": reqobj?.p005,
										"Recivedtype": reqobj?.p003,
										"Staff": reqobj?.p002,
										"Weight": reqobj?.p006,
										"DestinationCode": reqobj?.p007
									}
									BothManifestDetailsUpdate({ ...swap, IsSendApi: 0 }, route?.name);
								} else {
									loading(false);
									alert('First scan required Internet');
								}
							}
						}
					} else {
						if (route?.name === "Incoming Manifest Document" ? (InMainfestArr.length > 0) : (OutMainfestArr.length > 0)) {
							loading(false);
							Alert.alert(
								//This is title
								"Alert",
								//This is body text
								"Change manifest number?",
								[
									{ text: 'Yes', onPress: () => { (route?.name === "Incoming Manifest Document" ? (InMainfestArr.length > 0) : (OutMainfestArr.length > 0)) ? alertFunc() : null } },
									{ text: 'No', onPress: () => null },
								],
							)
						}

						alertFunc = async () => {
							if (Internet) {
								loading(true);
								const fetch_API = await globalaction(reqobj, MANIFEST_DETAILS_UPDATE, el);
								if (fetch_API?.data?.data.length > 0 && Internet) {
									const responseData = fetch_API?.data?.data;
									ManifestDetailsReset(route?.name);
									BothManifestDetailsUpdate(responseData, route?.name);
								}
							}
							else { alert('First manifest required Internet') }
							loading(false);
						}

						if (route?.name === "Incoming Manifest Document" ? (InMainfestArr.length === 0) : (OutMainfestArr.length === 0)) {
							if (Internet) {
								loading(true);
								const fetch_API = await globalaction(reqobj, MANIFEST_DETAILS_UPDATE, el);
								if (fetch_API?.data?.data.length > 0 && Internet) {
									const responseData = fetch_API?.data?.data;
									ManifestDetailsReset(route?.name);
									BothManifestDetailsUpdate(responseData, route?.name);
								}
							} else {
								if (route?.name === "Incoming Manifest Document" ? (InMainfestArr?.length > 0) : (OutMainfestArr?.length > 0)) {
									const swap = {
										"Consignmentno": reqobj?.p004,
										"ManifestID": reqobj?.p001,
										"Packet": reqobj?.p005,
										"Recivedtype": reqobj?.p003,
										"Staff": reqobj?.p002,
										"Weight": reqobj?.p006,
										"DestinationCode": reqobj?.p007
									}
									BothManifestDetailsUpdate({ ...swap, IsSendApi: 0 }, route?.name);
								} else {
									loading(false);
									alert('First scan required Internet');
								}
							}
						}
					}
					setMainfestData(initialAdd);
					const Type = getUserTypeName(route?.name);
					setSelection(true);
					setmenuTtem("Normal", Type);
					loading(false);
				} else {
					loading(false);
					alert('Invalid Input');
				}
			}
			catch (err) {
				loading(false);
				alert(err);
			}
		} else {
			loading(false);
			alert("Imvalid Input");
		}
	};

	const addManifest = () => {
		if (route?.name === "Incoming Manifest Document") {
			handleApi("Incoming");
		}
		else if (route?.name === "Outgoing Manifest Document") {
			handleApi("Outgoing");
		}
	};

	const newManifest = () => {
		loading(true);
		const Type = getUserTypeName(route?.name);
		setmenuTtem("Normal", Type);
		setMainfestData(initialObj);
		setSelection(true);
		ManifestDetailsReset(route?.name);
		setSearchDestination(null);
		setAllCount(initialCount);
		setPreviousConsigment(null);
		loading(false);
	};

	const getManifest = async () => {
		if (Internet) {
			if (MainfestData?.manifestNo) {
				loading(true);
				const reqObj = {
					objName: route?.name === "Incoming Manifest Document" ? "manifestin" : "manifestout",
					cmod: "GETDOC",
					p001: MainfestData?.manifestNo
				}
				const fetch_API = await getinoutmanifest(reqObj);
				if (fetch_API?.data?.data.length != 0) {
					await setmainifestDetails(fetch_API?.data?.data, route?.name);
				}
				loading(false);
			} else {
				loading(false);
				alert('Manifest number required');
			}
		} else {
			loading(false);
			alert('Internet required');
		}
	}

	return (
		<>
			<TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
				<ScrollView showsHorizontalScrollIndicator={false}>
					<View style={{ alignItems: 'center' }}>
						{route?.name === "Incoming Manifest Document" ? null: <Text style={styles.pocketText} >Pocket No: 1</Text>}
						{route?.name === "Outgoing Manifest Document" && <CustomTextField  {...props} placeholder='Staff Name' placeholderTextColor="#a3a3a3" style={styles.staffField}
							onChangeText={(e) => handleChangeValue(e, "staffName")}
							value={MainfestData?.staffName} returnKeyType="next"
							onSubmitEditing={() => manifestNoRef?.current?.focus()}
							blurOnSubmit={false} />
						}
						<View style={cn(styles, Device.isTablet ? 'containerTabletview' : 'container_input')}>
							{route?.name === "Incoming Manifest Document" && <CustomTextField placeholder='Staff Name' {...props}
								onChangeText={(e) => handleChangeValue(e, "staffName")}
								value={MainfestData?.staffName}
								onSubmitEditing={() => manifestNoRef?.current?.focus()} />
							}
							<CustomFieldWithAction value={MainfestData?.manifestNo} onPress={() => handleScan("manifestNo")}
								placeholder="Manifest No" onChange={(e) => handleChangeValue(e?.nativeEvent?.text, "manifestNo")}
								ref={manifestNoRef}
								returnKeyType="next"
								onSubmitEditing={() => [consigmentNoRef?.current?.focus(), destinationRef?.current?.focus()]}
								blurOnSubmit={false} />
							{route?.name === "Outgoing Manifest Document" && <CustomSearchColumn {...props} handleSearchColumn={(txt, key) => handleSearchColumn(txt, key)}
								type={"destination"} placeHolder="Destination" handleSelectColumn={(sel, key) => handleSelectColumn(sel, key)} searchTxt={searchDestination}
								show={expandDestination} data={AutoCompleteData?.OutManifestDestination?.data} ref={destinationRef}
								returnKeyType="next"
								onSubmitEditing={() => consigmentNoRef?.current?.focus()}
								blurOnSubmit={false} />
							}
							{route?.name === "Outgoing Manifest Document" && <View style={styles.view1}>
								<Pressable value={isSelected}>
									{isSelected ? <FontAwesome name="check-square" size={20} color="#06b6d4" /> : <MaterialCommunityIcons name="checkbox-blank-outline" size={20} />}
								</Pressable>
								<Text style={styles.manifest_txt}>Auto Validation Required?</Text>
							</View>
							}
						</View>

						<View style={cn(styles, Device.isTablet ? 'secondcontainerTabletview' : 'container_in')} pointerEvents={disableContainer ? "none" : "auto"}>
							<CustomSelect placeholder="Received Status" {...props} dataDropdown={receivedStatus?.data} value={renderedStatus?.receivedStatus} dissableField={disableContainer} />
							<View style={Device.isTablet ? styles.tabletview : styles.view1}>
								<TouchableOpacity value={isSelected} onPress={() => toggleCheckbox()}>
									{isSelected ? <FontAwesome name="check-square" size={20} color={disableContainer ? "#999999" : "#06b6d4"} /> : <MaterialCommunityIcons name="checkbox-blank-outline" size={20} />}
								</TouchableOpacity>
								<Text style={styles.manifest_txt}>Consignment No. Only</Text>
								{route?.name === "Incoming Manifest Document" && <Text style={Device.isTablet ? styles.packetTxt : styles.manifest_txt}>Packet</Text>}
								{route?.name === "Incoming Manifest Document" && <TextInput style={(isSelected) ? (styles.pac_pices_Disable) : (styles.pac_pices_Enable)} keyboardType="numeric" editable={isSelected ? (false) : (true)} value={MainfestData?.packet}
									onChange={(e) => handleChangeValue(e?.nativeEvent?.text, "packet")} />}
							</View>
							<CustomFieldWithAction value={MainfestData.consigmentNo} onPress={() => handleScan("consigmentNo")} placeholder="Consignment No"
								onChange={(e) => handleChangeValue(e?.nativeEvent?.text, "consigmentNo")} ref={consigmentNoRef} dissableField={disableContainer} />
							<View style={styles.view2}>
								<Text style={styles.weight}>Weight</Text>
								<TextInput style={(isSelected) ? (styles.weight_fld_Disable) : (styles.weight_fld_Enable)} keyboardType="numeric" editable={isSelected ? (false) : (true)} value={MainfestData?.weight}
									onChange={(e) => handleChangeValue(e?.nativeEvent?.text, "weight")} />
								<Text style={styles.manifest_txt}>Pices</Text>
								<TextInput style={(isSelected) ? (styles.pac_pices_Disable) : (styles.pac_pices_Enable)} keyboardType="numeric" editable={isSelected ? (false) : (true)} value={MainfestData?.pices}
									onChange={(e) => handleChangeValue(e?.nativeEvent?.text, "pices")} />
								<TouchableOpacity onPress={() => addManifest()}>
									<Ionicons name="add-circle" size={40} color={disableContainer ? "#999999" : "#4ade80"} style={{ marginLeft: 20 }} />
								</TouchableOpacity>
							</View>
							<Text style={styles.prev_cnsgn}>Previous Consignment: {previousConsigment}</Text>
						</View>

						<View style={{ justifyContent: 'space-between', flexDirection: 'row', width: '90%', paddingTop: 10 }}>
							<Text style={styles.count}>Count: {(route?.name === "Incoming Manifest Document" ? (allCount?.inFailed) : (allCount?.outFailed))} / {totalCount}</Text>
							<TouchableOpacity onPress={() => handlePrinter()}>
								<MaterialCommunityIcons name="printer-settings" size={24} />
							</TouchableOpacity>
						</View>

						<View style={styles.newManifest_container}>
							<CustomSubmit type={"Get Manifest"} onPress={() => getManifest()} />
							<CustomSubmit type={"New Manifest"} onPress={() => newManifest()} />
						</View>
					</View>
				</ScrollView>
			</TouchableWithoutFeedback>

			{scan && <CustomQRScanner onSuccess={onSuccess} />}
		</>
	)
}

const mapStatetoProps = (state) => {
	const { AutoCompleteData } = state.global;
	const { mainfestObj, InMainfestArr, OutMainfestArr, Internet } = state.local;

	return {
		mainfestObj,
		AutoCompleteData,
		InMainfestArr,
		OutMainfestArr,
		Internet
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		globalaction: (reqObj, reducerType, reducerKey) => { return dispatch(callActions.Common_GlobalActions(reqObj, reducerType, reducerKey)) },
		setmenuTtem: (val, key) => { return dispatch(calllocalActions.setMenuItem(val, key)) },
		loading: (reqObj) => { return dispatch(calllocalActions.Loading(reqObj)) },
		BothManifestDetailsUpdate: (reqObj, key) => { return dispatch(callActions.setInMainDetailsUpdate(reqObj, key)) },
		ManifestDetailsReset: (key) => { return dispatch(calllocalActions.setInMainDetailsReset(key)) },
		getinoutmanifest: (reqObj) => { return dispatch(callActions.POST_IN_OUT_GET_Manifest(reqObj)) },
		setmainifestDetails: (reqObj, el) => { return dispatch(calllocalActions.setMainifestDetails(reqObj, el)) },
	}
}

export default connect(mapStatetoProps, mapDispatchToProps)(CustomManifest);