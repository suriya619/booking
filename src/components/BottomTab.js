import React from "react";
import { HStack, Center, Pressable, Text, Box, View } from 'native-base';
import Entypo from "react-native-vector-icons/Entypo";
import * as renderRoutes from "../routes";
import styles from "./styles";
import { isEmpty } from "lodash";
import Device from "react-native-device-detection";

const BottomTab = (props) => {
	const { navigationRef, getheadersearch, route } = props;
	const [selected, setSelected] = React.useState("");

	const renderRouteCompoenet = (ele) => {
		setSelected(ele.name)
		navigationRef?.current?.navigate(ele?.name, { name: ele?.disName });
	}

	return (<>
		<View style={styles.bottomtab}>
			<Box flex={1} safeAreaTop>
				<HStack alignItems='baseline' safeAreaBottom>
					{!isEmpty(renderRoutes.tabRoutes) && (renderRoutes.tabRoutes || []).map((ele, index) =>
						<Pressable key={index} onPress={() => renderRouteCompoenet(ele)} py="2" px="2" flex={1} >
							<Center>
								<Entypo name="music" color={ele.name === selected ? "#000" : "#a3a3a3"} size={Device.isTablet ? 24 : 20} />
								<Text style={{ color: ele.name === selected ? "#000" : "#a3a3a3", textAlign: 'center', fontSize: 14, }}>{ele.label}</Text>
							</Center>
						</Pressable>
					)}
				</HStack>
			</Box>
		</View>
	</>)
}

export default BottomTab;