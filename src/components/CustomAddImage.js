import React, { useState } from "react";
import { View, Platform, Text, TouchableOpacity, Image, Modal, StyleSheet, Keyboard, TouchableWithoutFeedback, SafeAreaView } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import { connect } from "react-redux";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import moment from 'moment';

// Components
import CustomFieldWithAction from "./CustomFieldWithAction";
import CustomQRScanner from "./CustomQRScanner";
import { CustomSubmit } from "../components/CustomButton";

// Stylesheet
import styles from "./styles";

// Utils
import { chooseFiles } from "../utils/Helper";

// Redux :: Actions
import * as callActions from "../../src/redux/actions/globalActions";
import { BOOKING_IMG_CAPTURE_GET } from "../redux/types";


const CustomAddImage = (props) => {
    const { bookingImgcapture, globalaction } = props;

    const tempImageObj = {
        fullImagesAdd: "",
        toAddress: "",
    }
    const todayDate = moment(new Date).format("DD/MM/YYYY");
    const [scan, setScan] = useState(false);
    const [consigmentNo, setconsigmentNo] = useState(null);
    const [imageSelected, setImageSelected] = useState(null);
    const [imageObject, setImageObj] = useState(tempImageObj);
    const [swappingFull, setSwappingFull] = useState(false);
    const [swappingTo, setSwappingTo] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [popupImage, setPopupImage] = useState(null);


    const handleScan = () => {
        setScan(true);
    };

    const onSuccess = (e) => {
        setconsigmentNo(e?.data);
        setScan(false);
    };

    const handleChangeValue = (data) => {
        setconsigmentNo(data);
    };

    const handleCancel = (data) => {
        if (data === 'fullImagesAdd') {
            setImageObj({ ...imageObject, fullImagesAdd: "" });
            setSwappingFull(false);

        } else if (data === 'toAddress') {
            setImageObj({ ...imageObject, toAddress: "" });
            setSwappingTo(false);
        }
    };

    const submitHandler = async () => {
        const reqObj = {
            objName: "consignment",
            cmod: "GETBOOKINGIMG",
            p001: consigmentNo,
        }
        const fetch_API = await globalaction(reqObj, BOOKING_IMG_CAPTURE_GET, null);
        if (fetch_API?.data?.data?.length > 0) {
            setImageObj({
                ...imageObject,
                fullImagesAdd: 'https://g3proweb.sgp1.digitaloceanspaces.com' + fetch_API?.data?.data[0].fromimage,
                toAddress: 'https://g3proweb.sgp1.digitaloceanspaces.com' + fetch_API?.data?.data[0].toimage
            });
            setSwappingFull(true);
            setSwappingTo(true);
        }
    };

    const updateHandler = async () => {
        const imgArr = [];
        const resArr = [];
        imgArr.push(imageObject?.fullImagesAdd, imageObject?.toAddress);
        for (let i = 0; i < imgArr?.length; i++) {
            const formData = new FormData();
            formData.append("interBranchCode", "HEAD");
            formData.append("consinmentdate", todayDate);
            formData.append("formtype", "Booking");
            formData.append("file", {
                uri: imgArr[i],
                type: "image/jpeg",
                name: imgArr[i]
            });
            const fetch_API = await bookingImgcapture(formData, "Booking");
            if (fetch_API?.status === 201) {
                resArr.push(fetch_API?.data?.data);
            } else {
                alert(fetch_API);
            }
        }
        if (resArr[0] != undefined && consigmentNo) {
            const reqObj = {
                objName: "consignment",
                cmod: "UPDBOOKINGIMG ",
                p001: consigmentNo,
                p002: resArr[0]?.folder + '/' + resArr[0]?.filename,
                p003: ((resArr[1] != undefined) ? (resArr[1]?.folder + '/' + resArr[1]?.filename) : (""))
            }
            if (fetch_API?.status === 200) {
                setconsigmentNo(null);
                setImageObj({ ...imageObject, fullImagesAdd: "" });
                setImageObj({ ...imageObject, toAddress: "" });
                setSwappingFull(false);
                setSwappingTo(false);
            } else {
                alert(fetch_API);
            }
        } else {
            alert('Invalid Input');
        }
    };

    return (
        <>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <SafeAreaView>
                    <View style={styles.imageContainerSearch}>
                        <CustomFieldWithAction placeholder="Consignment Number" {...props} value={consigmentNo} onPress={() => handleScan()}
                            onChange={(e) => handleChangeValue(e)} />
                    </View>
                    <View style={styles.imageContainer}>
                        <View style={styles.dragdrop} >
                            <Text style={Platform.OS === 'ios' ? styles.imageText_ios : styles.imageText}>Full Image Add</Text>
                            {swappingFull ? <View>
                                <TouchableOpacity onPress={() => [setModalVisible(true), setPopupImage(imageObject?.fullImagesAdd)]}>
                                    <Image style={styles.imagesUpload}
                                        source={{ uri: imageObject?.fullImagesAdd }}>
                                    </Image>
                                </TouchableOpacity>
                                <MaterialIcons name="cancel" color="red" size={24} style={styles.multiImageCancelIcon} onPress={() => handleCancel('fullImagesAdd')} />
                            </View> : null}
                            {!swappingFull ? <View style={{ flexDirection: 'row' }}>
                                <View style={{ paddingRight: 20 }}>
                                    <MaterialIcons name="photo-camera" color="gray" size={24} onPress={() =>
                                        [chooseFiles('Images', "fullImagesAdd", imageObject, setImageObj) && setImageSelected('fullImagesAdd'), setSwappingFull(true)]} />
                                </View>
                                <View style={{ paddingLeft: 20 }}>
                                    <MaterialIcons name="insert-photo" color="gray" size={24} onPress={() =>
                                        [chooseFiles('Files', "fullImagesAdd", imageObject, setImageObj) && setImageSelected('fullImagesAdd'), setSwappingFull(true)]} />
                                </View>
                            </View> : null}
                        </View>

                        <View style={styles.dragdrop}>
                            <Text style={Platform.OS === 'ios' ? styles.imageText_ios : styles.imageText}>To Address</Text>
                            {swappingTo ? <View>
                                <TouchableOpacity onPress={() => [setModalVisible(true), setPopupImage(imageObject?.toAddress)]}>
                                    <Image style={styles.imagesUpload}
                                        source={{ uri: imageObject?.toAddress }}>
                                    </Image>
                                    <MaterialIcons name="cancel" color="red" size={24} style={styles.multiImageCancelIcon} onPress={() => handleCancel('toAddress')} />
                                </TouchableOpacity>
                            </View> : null}
                            {!swappingTo ? <View style={{ flexDirection: 'row' }}>
                                <View style={{ paddingRight: 20 }}>
                                    <MaterialIcons name="photo-camera" color="gray" size={24} onPress={() => [
                                        chooseFiles('Images', "toAddress", imageObject, setImageObj) && setImageSelected('toAddress'), setSwappingTo(true)]} />
                                </View>
                                <View style={{ paddingLeft: 20 }}>
                                    <MaterialIcons name="insert-photo" color="gray" size={24} onPress={() => [
                                        chooseFiles('Files', "toAddress", imageObject, setImageObj) && setImageSelected('toAddress'), setSwappingTo(true)]} />
                                </View>
                            </View> : null}
                        </View>
                    </View>
                    <View style={styles.buttons}>
                        <CustomSubmit type={"Submit"} onPress={() => submitHandler()} />
                        <CustomSubmit type={"Update Image"} onPress={() => updateHandler()} />
                    </View>
                </SafeAreaView>
            </TouchableWithoutFeedback>

            {scan && <CustomQRScanner onSuccess={onSuccess} />}

            {modalVisible &&
                <View style={modelStyle.centeredView}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => { setModalVisible(!modalVisible) }}>
                        <ImageViewer imageUrls={[{ url: popupImage }]} enableSwipeDown={false} saveToLocalByLongPress={false} failImageSource={() => alert('Error Image')}
                            loadingRender={() => <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>Loading ...</Text>} />
                    </Modal>
                </View>
            }
        </>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
        bookingImgcapture: (reqObj) => { return dispatch(callActions.POST_ImageUpload(reqObj)) },
        bookimageuploadUpdate: (reqObj) => { return dispatch(callActions.POST_BookingImgCaptureUpdate(reqObj)) },
    }
}

const modelStyle = StyleSheet.create({
    centeredView: {
        flex: 1,
        flexDirection: 'column'
    },
});

export default connect(null, mapDispatchToProps)(CustomAddImage);