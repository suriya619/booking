import React, { useEffect, useState, useRef } from 'react';
import { StatusBar, Dimensions, Platform, SafeAreaView, StyleSheet, View, Text, Keyboard, BackHandler, Alert } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator, } from "@react-navigation/native-stack";
import { isEmpty } from 'lodash';
import * as renderRoutes from "../routes/index"
import { connect } from "react-redux";
import Spinner from 'react-native-loading-spinner-overlay';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

// Components
import Header from './Header';
import InitialPage from '../views/InitialPage/InitialPage';
import BottomTab from './BottomTab';
import SplashScreen from 'react-native-splash-screen';
import ManageOTP from '../views/OTP/index';
import Registration from '../views/Registration';

// Utils
import RouteHeader from '../utils/RouteHeader';
import NetInfo from "@react-native-community/netinfo";

// Redux :: Actions
import * as calllocalActions from "../redux/actions/localActions";
import * as callActions from "../redux/actions/globalActions";
import { SIDEMENU, AUTOCOMPLETE_DATA } from '../redux/types';



const Stack = createNativeStackNavigator();

const AuthenticateRoute = (props) => {
  const { loadingScreen, internet, ResponseOTP, globalaction, ToggleSideMenu, togglesidemenu, customsearchhides } = props;

  const [routeName, setRouteName] = useState("");
  const navigationRef = useRef();
  const [isKeyboardVisible, setKeyboardVisible] = useState(false);
  const [connected, setConnected] = useState(false);

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setKeyboardVisible(true); // hides bottom nav tab
        customsearchhides(true);
      }
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setKeyboardVisible(false); // show bottom nav tab
        customsearchhides(false);
      }
    );
    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, []);

  useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 2000)
  }, []);

  useEffect(() => {
    NetInfo.addEventListener(state => {
      setConnected(state?.isConnected);
      internet(state?.isConnected);
    });
  }, [connected])

  useEffect(() => {
    fetchReason();
    handlesideMenu();
  })

  const handlesideMenu = async () => {
    const reqObj = {
      objName: "mobileregistration",
      cmod: "GETMENU",
      p001: ResponseOTP?.branch_code
    }
    if (ResponseOTP?.branch_code) {
      await globalaction(reqObj, SIDEMENU, null);
    }
  }

  const fetchReason = async () => {
    if (connected) {
      const reqObj = {
        objName: "autocomplete",
        cmod: "SEARCDELRE",
        p001: "",
      }
      const fetch_API = await globalaction(reqObj, AUTOCOMPLETE_DATA, "REASON");
    }
  }

  const getheadersearch = (props) => {
    setRouteName(props.route?.params?.name);
    return {
      headerTitle: () => <RouteHeader {...props} />,
      headerStyle: {
        width: Dimensions.get('window').width,
      },
      headerBackVisible: false,
    }
  }

  const handleDrawer = () => {
    if (ToggleSideMenu) {
      togglesidemenu(false);
    } else {
      togglesidemenu(true);
    }
  }

  return (
    <>
      {Platform.OS === 'ios' ? <StatusBar barStyle="dark-content" /> : <StatusBar backgroundColor={"#008ea7"} translucent={false} />}
      <NavigationContainer ref={navigationRef} >
        <SafeAreaView style={{ flex: 1 }}>
          <Header navigationRef={navigationRef} getheadersearch={getheadersearch} handleDrawer={handleDrawer} />
          <Stack.Navigator screenOptions={getheadersearch} initialRouteName="InitialPage"  >
            <Stack.Screen name="InitialPage" component={InitialPage} options={{ headerShown: false }} {...props} />
            <Stack.Screen name="Registration" component={Registration} options={getheadersearch} {...props} />
            <Stack.Screen name="OTP" component={ManageOTP} options={getheadersearch} {...props} navigationRef={navigationRef} />
            {!isEmpty(renderRoutes.unAuthenticateRoutes) && (renderRoutes.unAuthenticateRoutes || []).map((ele, index) =>
              <Stack.Screen key={index} name={ele.name} component={ele.component} options={getheadersearch} />
            )}
          </Stack.Navigator>
          {/* {(routeName !== "" && (routeName === "InitialPage" || routeName === "Registration" || routeName === "OTP")) ? null : (
            !isKeyboardVisible && <BottomTab {...props} navigationRef={navigationRef} getheadersearch={getheadersearch} />
          )} */}
          {!connected && <View style={{ backgroundColor: 'red', height: 30, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
            <MaterialIcons name="wifi-off" color="white" size={20} />
            <Text style={{ color: 'white', fontWeight: 'bold', paddingLeft: 5 }}>{connected ? 'Back to online' : 'Offline'}</Text>
          </View>}
        </SafeAreaView>
        {loadingScreen && <Spinner
          size={48}
          visible={true}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />}
      </NavigationContainer>
    </>
  )
}

const mapStateToProps = (state) => {
  const { loadingScreen, ToggleSideMenu } = state.local;
  const { ResponseOTP } = state.presistLocal;
  return { loadingScreen, ResponseOTP, ToggleSideMenu }
}

const mapDispatchToProps = (dispatch) => {
  return {
    globalaction: (reqObj, type, key) => { return dispatch(callActions.Common_GlobalActions(reqObj, type, key)) },
    internet: (reqObj) => { return dispatch(calllocalActions.Internet(reqObj)) },
    togglesidemenu: (reqObj) => { return dispatch(calllocalActions.setToggleSideMenu(reqObj)) },
    customsearchhides: (reqObj) => { return dispatch(calllocalActions.setCustomSearchOutsideClick(reqObj)) },
  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: 'white',
    fontSize: 16,
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticateRoute);